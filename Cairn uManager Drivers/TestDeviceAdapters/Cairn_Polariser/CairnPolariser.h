#ifndef CAIRN_POLARISER_H
#define CARIN_POLARISER_H

#include <DeviceBase.h>
#include <string>

enum PolariserMode
{
	POINT_TO_POINT = 0,
	HYSTERESIS_CORRECTION = 1,
	HOME_POINT_TO_POINT = 2,
	ALL = 3
};

enum COMResponse
{
	DONE,
	ERR,
	UNKNOWN,
	BUSY,
	COM_ERROR
};

class CairnPolariser : public CStageBase<CairnPolariser> {
public:
	CairnPolariser();
	~CairnPolariser();


	int Initialize();
	int Shutdown();

	void GetName(char* pszName) const;
	bool Busy();


	int SetPositionUm(double pos);
	int GetPositionUm(double& pos);
	int SetPositionSteps(long steps);
	int GetPositionSteps(long& steps);
	int SetOrigin();
	int GetLimits(double& min, double& max);

	int IsStageSequenceable(bool& isSequenceable) const {isSequenceable = false; return DEVICE_OK;}
	bool IsContinuousFocusDrive() const {return false;}



	COMResponse SetPos(char PAB, int pos);
	COMResponse Home(char PAB);
	COMResponse Stop(char PAB);

	COMResponse GetPos(char PAB, int *pos);
	COMResponse Ready(char PAB, bool *ready);
	COMResponse GetMode(char PAB, PolariserMode *mode);
	COMResponse GetLock(bool *locked);
	COMResponse GetCalibrated(char PAB, bool *calibrated);

	COMResponse Save(char PAB, int pos);
	COMResponse SetMode(char PAB, int mode);
	COMResponse Lock(bool lock);

	COMResponse Reset();


	int OnPort(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAPos(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnPPos(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnLock(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAMode(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnPMode(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnReset(MM::PropertyBase* pProp, MM::ActionType eAct);

	int OnCalibrate(MM::PropertyBase* pProp, MM::ActionType eAct, long data);
	int OnCalibrated(MM::PropertyBase* pProp, MM::ActionType eAct, long data);
	int OnSave(MM::PropertyBase* pProp, MM::ActionType eAct, long data);

private:
	std::string m_port;
	bool m_initialised;

	int m_APos;
	int m_PPos;

	PolariserMode m_AMode;
	PolariserMode m_PMode;

	bool m_locked;

	void ProcessError(COMResponse response, std::string message);
	std::string GetErrorText(COMResponse response);

	COMResponse SendMessageToPolariser(const std::string& command, char PAB, int* arg, int* response);

	std::string PolariserModeToName(PolariserMode mode);
	PolariserMode NameToPolariserMode(const std::string& name);
};

#endif // !CAIRN_POLARISER_H
