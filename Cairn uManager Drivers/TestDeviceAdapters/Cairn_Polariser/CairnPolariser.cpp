#include "CairnPolariser.h"
#include <sstream>
#include <ctime>

const char* g_CairnPolariserName = "Cairn Polariser Control";
#define POLARISER_ERROR 10003

MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_CairnPolariserName, MM::StageDevice, "Polariser Control");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
   if (deviceName == 0)
      return 0;

   if (strcmp(deviceName, g_CairnPolariserName) == 0)
   {
      CairnPolariser* s = new CairnPolariser();
      return s;
   }

   return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}

CairnPolariser::CairnPolariser()
	: m_port("Undefined"),
	m_initialised(false),
	m_APos(0),
	m_PPos(0),
	m_locked(false)
{
	InitializeDefaultErrorMessages();

	CreateProperty(MM::g_Keyword_Name, g_CairnPolariserName, MM::String, true);

	CreateProperty(MM::g_Keyword_Description, "Cairn Polariser Control", MM::String, true);

	CPropertyAction* pActPort = new CPropertyAction (this, &CairnPolariser::OnPort);
	CreateProperty(MM::g_Keyword_Port, "Undefined", MM::String, false, pActPort, true);
}


CairnPolariser::~CairnPolariser()
{
	Shutdown();
}

int CairnPolariser::Initialize()
{
	m_initialised = true;

	PurgeComPort(m_port.c_str());

	CPropertyAction* pAct = new CPropertyAction (this, &CairnPolariser::OnAPos);
	CreateProperty("Analyser Position", "0", MM::Integer, false, pAct);
	SetPropertyLimits("Analyser Position", 0, 19999);
	
	pAct = new CPropertyAction (this, &CairnPolariser::OnPPos);
	CreateProperty("Polariser Position", "0", MM::Integer, false, pAct);
	SetPropertyLimits("Polariser Position", 0, 19999);

	pAct = new CPropertyAction(this, &CairnPolariser::OnLock);
	CreateProperty("Locked", "0", MM::Integer, false, pAct);
	AddAllowedValue("Locked", "0");
	AddAllowedValue("Locked", "1");

	pAct = new CPropertyAction(this, &CairnPolariser::OnAMode);
	CreateProperty("Analyser Mode", PolariserModeToName((PolariserMode) 0).c_str(), MM::String, false, pAct);
	for (int i = 0; i < 4; i++)
		AddAllowedValue("Analyser Mode", PolariserModeToName((PolariserMode)i).c_str());

	pAct = new CPropertyAction(this, &CairnPolariser::OnPMode);
	CreateProperty("Polariser Mode", PolariserModeToName((PolariserMode) 0).c_str(), MM::String, false, pAct);
	for (int i = 0; i < 4; i++)
		AddAllowedValue("Polariser Mode", PolariserModeToName((PolariserMode)i).c_str());

	CPropertyActionEx* pActEx = new CPropertyActionEx(this, &CairnPolariser::OnCalibrate, (long)'P');
	CreateProperty("Calibrate Polariser", "0", MM::Integer, false, pActEx);
	AddAllowedValue("Calibrate Polariser", "0");
	AddAllowedValue("Calibrate Polariser", "1");

	pActEx = new CPropertyActionEx(this, &CairnPolariser::OnCalibrate, (long)'A');
	CreateProperty("Calibrate Analyser", "0", MM::Integer, false, pActEx);
	AddAllowedValue("Calibrate Analyser", "0");
	AddAllowedValue("Calibrate Analyser", "1");

	pActEx = new CPropertyActionEx(this, &CairnPolariser::OnCalibrate, (long)'B');
	CreateProperty("Calibrate", "0", MM::Integer, false, pActEx);
	AddAllowedValue("Calibrate", "0");
	AddAllowedValue("Calibrate", "1");

	pActEx = new CPropertyActionEx(this, &CairnPolariser::OnSave, (long)'P');
	CreateProperty("Set Polariser Home", "0", MM::Integer, false, pActEx);
	AddAllowedValue("Set Polariser Home", "0");
	AddAllowedValue("Set Polariser Home", "1");

	pActEx = new CPropertyActionEx(this, &CairnPolariser::OnSave, (long)'A');
	CreateProperty("Set Analyser Home", "0", MM::Integer, false, pActEx);
	AddAllowedValue("Set Analyser Home", "0");
	AddAllowedValue("Set Analyser Home", "1");

	pAct = new CPropertyAction(this, &CairnPolariser::OnReset);
	CreateProperty("Reset", "0", MM::Integer, false, pAct);
	AddAllowedValue("Reset", "0");
	AddAllowedValue("Reset", "1");

	return DEVICE_OK;
}

void CairnPolariser::GetName(char* pszName) const
{
   CDeviceUtils::CopyLimitedString(pszName, g_CairnPolariserName);
}

int CairnPolariser::Shutdown()
{
	return DEVICE_OK;
}

bool CairnPolariser::Busy()
{
	bool rP, rA;

	/*COMResponse response = Ready('P', &rP);
	if (response != DONE)
	{
		ProcessError(response, "Ready Check (Polariser)");
		return true;
	}

	response = Ready('A', &rA);
	if (response != DONE)
	{
		ProcessError(response, "Ready Check (Analyser)");
		return true;
	}

	return (!rP) || (!rA);*/
	return false;
}

int CairnPolariser::SetPositionUm(double pos)
{
	return SetPositionSteps(pos);
}

int CairnPolariser::GetPositionUm(double & pos)
{
	int res;
	long tmp;
	res = GetPositionSteps(tmp);
	pos = tmp;
	return res;
}

int CairnPolariser::SetPositionSteps(long steps)
{
	std::stringstream ss;
	ss << steps;
	return SetProperty("Polariser Position", ss.str().c_str());
}

int CairnPolariser::GetPositionSteps(long & steps)
{
	int res = GetProperty("Polariser Position", steps);
	return res;
}

int CairnPolariser::SetOrigin()
{
	return SetProperty("Set Polariser Position", "1");
}

int CairnPolariser::GetLimits(double & min, double & max)
{
	int r;
	if ((r = GetPropertyUpperLimit("Polariser Position", max)) != DEVICE_OK)
		return r;
	else
		return GetPropertyLowerLimit("Polariser Position", min);
}

COMResponse CairnPolariser::SetPos(char PAB, int pos)
{
	return SendMessageToPolariser("POS", PAB, &pos, NULL);
}

COMResponse CairnPolariser::Home(char PAB)
{
	return SendMessageToPolariser("HOM", PAB, NULL, NULL);
}

COMResponse CairnPolariser::Stop(char PAB)
{
	return SendMessageToPolariser("STO", PAB, NULL, NULL);
}

COMResponse CairnPolariser::GetPos(char PAB, int *pos)
{
	return SendMessageToPolariser("GET", PAB, NULL, pos);
}

COMResponse CairnPolariser::Ready(char PAB, bool *ready)
{
	int rdy;
	COMResponse resp = SendMessageToPolariser("RDY", PAB, NULL, &rdy);
	*ready = rdy;

	return resp;
}

COMResponse CairnPolariser::GetMode(char PAB, PolariserMode * mode)
{
	int m;
	COMResponse resp = SendMessageToPolariser("GMO", PAB, NULL, &m);

	switch (m) {
	case 0:
		*mode = POINT_TO_POINT;
		break;
	case 1:
		*mode = HYSTERESIS_CORRECTION;
		break;
	case 2:
		*mode = HOME_POINT_TO_POINT;
		break;
	case 3:
		*mode = ALL;
		break;
	default:
		return UNKNOWN;
	}

	return resp;
}

COMResponse CairnPolariser::GetLock(bool * locked)
{
	int lck;
	COMResponse resp = SendMessageToPolariser("GLK", 'N', NULL, &lck);
	*locked = lck;

	return resp;
}

COMResponse CairnPolariser::Save(char PAB, int pos)
{
	return SendMessageToPolariser("SAV", PAB, &pos, NULL);
}

COMResponse CairnPolariser::SetMode(char PAB, int mode)
{
	return SendMessageToPolariser("MOD", PAB, &mode, NULL);
}

COMResponse CairnPolariser::Lock(bool lock)
{
	int lck = lock;

	return SendMessageToPolariser("LOK", 'N', &lck, NULL);
}

COMResponse CairnPolariser::Reset()
{
	return SendMessageToPolariser("RST", 'N', NULL, NULL);
}

COMResponse CairnPolariser::GetCalibrated(char PAB, bool *calibrated)
{
	int val;
	COMResponse r = SendMessageToPolariser("CAL", PAB, NULL, &val);
	*calibrated = (bool)val;

	return r;
}

int CairnPolariser::OnPort(MM::PropertyBase * pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(m_port.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		if (m_initialised)
		{
			// revert
			pProp->Set(m_port.c_str());
			return 1;// ERR_PORT_CHANGE_FORBIDDEN;
		}
		pProp->Get(m_port);
	}

	return DEVICE_OK;
}

void CairnPolariser::ProcessError(COMResponse response, std::string message)
{
	switch (response)
	{
	case DONE:
		LogMessage("Done Response from " + message);
	case ERR:
		LogMessage("Error Response from " + message);
	case BUSY:
		LogMessage("Busy Response from " + message);
	case COM_ERROR:
		LogMessage("COM Error Response from " + message);
	default:
		LogMessage("Unknown Respose from " + message);
		break;
	}
}



COMResponse CairnPolariser::SendMessageToPolariser(const std::string& command, char PAB, int* arg, int* response)
{
	std::stringstream ss = std::stringstream();

	ss << command;

	if (PAB != 'N')
		ss << " " << PAB;

	if (arg != NULL)
		ss << " " << *arg << ";";
	else
		ss << ";";

	if (SendSerialCommand(m_port.c_str(), ss.str().c_str(), "") != DEVICE_OK)
		return COM_ERROR;

	int ret = DEVICE_OK;
	std::string ans;
	clock_t start = clock();
	while (ret == DEVICE_OK)
	{
		if ((clock() - start) / CLOCKS_PER_SEC > 11)
			return COM_ERROR;

		GetSerialAnswer(m_port.c_str(), "\r\n", ans);
		if (ans != "")
			break;
	}


	if (ans == "D")
	{
		return DONE;
	}
	else if (ans == "E")
	{
		return ERR;
	}
	else if (ans == "B")
	{
		return BUSY;
	}
	else
	{
		if (response != NULL && sscanf_s(ans.c_str(), "%d", response) == 1)
			return DONE;
		else
			return UNKNOWN;
	}
}

std::string CairnPolariser::GetErrorText(COMResponse response)
{
	switch (response)
	{
	case DONE:
		return "Response: Done";
	case BUSY:
		return "Response: Busy";
	case ERR:
		return "Response: Error";
	case COM_ERROR:
		return "Communication with polariser failed";
	case UNKNOWN:
	default:
		return "Unrecognised response";
	}
}

std::string CairnPolariser::PolariserModeToName(PolariserMode mode)
{
	switch (mode)
	{
	case POINT_TO_POINT:
		return "Point-to-Point";
	case HYSTERESIS_CORRECTION:
		return "Hysteresis Correction";
	case HOME_POINT_TO_POINT:
		return "Home Point-to-Point";
	case ALL:
		return "All";
	default:
		return "";
	}
}

PolariserMode CairnPolariser::NameToPolariserMode(const std::string& name)
{
	if (name == "Point-to-Point")
		return POINT_TO_POINT;
	else if (name == "Hysteresis Correction")
		return HYSTERESIS_CORRECTION;
	else if (name == "Home Point-to-Point")
		return HOME_POINT_TO_POINT;
	else if (name == "All")
		return ALL;
	else
		return ALL;

}

int CairnPolariser::OnAPos(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet) 
	{
		COMResponse r;
		if ((r = GetPos('A', &m_APos)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			pProp->Set((long)m_APos);
			return DEVICE_OK;
		}
	} 
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		COMResponse r;
		if ((r = SetPos('A', val)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			return DEVICE_OK;
		}
	}

	return DEVICE_OK;
}

int CairnPolariser::OnPPos(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet) 
	{
		COMResponse r;
		if ((r = GetPos('P', &m_PPos)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			pProp->Set((long)m_PPos);
			return DEVICE_OK;
		}
	} 
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		COMResponse r;
		if ((r = SetPos('P', val)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			return DEVICE_OK;
		}
	}

	return DEVICE_OK;
}

int CairnPolariser::OnLock(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet) 
	{
		COMResponse r;
		if ((r = GetLock(&m_locked)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			pProp->Set((long) m_locked);
			return DEVICE_OK;
		}
	} 
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		COMResponse r;
		if ((r = Lock(val)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			return DEVICE_OK;
		}
	}

	return DEVICE_OK;
}

int CairnPolariser::OnAMode(MM::PropertyBase * pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet) 
	{
		COMResponse r;
		if ((r = GetMode('A', &m_AMode)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			std::string name = PolariserModeToName((PolariserMode)m_AMode);

			pProp->Set(name.c_str());
			return DEVICE_OK;
		}
	} 
	else if (eAct == MM::AfterSet)
	{
		std::string name;
		long val;
		pProp->Get(name);

		val = NameToPolariserMode(name);

		COMResponse r;
		if ((r = SetMode('A', val)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			return DEVICE_OK;
		}
	}

	return DEVICE_OK;
}

int CairnPolariser::OnPMode(MM::PropertyBase * pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet) 
	{
		COMResponse r;
		if ((r = GetMode('P', &m_PMode)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			std::string name = PolariserModeToName((PolariserMode)m_PMode);

			pProp->Set(name.c_str());
			return DEVICE_OK;
		}
	} 
	else if (eAct == MM::AfterSet)
	{
		std::string name;
		long val;
		pProp->Get(name);

		val = NameToPolariserMode(name);

		COMResponse r;
		if ((r = SetMode('P', val)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}
		else
		{
			return DEVICE_OK;
		}
	}

	return DEVICE_OK;
}

int CairnPolariser::OnReset(MM::PropertyBase * pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set((long)0);
		return DEVICE_OK;
	}
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		if (val == 1)
		{
			COMResponse r;
			if ((r = Reset()) != DONE)
			{
				SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
				return POLARISER_ERROR;
			}
		}

		return DEVICE_OK;
	}	
}

int CairnPolariser::OnCalibrate(MM::PropertyBase * pProp, MM::ActionType eAct, long data)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set((long)0);
		return DEVICE_OK;
	}
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		if (val == 1)
		{
			COMResponse r;
			if ((r = Home((char)data)) != DONE)
			{
				SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
				return POLARISER_ERROR;
			}
		}

		return DEVICE_OK;
	}
}

int CairnPolariser::OnSave(MM::PropertyBase * pProp, MM::ActionType eAct, long data)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set((long)0);
		return DEVICE_OK;
	}
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);

		if (val == 1)
		{
			COMResponse r;
			if ((r = Save((char)data, 0)) != DONE)
			{
				SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
				return POLARISER_ERROR;
			}
		}

		return DEVICE_OK;
	}
}

int CairnPolariser::OnCalibrated(MM::PropertyBase* pProp, MM::ActionType eAct, long data)
{
	if (eAct == MM::BeforeGet)
	{
		bool calibrated;

		COMResponse r;
		if ((r = GetCalibrated((char)data, &calibrated)) != DONE)
		{
			SetErrorText(POLARISER_ERROR, GetErrorText(r).c_str());
			return POLARISER_ERROR;
		}

		pProp->Set((long)r);

		return DEVICE_OK;
	}

	return DEVICE_OK;
}
