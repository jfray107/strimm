#include "CairnDAQ.h"
//#include "stdfx.h"
#include <string>
#include <math.h>
#include "ModuleInterface.h"
//#include "Error.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
//#pragma comment(lib, "NIDAQmx.lib")

//#include "osLibrary.h"
//#include "os_maths.h"

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   #define snprintf _snprintf 
#endif

//int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);

#define AIN_0_LABEL				"Direct Analog In 0"
#define AIN_1_LABEL				"Direct Analog In 0"
#define AIN_2_LABEL				"Direct Analog In 0"
#define AIN_3_LABEL				"Direct Analog In 0"
#define AIN_4_LABEL				"Direct Analog In 0"
#define AIN_5_LABEL				"Direct Analog In 0"
#define AIN_6_LABEL				"Direct Analog In 0"
#define AIN_7_LABEL				"Direct Analog In 0"

#define DIN_LABEL				"Direct Digital In"

#define DOUT_LABEL				"Direct Digital Out"
#define AOUT_LABEL_5V           "Direct Analog Out 5 Volts"
#define AOUT_LABEL_10V          "Direct Analog Out 10 Volts"

#define ANotLabel				"Unused"
#define AO0OutLabel				"Analog Out 0"
#define AO1OutLabel				"Analog Out 1"
#define AO2OutLabel				"Analog Out 2"
#define AO3OutLabel				"Analog Out 3"
#define AO_CHAN_0				1
#define AO_CHAN_1				2
#define AO_CHAN_2				3
#define AO_CHAN_3				4


#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_INVERT		"TTL Invert"
#define SHUT_MODE_LOW			"TTL Low"
#define SHUT_MODE_HIGH			"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3

#define DEFAULTS_LABEL			"All Settings Defaults"
#define DEFAULTS_NONE			"None"
#define DEFAULTS_OPTOSCAN		"Optoscan Defaults"
#define DEFAULTS_NONE_VAL		0
#define DEFAULTS_OPTOSCAN_VAL	1

#define AOVoltsLabel			"Analog Output Voltage"
#define AOVolts1				"5.0 Volts"
#define AOVolts2				"10.0 Volts"


using namespace std;
//static TaskHandle  AOtaskHandle = 0, DOtaskHandle = 0;

//#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
//const char* g_Keyword_Grating = "Optoscan Grating Lines";
const char* g_Desc = "Cairn USB-3103";
const char* g_CairnDAQ = "CairnIO";
//const char* digitalPort = "Dev1/port0/line0:7";
//const char* digitalInPort = "Dev1/port0/line8:15";



// TODO: linux entry code

// windows DLL entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE /*hModule*/, DWORD  reason, LPVOID /*lpReserved*/  )
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
		break;
   case DLL_PROCESS_DETACH:
		break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_CairnDAQ, MM::ShutterDevice, g_Desc);
   //AddAvailableDeviceName(g_CairnDAQ, g_Desc );
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_CairnDAQ) == 0) return new CCairnDAQ();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}


///////////////////////////////////////////////////////////////////////////////
// osShutter implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~

CCairnDAQ::CCairnDAQ() :
initialized_(false),
shuttered_(true)
//gratingLines(1800),
//inVal (0),
//outVal (0),
//bandWidth (0),
//wavelength(300),
//waveVolts(0)
{
	
	waveChan = 0;
	inChan = 0;
	outChan = 0;
	lastDefaults = 0;
	DACTotal = DAC_COUNT_USED_5V + DAC_COUNT_USED_10V;
	DACUsed[0] = 0;
	DACUsed[1] = 0;
	DACUsed[2] = 0;
	DACUsed[3] = 0;
	int i;
	
	for ( i=0 ; i<DOUT_COUNT_MAX ; i++ ) DOShutMode[i] = DOSM_HIGH;

	InitializeDefaultErrorMessages();
	   // Name
	int ret = CreateProperty(MM::g_Keyword_Name, g_CairnDAQ, MM::String, true);
	//if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description , g_Desc , MM::String, true);
	//if (DEVICE_OK != ret) return ret;
}
CCairnDAQ::~CCairnDAQ()
{
   Shutdown();
}
void CCairnDAQ::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_CairnDAQ); 
}
int CCairnDAQ::initDAQ (  )
{

	revLevel = (float)CURRENTREVNUM;

	int ret = cbDeclareRevision(&revLevel);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


	/* Allocate D/A Windows buffer */
    DAMemHandle = cbWinBufAlloc ((long)DACTotal);
	
	/* Get a 32-bit pointer to the D/A WIN32 buffer */
    DAValues = (unsigned short *)GlobalLock(DAMemHandle);

	count = DACTotal;

	rate = NOTUSED;
	DAOptions = 0;
	portNum = AUXPORT;
	direction = DIGITALOUT;
	DOValues = 0;
	boardNum = 0;
	range =  UNI10VOLTS;

	ret = cbDConfigPort (boardNum, portNum, direction);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	//binValue  = 0;
	percentMult = 5.0;
	percentMult10V = 10.0;
	//data[0] = 0;
	//data[1] = 0;
	//data[2] = 0;
	//data[3] = 0;
	//data[4] = 0;
	//data[5] = 0;
	//data[6] = 0;
	//data[7] = 0;
/*
	inputData[0] =0;
	inputData[1] =0;
	inputData[2] =0;
	inputData[3] =0;
	inputData[4] =0;
	inputData[5] =0;
	inputData[6] =0;
	inputData[7] =0;
	*/
	//AOdata[0] = 0.0;
	//AOdata[1] = 0.0;
	//AOdata[2] = 0.0;
	//AOdata[3] = 0.0;



	
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	
	return 0;
}
int CCairnDAQ::Shutdown()
{
	DOValues = 0;
	shuttered_ = false;
	if (!hasRunOnce)
	{
		DOSetData(&DOValues);
		for ( int i = 0; i < DACTotal; i++)
		{
			DAValues[i] = 0;
		}
		AOSetData();
		cbStopBackground(boardNum, AOFUNCTION);
	
		if (DAMemHandle)                  
			cbWinBufFree (DAMemHandle);   /* Free allocated memory */
		hasRunOnce = true;
	}
	
   // if (MemHandle32)
    //    GlobalFree (MemHandle32);
	initialized_ = false; 

	return DEVICE_OK;
}
int CCairnDAQ::ErrorHandler(int error)
{
	char errMsg[ERRSTRLEN];
	cbGetErrMsg(error, errMsg);
	printf("MC Error: %s\n",errMsg);
	return error;
}
int CCairnDAQ::Initialize()
{
	if (initialized_) return DEVICE_OK;
	//gratingLines = 1800.0;
	inVolts = 0.0;
	outVolts = 0.0;
	bandWidth = 0.0;
	wavelength = 300.0;
	waveVolts = 0.0;
	int ret = initDAQ();
	if (DEVICE_OK != ret) return ret;

	double topWave, botWave;
	double topBand, botBand;


	CPropertyAction* pAct = new CPropertyAction (this, &CCairnDAQ::OnShutter);
	ret = CreateProperty( "Shutter Override" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter Override" , "0" ); // Closed
	AddAllowedValue( "Shutter Override" , "1" ); // Open

	CPropertyActionEx *pExAct;

	int i;
	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx(this, &CCairnDAQ::OnDOShutMode , i );
		ostringstream os;
		os << SHUT_MODE_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str(), "0.0", MM::String, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		//AddAllowedValue( os.str().c_str() , SHUT_MODE_UNUSED , DOSM_UNUSED );
		AddAllowedValue( os.str().c_str() , SHUT_MODE_INVERT , DOSM_INVERT ); 
		AddAllowedValue( os.str().c_str() , SHUT_MODE_LOW , DOSM_LOW ); 
		AddAllowedValue( os.str().c_str() , SHUT_MODE_HIGH , DOSM_HIGH ); 
	}

	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnBit , i );
		ostringstream os;
		os << DOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}

	for ( i=0 ; i < DAC_COUNT_USED_5V; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnAnaOut5V , i );
		ostringstream os;
		os << AOUT_LABEL_5V << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits( os.str().c_str() , 0.0 , 5.0);
		SetProperty(os.str().c_str(), "0");
	}
	for ( i=0 ; i < DAC_COUNT_USED_10V; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnAnaOut10V , i );
		ostringstream os;
		os << AOUT_LABEL_10V << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits( os.str().c_str() , 0.0 , 10.0);
		SetProperty(os.str().c_str() , "0");
	}
	//for ( i=0 ; i < DIN_COUNT_USED; i++)
	//{
	//	pExAct = new CPropertyActionEx (this, &CCairnDAQ::OnDigIn , i );
	//	ostringstream os;
	//	os << DIN_LABEL << " " << i;
	//	ret = CreateProperty( os.str().c_str() , "0", MM::Float, true, pExAct);
	//	if (ret != DEVICE_OK) return ret;
	//}

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnValue);
	////ret = CreateProperty( "Direct Digital Output Value" , "0", MM::Integer, false, pAct); 
	////if (ret != DEVICE_OK) return ret; 

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnWavelength);
	////ret = CreateProperty( "Optoscan Wavelength", "0.0", MM::Integer, false, pAct);
	////if (ret != DEVICE_OK) return ret;
	////SetPropertyLimits("Optoscan Wavelength",botWave,topWave);

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnBandwidth);
	////ret = CreateProperty( "Optoscan Bandwidth", "0", MM::Integer, false, pAct);
	////if (ret != DEVICE_OK) return ret;
	////SetPropertyLimits("Optoscan Bandwidth",botBand,topBand);

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnWaveVolts);
	////ret = CreateProperty( "Report Optoscan Wavelength Volts", "0", MM::Float, true, pAct);
	////if (ret != DEVICE_OK) return ret;

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnInVolts);
	////ret = CreateProperty( "Report Optoscan Input Slit Volts", "0", MM::Float, true, pAct);
	////if (ret != DEVICE_OK) return ret;

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnOutVolts);
	////ret = CreateProperty( "Report Optoscan Output Slit Volts", "0", MM::Float, true, pAct);
	////if (ret != DEVICE_OK) return ret;




	////pAct = new CPropertyAction (this, &CCairnDAQ::OnWaveChan);
	////ret = CreateProperty( OS_WV_CHAN_LABEL , "0", MM::String, false, pAct); 
	////if (ret != DEVICE_OK) return ret; 
	////AddAllowedValue( OS_WV_CHAN_LABEL , ANotLabel ); 
	////AddAllowedValue( OS_WV_CHAN_LABEL , AO0OutLabel ); 
	////AddAllowedValue( OS_WV_CHAN_LABEL , AO1OutLabel ); 
	////AddAllowedValue( OS_WV_CHAN_LABEL , AO2OutLabel ); 
	////AddAllowedValue( OS_WV_CHAN_LABEL , AO3OutLabel ); 

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnInChan);
	////ret = CreateProperty( OS_IP_CHAN_LABEL , "0", MM::String, false, pAct); 
	////if (ret != DEVICE_OK) return ret; 
	////AddAllowedValue( OS_IP_CHAN_LABEL , ANotLabel ); 
	////AddAllowedValue( OS_IP_CHAN_LABEL , AO0OutLabel ); 
	////AddAllowedValue( OS_IP_CHAN_LABEL , AO1OutLabel ); 
	////AddAllowedValue( OS_IP_CHAN_LABEL , AO2OutLabel ); 
	////AddAllowedValue( OS_IP_CHAN_LABEL , AO3OutLabel ); 

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnOutChan);
	////ret = CreateProperty( OS_OP_CHAN_LABEL , "0", MM::String, false, pAct); 
	////if (ret != DEVICE_OK) return ret; 
	////AddAllowedValue( OS_OP_CHAN_LABEL , ANotLabel ); 
	////AddAllowedValue( OS_OP_CHAN_LABEL , AO0OutLabel ); 
	////AddAllowedValue( OS_OP_CHAN_LABEL , AO1OutLabel ); 
	////AddAllowedValue( OS_OP_CHAN_LABEL , AO2OutLabel ); 
	////AddAllowedValue( OS_OP_CHAN_LABEL , AO3OutLabel ); 

	////pAct = new CPropertyAction (this, &CCairnDAQ::OnDefaults);
	////ret = CreateProperty( DEFAULTS_LABEL , "0", MM::String, false, pAct); 
	////if (ret != DEVICE_OK) return ret; 
	////AddAllowedValue( DEFAULTS_LABEL , DEFAULTS_NONE  );
	////AddAllowedValue( DEFAULTS_LABEL , DEFAULTS_OPTOSCAN  );

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	//SetVoltages();
	hasRunOnce = false;
	initialized_ = true;
	return DEVICE_OK;
}
bool CCairnDAQ::Busy()
{

	return false;
}
int CCairnDAQ::DOSetData(WORD *myData)
{
	if (!shuttered_)
	{
		int ret = cbDOut(boardNum, portNum, *myData);
		if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
		
	}
	return DEVICE_OK;
}
int CCairnDAQ::AOSetData()
{

	int channel;
	int ret = cbAOutScan (boardNum, 0, DACTotal -1 , count, &rate, range, DAMemHandle, DAOptions);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	//int ret;
	//for (int i = 0; i < DAC_COUNT_USED_5V ; i++)
	//{
	//	channel = i;
	//	ret = cbVOut(boardNum, channel, range, DAValues[i], DAOptions);
	//}

	//ret = cbAOutScan (boardNum, DAC_COUNT_USED_5V  , DAC_COUNT_USED_10V +3 , count10V, &rate, range10V, DAMemHandle10V, DAOptions);
	//if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}

int CCairnDAQ::SetOpen (bool open)
{
	if ( open )
	{
		shuttered_ = false;
		DOSetData(&DOValues);
	}else{
		//uInt8 DigitalShutter[DOUT_COUNT_MAX];
		//if ( pos )
		//	DOValues = DOValues | (0x01 << channel);
	//	else
		//	DOValues = DOValues & ~(0x01 << channel);
//// this may not work !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		WORD DigitalShutter = 0;
		//unsigned short *myData = new unsigned short;
		//*myData = DOValues;
		int i;
		for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
		{
			switch ( DOShutMode[i] )
			{
				case DOSM_INVERT:
						if ( DOValues & (0x01 << i) )
							DigitalShutter = DigitalShutter & ~(0x01 << i);
						else
							DigitalShutter = DigitalShutter | (0x01 << i);
					break;
				case DOSM_HIGH:
						DigitalShutter = DigitalShutter & ~(0x01 << i);
					break;
				case DOSM_LOW:
						DigitalShutter = DigitalShutter | (0x01 << i);
					break;
				//default:	
				//	DigitalShutter = myData[i];
			}
		}
		DOSetData( &DigitalShutter);
		shuttered_ = true;
	}
	return DEVICE_OK;
}
int CCairnDAQ::GetOpen(bool& open)
{
	if ( shuttered_ )
		open = false;
	else
		open = true;
	return DEVICE_OK;
}
int CCairnDAQ::OnValue(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	long pos;
	if (eAct == MM::BeforeGet)
	{
		pos = DOValues;
		pProp->Set(pos);
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get(pos);
		DOValues = pos;
		
		DOSetData( &DOValues);
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shuttered_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}



int CCairnDAQ::OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( DOValues  & (0x01 << channel))
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			DOValues = DOValues | (0x01 << channel);
		else
			DOValues = DOValues & ~(0x01 << channel);
		DOSetData( &DOValues );
   }
   return DEVICE_OK;
}

int CCairnDAQ::OnAnaOut5V(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		unsigned short aVal;
		aVal = DAValues[channel];
		pVal = ((float)(20.0/65535.0) * aVal);
		//pVal = (float)DAValues[channel] * percentMult;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		double val;
		unsigned short temp;
		pProp->Get(val);
		val = val ;// percentMult;
		
		int ret = cbFromEngUnits(boardNum, range, val, &temp);
		if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
		DAValues[channel] = temp;
		AOSetData();
	}
	return DEVICE_OK;
}

int CCairnDAQ::OnAnaOut10V(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		unsigned short aVal;
		aVal = DAValues[channel];
		pVal = ((float)((10.0/65535.0) * aVal) );
		//pVal = (float)DAValues[channel] * percentMult;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		double val;
		unsigned short temp;
		pProp->Get(val);
		val = val ;// percentMult10V;
		
		int ret = cbFromEngUnits(boardNum, range, val, &temp);
		if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
		DAValues[channel] = temp;
		AOSetData();
	}
	return DEVICE_OK;
}
int CCairnDAQ::OnOutChan(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		pVal = ANotLabel;
		switch ( outChan )
		{
			case AO_CHAN_0:
				pVal = AO0OutLabel;
				break;
			case AO_CHAN_1:
				pVal = AO1OutLabel;
				break;
			case AO_CHAN_2:
				pVal = AO2OutLabel;
				break;
			case AO_CHAN_3:
				pVal = AO3OutLabel;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		if ( outChan )	DACUsed[outChan-1] = 0;
		outChan = 0;
		if ( 0 == val.compare( AO0OutLabel ) ) outChan = AO_CHAN_0;
		if ( 0 == val.compare( AO1OutLabel ) ) outChan = AO_CHAN_1;
		if ( 0 == val.compare( AO2OutLabel ) ) outChan = AO_CHAN_2;
		if ( 0 == val.compare( AO3OutLabel ) ) outChan = AO_CHAN_3;
		if ( outChan )
		{
			if  ( DACUsed[outChan-1] ) 
			{
				outChan = 0;
				return DEVICE_CAN_NOT_SET_PROPERTY;
			}else{
				DACUsed[outChan-1] = 1;
			}
		}
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnDOShutMode(MM::PropertyBase* pProp, MM::ActionType eAct, long channel )
{
   if (eAct == MM::BeforeGet)
   {
	    string pVal;
		//pVal = SHUT_MODE_UNUSED;
		switch ( DOShutMode[channel] )
		{
			case DOSM_INVERT:
				pVal = SHUT_MODE_INVERT;
				break;
			case DOSM_LOW:
				pVal = SHUT_MODE_LOW;
				break;
			case DOSM_HIGH:
				pVal = SHUT_MODE_HIGH;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		//DOShutMode[channel] = 0;
		if ( 0 == val.compare( SHUT_MODE_INVERT ) ) DOShutMode[channel] = DOSM_INVERT;
		if ( 0 == val.compare( SHUT_MODE_LOW ) ) DOShutMode[channel] = DOSM_LOW;
		if ( 0 == val.compare( SHUT_MODE_HIGH ) ) DOShutMode[channel] = DOSM_HIGH;
   }
   return DEVICE_OK;
}
int CCairnDAQ::OnDefaults(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
		string pVal;
		switch ( lastDefaults )
		{
			case DEFAULTS_OPTOSCAN_VAL:
				pVal = DEFAULTS_OPTOSCAN;
				break;
			default:
				pVal = DEFAULTS_NONE;
				break;
		}
		pProp->Set ( pVal.c_str ( ) );
   }
   else if (eAct == MM::AfterSet)
   {
		string val;
		pProp->Get(val);
		lastDefaults = 0;
		if ( 0 == val.compare( DEFAULTS_OPTOSCAN ) ) 
		{
			lastDefaults = DEFAULTS_OPTOSCAN_VAL;
			inChan = AO_CHAN_0;
			outChan = AO_CHAN_2;
			waveChan = AO_CHAN_1;
			DOShutMode[0] = DOSM_HIGH;
		}
   }
   return DEVICE_OK;
}
