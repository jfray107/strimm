#ifndef _CAIRNNI6341_H_
#define _CAIRNNI6341_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
//#include "FTD2XX.h"
#include "NIDAQmx.h"
#include <string>
#include <map>

#define ERR_UNKNOWN_MODE         102
#define ERR_UNKNOWN_POSITION     103


#define DOUT_COUNT_MAX				8
#define DOUT_COUNT_USED				8
#define DAC_COUNT_MAX				2
#define DAC_COUNT_USED				2
//Cairn Optoscan Device
class CCairnNI6341 : public CShutterBase<CCairnNI6341>
{
public:
   CCairnNI6341() ;
   ~CCairnNI6341() {}
   int Initialize();
   int Shutdown();
   int init_optoDO ( void );
   void GetName (char* pszName) const;
   bool Busy();
   // Shutter API
   int SetOpen (bool open);
   int GetOpen(bool& open);
   int Fire(double /*deltaT*/) {return DEVICE_UNSUPPORTED_COMMAND;}

	int OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);
	//int OnChannel(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnPort(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnValue(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);

private:
	uInt8  data[DOUT_COUNT_MAX];
	float64 AOData[DAC_COUNT_MAX];
	unsigned char bits_;
	long binValue;
	//FT_HANDLE hnd_;
	std::string channel_;
	std::string port_;
	bool initialized_;
	bool shuttered_;
	void SetBits( unsigned char bits );
	int ErrorHandler(int error);
	TaskHandle DOtaskHandle, AOtaskHandle;
	int DOSetData(uInt8 *myData);
	int AOSetData(float64 *myAOData);
	int SetVoltages();
	int DACUsed[DAC_COUNT_MAX];


};

#endif //_OPTOBYTE_H_
