///////////////////////////////////////////////////////////////////////////////
// FILE:          DemoCamera.h
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   The example implementation of the demo camera.
//                Simulates generic digital camera and associated automated
//                microscope devices and enables testing of the rest of the
//                system without the need to connect to the actual hardware. 
//                
// AUTHOR:        Nenad Amodaj, nenad@amodaj.com, 06/08/2005
//                
//                Karl Hoover (stuff such as programmable CCD size  & the various image processors)
//                Arther Edelstein ( equipment error simulation)
//
// COPYRIGHT:     University of California, San Francisco, 2006-2015
//                100X Imaging Inc, 2008
//
// LICENSE:       This file is distributed under the BSD license.
//                License text is included with the source distribution.
//
//                This file is distributed in the hope that it will be useful,
//                but WITHOUT ANY WARRANTY; without even the implied warranty
//                of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//                IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//                CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//                INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.

#ifndef _DEMOCAMERA_H_
#define _DEMOCAMERA_H_

#include "../../MMDevice/DeviceBase.h"
#include "../../MMDevice/ImgBuffer.h"
#include "../../MMDevice/DeviceThreads.h"
#include <string>
#include <map>
#include <algorithm>
#include <stdint.h>

//////////////////////////////////////////////////////////////////////////////
// Error codes
//
#define ERR_UNKNOWN_MODE         102
#define ERR_UNKNOWN_POSITION     103
#define ERR_IN_SEQUENCE          104
#define ERR_SEQUENCE_INACTIVE    105
#define ERR_STAGE_MOVING         106
#define HUB_NOT_AVAILABLE        107

const char* NoHubError = "Parent Hub not defined.";
const char* const g_ReadoutPropertyName = "ReadoutFormat";
const char* const g_NDRCycleName = "NDR-FrameCycle";
const char* const g_TriggerName = "TriggerOption";
const char* const triggerMode0 = "Software - Internal";
const char* const triggerMode1 = "Start of Acq - External";
const char* const triggerMode2 = "Start of Frame - External";

// Defines which segments in a seven-segment display are lit up for each of
// the numbers 0-9. Segments are:
//
//  0       1
// 1 2     2 4
//  3       8
// 4 5    16 32
//  6      64
const int SEVEN_SEGMENT_RULES[] = {1+2+4+16+32+64, 4+32, 1+4+8+16+64,
      1+4+8+32+64, 2+4+8+32, 1+2+8+32+64, 2+8+16+32+64, 1+4+32,
      1+2+4+8+16+32+64, 1+2+4+8+32+64};
// Indicates if the segment is horizontal or vertical.
const int SEVEN_SEGMENT_HORIZONTALITY[] = {1, 0, 0, 1, 0, 0, 1};
// X offset for this segment.
const int SEVEN_SEGMENT_X_OFFSET[] = {0, 0, 1, 0, 0, 1, 0};
// Y offset for this segment.
const int SEVEN_SEGMENT_Y_OFFSET[] = {0, 0, 0, 1, 1, 1, 2};

class ImgManipulator 
{
   public:
      virtual int ChangePixels(ImgBuffer& img) = 0;
};

////////////////////////
// DemoHub
//////////////////////

class SciMeasureHub : public HubBase<SciMeasureHub>
{
public:
   SciMeasureHub() :
      initialized_(false),
      busy_(false)
   {}
   ~SciMeasureHub() {}

   // Device API
   // ---------
   int Initialize();
   int Shutdown() {return DEVICE_OK;};
   void GetName(char* pName) const; 
   bool Busy() { return busy_;} ;

   // HUB api
   int DetectInstalledDevices();

private:
   void GetPeripheralInventory();

   std::vector<std::string> peripherals_;
   bool initialized_;
   bool busy_;
};


//////////////////////////////////////////////////////////////////////////////
// CSciMeasure class
// Simulation of the Camera device
//////////////////////////////////////////////////////////////////////////////

class MySequenceThread;

class CSciMeasure : public CCameraBase<CSciMeasure>  
{
public:
   CSciMeasure();
   ~CSciMeasure();
  
   // MMDevice API
   // ------------
   int Initialize();
   int Shutdown();
  
   void GetName(char* name) const;      
   
   // MMCamera API
   // ------------
   int SnapImage();
   const unsigned char* GetImageBuffer();
   unsigned GetImageWidth() const;
   unsigned GetImageHeight() const;
   unsigned GetImageBytesPerPixel() const;
   unsigned GetBitDepth() const;
   long GetImageBufferSize() const;
   double GetExposure() const;
   void SetExposure(double exp);
   int SetROI(unsigned x, unsigned y, unsigned xSize, unsigned ySize); 
   int GetROI(unsigned& x, unsigned& y, unsigned& xSize, unsigned& ySize); 
   int ClearROI();
   int PrepareSequenceAcqusition() { return DEVICE_OK; }
   int StartSequenceAcquisition(double interval);
   int StartSequenceAcquisition(long numImages, double interval_ms, bool stopOnOverflow);
   int StopSequenceAcquisition();
   int InsertImage();
   int RunSequenceOnThread(MM::MMTime startTime);
   bool IsCapturing();
   void OnThreadExiting() throw(); 
//   double GetNominalPixelSizeUm() const {return nominalPixelSizeUm_;}
   double GetPixelSizeUm() const {return nominalPixelSizeUm_ * GetBinning();}
   int GetBinning() const;
   int SetBinning(int bS);
   int IsExposureSequenceable(bool& isSequenceable) const {isSequenceable = false; return DEVICE_OK;}

   // action interface
   // ----------------
	int OnTestProperty(MM::PropertyBase* pProp, MM::ActionType eAct, long);
	int OnBinning(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnPixelType(MM::PropertyBase* pProp, MM::ActionType eAct);
	int CSciMeasure::OnReadoutSequence(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnCameraXSize(MM::PropertyBase* , MM::ActionType );
	int OnCameraYSize(MM::PropertyBase* , MM::ActionType );
	int OnNDRFrames(MM::PropertyBase* , MM::ActionType );
	int OnTriggerMode(MM::PropertyBase* pProp, MM::ActionType eAct);

   // Special public DemoCamera methods
   void AddBackgroundAndNoise(ImgBuffer& img, double mean, double stdDev);
   // this function replace normal_distribution in C++11
   double GaussDistributedValue(double mean, double std);

   int RegisterImgManipulatorCallBack(ImgManipulator* imgManpl);


private:
   int SetAllowedBinning();
   void TestResourceLocking(const bool);
   void GenerateEmptyImage(ImgBuffer& img);
   int ResizeImageBuffer();

   void CSciMeasure::SM_pdv_close();
   int CSciMeasure::SM_pdv_open();
   void CSciMeasure::SM_set_repeat(double frame_interval);
   void CSciMeasure::SM_set_trigger(int triggerM);
   int CSciMeasure::SM_check_camera();
   int CSciMeasure::SM_startCamera(int num_frames);
   void CSciMeasure::SM_wait_image();
   void CSciMeasure::SM_get_image(int loops);
   void CSciMeasure::SM_acq_one();
   void CSciMeasure::SM_stopCamera();
   int setNDR_pos(int NDR_pos);
   void CSciMeasure::clearSMbuffer();
   void CSciMeasure::SM_rearrangeData(int num_frames);
   void CSciMeasure::SM_2KMultiF_rearrange(int loops);
   void CSciMeasure::SM_OneFrame(ImgBuffer& img);
   void CSciMeasure::subtractCDS(unsigned short int *image_data, unsigned int loops, unsigned int width, unsigned int height, unsigned int length, int factor, int QUAD, int TWOK, int transitionlFlag);
   int CSciMeasure::makeLookUpTable(unsigned int *Lut, int image_width, int image_height, int file_width, int file_height, int stripes, int layers, int factor, int QUAD, int CDS, int ROTATE, int TWOK);
   void CSciMeasure::frame_deInterleave(int length, unsigned *lookuptable, unsigned short *old_images, unsigned short *new_images);
   int CSciMeasure::SM_snap(ImgBuffer& img);
   int CSciMeasure::read_SM_Config();
   void CSciMeasure::load_pdv_cfg();
   void CSciMeasure::SM_initCamera();
   void CSciMeasure::SM_serial_command(char *command);
   void CSciMeasure::get_config_size();
   int CSciMeasure::IsImageAvailable();
  
   static const double nominalPixelSizeUm_;

   double exposureMaximum_;
   double dPhase_;
   ImgBuffer img_;
   bool busy_;
   bool stopOnOverFlow_;
   bool initialized_;
   double readoutUs_;
   MM::MMTime readoutStartTime_;
   long scanMode_;
   int bitDepth_;
   unsigned roiX_;
   unsigned roiY_;
   MM::MMTime sequenceStartTime_;
   bool isSequenceable_;
   long sequenceMaxLength_;
   bool sequenceRunning_;
   unsigned long sequenceIndex_;
   double GetSequenceExposure();
   std::vector<double> exposureSequence_;
   long imageCounter_;
	long binSize_;
	long cameraCCDXSize_;
	long cameraCCDYSize_;

	bool stopOnOverflow_;

	bool shouldRotateImages_;
	bool shouldDisplayImageNumber_;

	double testProperty_[10];
   MMThreadLock imgPixelsLock_;
   friend class MySequenceThread;
   int nComponents_;
   MySequenceThread * thd_;
   int mode_;
   ImgManipulator* imgManpl_;
};

class MySequenceThread : public MMDeviceThreadBase
{
   friend class CSciMeasure;
   enum { default_numImages=1, default_intervalMS = 100 };
   public:
      MySequenceThread(CSciMeasure* pCam);
      ~MySequenceThread();
      void Stop();
      void Start(long numImages, double intervalMs);
      bool IsStopped();
      void Suspend();
      bool IsSuspended();
      void Resume();
      double GetIntervalMs(){return intervalMs_;}                               
      void SetLength(long images) {numImages_ = images;}                        
      long GetLength() const {return numImages_;}
      long GetImageCounter(){return imageCounter_;}                             
      MM::MMTime GetStartTime(){return startTime_;}                             
      MM::MMTime GetActualDuration(){return actualDuration_;}
   private:                                                                     
      int svc(void) throw();
      double intervalMs_;                                                       
      long numImages_;                                                          
      long imageCounter_;                                                       
      bool stop_;                                                               
      bool suspend_;                                                            
      CSciMeasure* camera_;                                                     
      MM::MMTime startTime_;                                                    
      MM::MMTime actualDuration_;                                               
      MM::MMTime lastFrameTime_;                                                
      MMThreadLock stopLock_;                                                   
      MMThreadLock suspendLock_;                                                
}; 



#endif //_DEMOCAMERA_H_
