///////////////////////////////////////////////////////////////////////////////
// FILE:          SciMeasureDV2KCamera.cpp
// PROJECT:       Micro-Manager
// SUBSYSTEM:     DeviceAdapters
//-----------------------------------------------------------------------------
// DESCRIPTION:   The example implementation of the demo camera.
//                Simulates generic digital camera and associated automated
//                microscope devices and enables testing of the rest of the
//                system without the need to connect to the actual hardware. 
//                
// AUTHOR:        Nenad Amodaj, nenad@amodaj.com, 06/08/2005
//
// COPYRIGHT:     University of California, San Francisco, 2006
// LICENSE:       This file is distributed under the BSD license.
//                License text is included with the source distribution.
//
//                This file is distributed in the hope that it will be useful,
//                but WITHOUT ANY WARRANTY; without even the implied warranty
//                of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//                IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//                CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//                INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.

#include "edtinc.h"
#include "SciMeasureCamera.h"
#include <cstdio>
#include <string>
#include <math.h>
#include "../../MMDevice/ModuleInterface.h"
#include <sstream>
#include <algorithm>
#include "WriteCompactTiffRGB.h"
#include <iostream>
#include <omp.h>



using namespace std;
const double CSciMeasure::nominalPixelSizeUm_ = 1.0;
double g_IntensityFactor_ = 1.0;

// External names used used by the rest of the system
// to load particular device from the "SciMeasureDV2KCamera.dll" library
const char* g_CameraDeviceName = "SciMeasureDV2K";

// constants for naming pixel types (allowed values of the "PixelType" property)
//const char* g_PixelType_16bit = "16bit (14bit A/D)";
const char* g_PixelType_16bit = "16bit";

//EDT-pdv 
char cameraType[80];
char config_list[8][50];
char liveConfig_list[8][80];
char streamConfig_list[8][80];
double sm_lib_rates[8];
double sm_cam_lib_rates[8]; 
double sm_integration_increments[8];
int ccd_lib_bin[8], config_load_lib[8], cds_lib[8], ndr_lib[8];
int stripes_lib[8], layers_lib[8], rotate_lib[8];
int bad_pix_lib[8], offset_chan_order[32], cds_offset_chan[8];
int start_line_lib[8], super_frame_lib[8], NDR_start_lib[8], NDR_inc_lib[8], NDR_code_lib[8], NDR_max_lib[8];
int reserve1_lib[8], reserve2_lib[8], reserve3_lib[8];
int curConfig, curCamGain, curChipGain, fixedCamGain, NDR_num_f, NDR_pos, horizontal_bin;

#define MAXCHANNELS	4
int     unit[MAXCHANNELS] = { 0, 0, 1, 1 };
int     channel[MAXCHANNELS] = { 0, 1, 0, 1 };
int     overrun[MAXCHANNELS], overruns[MAXCHANNELS] = { 0,0,0,0 };
PdvDev *pdv_p[MAXCHANNELS] = { NULL,NULL,NULL,NULL };
int numChannels = 1, sm_config_width, sm_config_height;
unsigned short int *image_data = (unsigned short int *)NULL;
unsigned short int *cds_frame = (unsigned short int *)NULL;
unsigned short int *first_frame = (unsigned short int *)NULL;
unsigned short int *image_buffer = (unsigned short int *)NULL;
unsigned short int *tmp_data = (unsigned short int *)NULL;
u_char *data_ptr, *image_ptr, *stack_ptr;
unsigned int *lut = (unsigned int *)NULL;
int image_width, image_height, file_width, file_height, stripes, layers, factor, quadFlag, twoKFlag, cdsFlag, rotateFlag;
double frame_interval;
int sm_triggerMode, liveFlag;


///////////////////////////////////////////////////////////////////////////////
// Exported MMDevice API
///////////////////////////////////////////////////////////////////////////////

MODULE_API void InitializeModuleData()
{
   RegisterDevice(g_CameraDeviceName, MM::CameraDevice, "SciMeasure DaVinci2K CMOS Camera");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
   if (deviceName == 0)
      return 0;

   // decide which device class to create based on the deviceName parameter
   if (strcmp(deviceName, g_CameraDeviceName) == 0)
   {
      // create camera
      return new CSciMeasure();
   }

   // ...supplied name not recognized
   return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}

// Edt-pdv

int CSciMeasure::read_SM_Config()
{
	char configFname[MAX_PATH] = ".\\sm_config.dat";
	char *libFmt = "%lf %lf %lf %lf %lf %lf %lf %lf - %s\n";
	char *libFmt2 = "%d %d %d %d %d %d %d %d - %s\n";
	char *libFmt3 = "%x %x %x %x %x %x %x %x - %s\n";
    FILE *fp;
	char tempStr[256];
    int row=0, i;
    char buf[256];
	char *p;

	if (!(fp = fopen(configFname, "r")))
        return 0;
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
	if (p = strstr(tempStr, "*"))
		*p = '\0';
	strcpy(cameraType, tempStr);
	for (i = 0; i < 8; i++)
		cds_offset_chan[i] = -1;

	if (strstr(cameraType, "CCD39"))
		quadFlag = TRUE;
	else 
		quadFlag = FALSE;

    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
	while (strstr(tempStr,"*"))
		*(strstr(tempStr,"*")) = ' ';
	while ((strcmp(tempStr,"End Of Config List")) && row < 8) {
		strcpy(config_list[row],tempStr);
		if (!fgets(buf,255,fp))
		    goto exit;
		sscanf(buf,"%s",tempStr);
		while (strstr(tempStr,"*"))
			*(strstr(tempStr,"*")) = ' ';
		++row;
	}

	row = 0;
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
	while ((strcmp(tempStr,"End*Of*LiveConfig*List")) && row < 8) {
		strcpy(liveConfig_list[row],tempStr);
		if (!fgets(buf,255,fp))
		    goto exit;
		sscanf(buf,"%s",tempStr);
		++row;
	}
	row = 0;
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
	while ((strcmp(tempStr,"End*Of*StreamConfig*List")) && row < 8) {
		strcpy(streamConfig_list[row],tempStr);
		if (!fgets(buf,255,fp))
		    goto exit;
		sscanf(buf,"%s",tempStr);
		++row;
	}

	if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%d",&curConfig);
	if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%d",&curCamGain);
	if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%d",&curChipGain);
	if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%d",&fixedCamGain);

    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt,&sm_lib_rates[0],&sm_lib_rates[1],&sm_lib_rates[2],&sm_lib_rates[3],
		&sm_lib_rates[4],&sm_lib_rates[5],&sm_lib_rates[6],&sm_lib_rates[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt,&sm_cam_lib_rates[0],&sm_cam_lib_rates[1],&sm_cam_lib_rates[2],&sm_cam_lib_rates[3],
		&sm_cam_lib_rates[4],&sm_cam_lib_rates[5],&sm_cam_lib_rates[6],&sm_cam_lib_rates[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt,&sm_integration_increments[0],&sm_integration_increments[1],&sm_integration_increments[2],&sm_integration_increments[3],
		&sm_integration_increments[4],&sm_integration_increments[5],&sm_integration_increments[6],&sm_integration_increments[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&ccd_lib_bin[0],&ccd_lib_bin[1],&ccd_lib_bin[2],&ccd_lib_bin[3],&ccd_lib_bin[4],&ccd_lib_bin[5],&ccd_lib_bin[6],&ccd_lib_bin[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&config_load_lib[0],&config_load_lib[1],&config_load_lib[2],&config_load_lib[3],&config_load_lib[4],&config_load_lib[5],&config_load_lib[6],&config_load_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&cds_lib[0],&cds_lib[1],&cds_lib[2],&cds_lib[3],&cds_lib[4],&cds_lib[5],&cds_lib[6],&cds_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&ndr_lib[0],&ndr_lib[1],&ndr_lib[2],&ndr_lib[3],&ndr_lib[4],&ndr_lib[5],&ndr_lib[6],&ndr_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&stripes_lib[0],&stripes_lib[1],&stripes_lib[2],&stripes_lib[3],&stripes_lib[4],&stripes_lib[5],&stripes_lib[6],&stripes_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&layers_lib[0],&layers_lib[1],&layers_lib[2],&layers_lib[3],&layers_lib[4],&layers_lib[5],&layers_lib[6],&layers_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&rotate_lib[0],&rotate_lib[1],&rotate_lib[2],&rotate_lib[3],&rotate_lib[4],&rotate_lib[5],&rotate_lib[6],&rotate_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,libFmt2,&bad_pix_lib[0],&bad_pix_lib[1],&bad_pix_lib[2],&bad_pix_lib[3],&bad_pix_lib[4],&bad_pix_lib[5],&bad_pix_lib[6],&bad_pix_lib[7],tempStr);
    if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%s",tempStr);
	if (strcmp(tempStr, "End*Of*Lib*List")) {
		sscanf(buf,libFmt2,&start_line_lib[0],&start_line_lib[1],&start_line_lib[2],&start_line_lib[3],&start_line_lib[4],&start_line_lib[5],&start_line_lib[6],&start_line_lib[7],tempStr);
		if (!fgets(buf,255,fp))
			goto exit;
		sscanf(buf,libFmt2,&super_frame_lib[0],&super_frame_lib[1],&super_frame_lib[2],&super_frame_lib[3],&super_frame_lib[4],&super_frame_lib[5],&super_frame_lib[6],&super_frame_lib[7],tempStr);
		if (!fgets(buf,255,fp))
			goto exit;
		sscanf(buf,libFmt3,&NDR_start_lib[0],&NDR_start_lib[1],&NDR_start_lib[2],&NDR_start_lib[3],&NDR_start_lib[4],&NDR_start_lib[5],&NDR_start_lib[6],&NDR_start_lib[7],tempStr);
		if (!fgets(buf,255,fp))
			goto exit;
		sscanf(buf,libFmt3,&NDR_inc_lib[0],&NDR_inc_lib[1],&NDR_inc_lib[2],&NDR_inc_lib[3],&NDR_inc_lib[4],&NDR_inc_lib[5],&NDR_inc_lib[6],&NDR_inc_lib[7],tempStr);
		if (!fgets(buf,255,fp))
			goto exit;
		sscanf(buf,libFmt3,&NDR_code_lib[0],&NDR_code_lib[1],&NDR_code_lib[2],&NDR_code_lib[3],&NDR_code_lib[4],&NDR_code_lib[5],&NDR_code_lib[6],&NDR_code_lib[7],tempStr);
		if (!fgets(buf,255,fp))
			goto exit;
			sscanf(buf,"%s",tempStr);
		if (strcmp(tempStr, "End*Of*Lib*List")) {
			sscanf(buf,libFmt2,&NDR_max_lib[0],&NDR_max_lib[1],&NDR_max_lib[2],&NDR_max_lib[3],&NDR_max_lib[4],&NDR_max_lib[5],&NDR_max_lib[6],&NDR_max_lib[7],tempStr);
			if (!fgets(buf,255,fp))
				goto exit;
			sscanf(buf,libFmt2,&reserve1_lib[0],&reserve1_lib[1],&reserve1_lib[2],&reserve1_lib[3],&reserve1_lib[4],&reserve1_lib[5],&reserve1_lib[6],&reserve1_lib[7],tempStr);
			if (!fgets(buf,255,fp))
				goto exit;
			sscanf(buf,libFmt2,&reserve2_lib[0],&reserve2_lib[1],&reserve2_lib[2],&reserve2_lib[3],&reserve2_lib[4],&reserve2_lib[5],&reserve2_lib[6],&reserve2_lib[7],tempStr);
			if (!fgets(buf,255,fp))
				goto exit;
			sscanf(buf,libFmt2,&reserve3_lib[0],&reserve3_lib[1],&reserve3_lib[2],&reserve3_lib[3],&reserve3_lib[4],&reserve3_lib[5],&reserve3_lib[6],&reserve3_lib[7],tempStr);
			if (!fgets(buf,255,fp))
				goto exit;
			sscanf(buf,"%s",tempStr);
		}
	}
	if (!fgets(buf,255,fp))
        goto exit;
	sscanf(buf,"%d",&offset_chan_order[0]);
	if (offset_chan_order[0] >= 0) {
		for (int i = 1; i < layers_lib[0]*stripes_lib[0]*numChannels; i++) {
			if (!fgets(buf,255,fp))
				goto exit;
			sscanf(buf,"%d",&offset_chan_order[i]);
		}
	}

exit:
	fclose(fp);
	return 1;
}

void CSciMeasure::SM_serial_command(char *command)
{
	char ret_c[55];
	int toRead;

	strcat(command, "\r");
	pdv_serial_command(pdv_p[0], command);						
	pdv_serial_binary_command(pdv_p[0],"\r\n", 2);
	Sleep(20);
	toRead = pdv_serial_wait(pdv_p[0], 50, 55);
	pdv_serial_read(pdv_p[0], ret_c, toRead);
}

void CSciMeasure::get_config_size()
{
	char *p;
	char str[100], str2[100];

	cdsFlag = cds_lib[curConfig];
	stripes = stripes_lib[curConfig];
	layers = layers_lib[curConfig];
	rotateFlag = rotate_lib[curConfig];
	horizontal_bin = 1;
	if (twoKFlag) {
		stripes *= 2;
		layers *= 2;
		horizontal_bin = max(1, ccd_lib_bin[curConfig]/2);
	}
	factor = 1;

	if (p = strstr(config_list[curConfig], "- ")) {
		strcpy(str, (p+2));
		if (p = strstr(str, "x")) {
			strcpy(str2, (p+1));
			*p = '\0';
			file_width = atoi(str)*horizontal_bin;
			if (p = strstr(str2, "-"))
				*p = '\0';
			file_height = atoi(str2);
			if (strstr(cameraType, "DM2K")) {
				sm_config_width = file_width*(1+cdsFlag)/2;
				sm_config_height = file_height/2;
				image_width = sm_config_width*2;
				image_height = sm_config_height*2;
			}
			else {
				sm_config_width = (image_width = file_width);
				sm_config_height = (image_height = file_height);
			}
		}
	}
}

void CSciMeasure::load_pdv_cfg()
{
	char command[256];
	int i;
	FILE *fp;
	char home_drive[10], config_file[100];

	strcpy(home_drive, "C");
	sprintf(config_file, "%s:\\EDT\\pdv\\camera_config\\sm_cam_config.cfg", home_drive);
	if (fp = fopen(config_file, "w")) {
		for (i = numChannels - 1; i >= 0; --i)
			pdv_close(pdv_p[i]);
		fprintf(fp, "camera_class: \"SciMeasure Config\"\n");
		fprintf(fp, "cameratype: \"SciMeasure Config\"\n");
		fprintf(fp,"width: %d\n", 512);
		fprintf(fp,"height: %d\n", 512);
		fprintf(fp,"depth: 14\n");
		fprintf(fp,"extdepth: 14\n");
		fprintf(fp,"byteswap: 0\n");
		fprintf(fp,"continuous: 1\n");
		fprintf(fp,"serial_baud: 38400\n");
//		fprintf(fp,"method_header_type: IRIG2\n");	// enable the IRIG timestamp
	//	fprintf(fp,"irig_raw: 1\n");	// enable packed BCD timestamp
		fprintf(fp,"CL_DATA_PATH_NORM: 0D\n");
		fprintf(fp,"CL_CFG_NORM: 0A\n");
		fclose(fp);
	}

	for (i = 0; i < numChannels; i++) {
		sprintf(command, "%s:\\EDT\\pdv\\initcam -u pdv%d_%d -f %s:\\EDT\\pdv\\camera_config\\%s", home_drive, unit[i], channel[i], home_drive, "sm_cam_config.cfg");
		system(command);
	}
	if (strstr(cameraType, "DM2K")) {
		sprintf(command, "%s:\\EDT\\pdv\\setgap2k", home_drive);
		system(command);
	}
	else if (strstr(cameraType, "128X")) {
		if (ndr_lib[curConfig])
			sprintf(command, "%s:\\EDT\\pdv\\setgap_c3", home_drive);
		else
			sprintf(command, "%s:\\EDT\\pdv\\setgap_80", home_drive);
		system(command);
	}
}

void CSciMeasure::SM_set_trigger(int triggerM)
{
	char command[256];

	if (triggerM == 1)
		sprintf(command,"@FVI 0002");
	else
		sprintf(command,"@FVI 0000");
	SM_serial_command(command);

	if (triggerM == 2)
		if (ndr_lib[curConfig] == 1) 
			sprintf(command,"@TXC 5");
		else
			sprintf(command,"@TXC 1");
	else
		sprintf(command,"@TXC 0");
	SM_serial_command(command);

	sprintf(command,"@SEQ 1");
	SM_serial_command(command);
}

void CSciMeasure::SM_set_repeat(double frame_interval)
{
	char command[256];
	long repetitions;

	if (ndr_lib[curConfig]) {
		if (curConfig == 7)
			repetitions = max(0, ndr_lib[curConfig] - 1);
		else
			repetitions = max(0, ndr_lib[curConfig] - 2);
	}
	else {
		repetitions = (long)((frame_interval-1/sm_cam_lib_rates[curConfig])*1000/sm_integration_increments[curConfig] + 0.5);
		if (repetitions < 0)
			repetitions = 0;
		if (repetitions > 65534)
			repetitions = 65534;
	}
	sprintf(command,"@REP %ld",repetitions);
	SM_serial_command(command);
}

void CSciMeasure::SM_initCamera()
{
	char command[256];
	int prog;

	SM_pdv_open();
	get_config_size();
	for (int i = 0; i <numChannels; ++i) {
		pdv_p[i]->dd_p->continuous = 1;
		pdv_setsize(pdv_p[i], sm_config_width, sm_config_height);
	}

	if (curCamGain >= 0)
		prog = 8*curCamGain + curConfig;
	else
		prog = curConfig;
	sprintf(command,"@RCL %d",prog);
	SM_serial_command(command);

	sprintf(command,"@SEQ 0");
	SM_serial_command(command);

	if (strstr(cameraType, "DM2K")) {
		int hbin = 0;
		if (ccd_lib_bin[curConfig] > 1)
			hbin = 1;
		int start_line;			
		if (start_line_lib[curConfig])
			start_line = start_line_lib[curConfig];
		else
			start_line = 65 + (1024-sm_config_height*ccd_lib_bin[curConfig]);

		if (curConfig > 6) {
			sprintf(command,"@RCL?");
			SM_serial_command(command);
			Sleep(20);
		}

		sprintf(command,"@SPI 0; 2");
		SM_serial_command(command);
		Sleep(20);
		sprintf(command,"@SPI 0; 0");
		SM_serial_command(command);
		Sleep(20);
		sprintf(command,"@SPI 0; 4");
		SM_serial_command(command);
		Sleep(20);
		sprintf(command,"@PSR %d; %d", ccd_lib_bin[curConfig], start_line);
		SM_serial_command(command);
		Sleep(20);
		sprintf(command,"@SPI 0; %d", hbin);
		SM_serial_command(command);
		Sleep(20);
	}
	else if (strstr(cameraType, "128X-V2")) {
		int psr_ar[] = {1, 49, 57, 57, 1, 49, 57, 57};
		sprintf(command,"@PSR 1; %d", psr_ar[curConfig]);
		Sleep(50);
		SM_serial_command(command);
		Sleep(20);
	}

	SM_set_repeat(frame_interval);
//		if (!strstr(cameraType, "DM2K") && !strstr(cameraType, "128X"))
//		loadCamOffsets();
	sprintf(command,"@FVI 0000");
	SM_serial_command(command);
	sprintf(command,"@TXC 0");
	SM_serial_command(command);
	sprintf(command,"@SEQ 1");
	SM_serial_command(command);

	SM_pdv_close();
}

void CSciMeasure::SM_pdv_close()
{
	if (pdv_p[0] != NULL) {
		for (int i = numChannels - 1; i >= 0; --i) {
			pdv_close(pdv_p[i]);
			pdv_p[i] = NULL;
		}
	}
}

int CSciMeasure::SM_pdv_open()
{
	SM_pdv_close();
 	for (int i = 0; i < numChannels; ++i) {
		if ((pdv_p[i] = pdv_open_channel(EDT_INTERFACE, unit[i], channel[i])) == NULL)
			return FALSE;
		else {
			pdv_flush_fifo(pdv_p[i]);
		}
	}
	return TRUE;
}

int CSciMeasure::SM_check_camera()
{
	if (!read_SM_Config()) {
		MessageBoxA(NULL, "You need to have sm_config.dat in MM folder", "message", NULL);
		file_width = 0;
		file_height = 0;
		return FALSE;
	}

	if (strstr(cameraType, "DM2K")) {
		numChannels = MAXCHANNELS;
		twoKFlag = 1; 
	}
	else {
		numChannels = 1;
		twoKFlag = 0; 
	}

	load_pdv_cfg();
	sm_triggerMode = 0;
	NDR_num_f = 100;

	if (SM_pdv_open()) {
		SM_pdv_close();
		SM_initCamera();
		return TRUE;
	}
	else {
		file_width = (sm_config_width = 0);
		file_height = (sm_config_height = 0);
		MessageBoxA(NULL, "Cannot open the device", "message", NULL);
		return FALSE;
	}
}

int CSciMeasure::SM_snap(ImgBuffer& img)
{
	if (SM_pdv_open()) {
		SM_OneFrame(img);
		SM_pdv_close();
		return DEVICE_OK;
	}
	return DEVICE_ERR;
}

int CSciMeasure::setNDR_pos(int NDR_pos)
{
	if (ndr_lib[curConfig] == 1) {
		char command[256];
		if (!NDR_pos) {
			if (sm_triggerMode == 2)
				NDR_pos = NDR_start_lib[curConfig];
			else
				NDR_pos = NDR_start_lib[curConfig]+NDR_inc_lib[curConfig]*NDR_num_f;
			sprintf(command,"@MCR $%X; 1; %X", NDR_pos, (NDR_code_lib[curConfig] | 0x80));
		}
		else {
			int prog;
			if (curCamGain >= 0)
				prog = 8*curCamGain + curConfig;
			else
				prog = curConfig;
			sprintf(command,"@PRG %d",prog);
			SM_serial_command(command);
			sprintf(command,"@MCR $%X; 1; %X", NDR_pos, NDR_code_lib[curConfig]);
//			NDR_pos = 0;
		}
		SM_serial_command(command);
		Sleep(20);
		sprintf(command,"@SEQ 1");
		SM_serial_command(command);
	}
	return NDR_pos;
}

void CSciMeasure::SM_OneFrame(ImgBuffer& img)
{
	MMThreadGuard g(imgPixelsLock_);
	unsigned short* pBuf = (unsigned short*) const_cast<unsigned char*>(img.GetPixels());
	SM_acq_one();
	memcpy(pBuf, image_data, img_.Height()*img_.Width()*img_.Depth());
}

int CSciMeasure::SM_startCamera(int num_frames)
{
	if (img_.Height() == 0 || img_.Width() == 0 || img_.Depth() == 0)
		return 0; 

	int i, numbufs, loops;
	int file_length = file_width*file_height*2;
	int image_length = image_width*image_height*2;

	if (sm_triggerMode && (num_frames > 1)) {
		SM_set_trigger(sm_triggerMode);
//		MessageBoxA(NULL, "TTT", "message", NULL);
	}
	double exp = GetSequenceExposure();
	if (frame_interval != exp) {
		frame_interval = exp;
		SM_set_repeat(frame_interval);
	}
	if (num_frames > 1) {
		factor = min(num_frames, 10*(int)(1024/sm_config_height));
		for (i = 0; i <numChannels; ++i) {
			pdv_setsize(pdv_p[i], sm_config_width, sm_config_height*factor);
			pdv_set_timeout(pdv_p[i], 0);
		}
	}
	else
		factor = 1;
	loops = max((num_frames + factor - 1) / factor, 1) + cdsFlag;

	numbufs = min(10, loops);
	for (i = 0; i < numChannels; ++i) 
		pdv_multibuf(pdv_p[i], numbufs);

	if (image_data != NULL)
		_aligned_free(image_data);
	if ((image_data = (unsigned short *)_aligned_malloc(image_length*factor*2*(1+cdsFlag), 2)) == NULL) {
		MessageBoxA(NULL, "Not enough memory for image_data", "message", NULL);
		return 0;
	}
	if (cdsFlag && num_frames > 1) {
		if ((cds_frame = (unsigned short *)_aligned_malloc(image_length*2, 2)) == NULL) {
			MessageBoxA(NULL, "Not enough memory for cds_frame", "message", NULL);
			return 0;
		}
		if ((first_frame = (unsigned short *)_aligned_malloc(image_length*2, 2)) == NULL) {
			MessageBoxA(NULL, "Not enough memory for first_frame", "message", NULL);
			return 0;
		}
	}
	if ((lut = (unsigned int *)_aligned_malloc(image_length*2, 2)) == NULL) {
		MessageBoxA(NULL, "Not enough memory for lut", "message", NULL);
		return 0;
	}
	if ((image_buffer = (unsigned short *)_aligned_malloc(factor*file_length, 2)) == NULL) {
		MessageBoxA(NULL, "Not enough memory for image_buffer", "message", NULL);
		return 0;
	}

	makeLookUpTable(lut, image_width, image_height, file_width, file_height, stripes, layers, 1, quadFlag, cdsFlag, rotateFlag, twoKFlag);

	int num_start_frames;
	if (num_frames > 1 && !cdsFlag)
		num_start_frames = loops +1;
	else
		num_start_frames = loops;
	for (i = 0; i < numChannels; ++i)
		pdv_start_images(pdv_p[i], num_start_frames);

	return loops;
}

void CSciMeasure::SM_get_image(int loops)
{
//	char timestamp[32];
	int frame_len = sm_config_width*sm_config_height*factor*2;
	data_ptr = (u_char *)image_data;
	for (int k = 0; k < loops; k++) {
		for (int i = 0; i < numChannels; ++i) {
			image_ptr = pdv_wait_image_raw(pdv_p[i]);
			memcpy(data_ptr, image_ptr, frame_len);
	//		image_ptr += frame_len;
		//	memcpy(timestamp, image_ptr, 32);
			data_ptr += frame_len;
		}
	}
//		MessageBoxA(NULL, timestamp, "message", NULL);
}

void CSciMeasure::SM_wait_image()
{
	int frame_len = sm_config_width*sm_config_height*factor*2;
	data_ptr = (u_char *)image_data;
	for (int i = 0; i < numChannels; ++i) {
		image_ptr = pdv_wait_image_raw(pdv_p[i]);
		if ((overrun[i] = (edt_reg_read(pdv_p[i], PDV_STAT) & PDV_OVERRUN)))
			++overruns[i];
		memcpy(data_ptr, image_ptr, frame_len);
		data_ptr += frame_len;
	}
}

void CSciMeasure::SM_stopCamera()
{
	for (int i = 0; i < numChannels; ++i) {
		edt_disable_ring_buffers(pdv_p[i]);
		if (factor > 1) {
			pdv_setsize(pdv_p[i], sm_config_width, sm_config_height);
			pdv_set_timeout(pdv_p[i], 500);
		}
	}
	if (sm_triggerMode)
		SM_set_trigger(0);
}

void CSciMeasure::SM_2KMultiF_rearrange(int f_loops)
{ 
	int image_length = image_width*image_height*2;

	if ((tmp_data = (unsigned short *)_aligned_malloc(image_length*factor*(1 + cdsFlag), 2)) == NULL) {
		MessageBoxA(NULL, "Not enough memory for tmp_data", "message", NULL);
		return;
	}
	unsigned short int *sptr, *dptr, *sptr0;
	unsigned int quadrant_len = image_length / 8;
	unsigned long long q_block_len = (unsigned long long)quadrant_len*factor;
	sptr0 = image_data;
	sptr = sptr0;
	dptr = tmp_data;
	for (int k = 0; k < f_loops; ++k) {
		for (int i = 0; i < numChannels; ++i) {
			memcpy(dptr, sptr, quadrant_len*2);
			dptr += quadrant_len;
			sptr += q_block_len;
		}
		sptr -= q_block_len*numChannels - quadrant_len;
	}
	memcpy(sptr0, tmp_data, (unsigned long long)factor*image_length);
	sptr0 += ((unsigned long long)factor*image_length) >> 1;
}

void CSciMeasure::SM_rearrangeData(int num_frames)
{ 
	int loops;
	int file_length = file_width*file_height*2;
	int image_length = image_width*image_height*2;
	int final_file_length = cameraCCDXSize_*file_height*2;

	loops = num_frames;
	if (cdsFlag)
		++loops;
	factor = 1;
	if (cdsFlag) {
		subtractCDS(image_data, loops, image_width, image_height, image_length, factor, quadFlag, twoKFlag, 0);
	}
	if (cdsFlag)
		--loops;
	data_ptr = (u_char *)image_data;
	u_char *final_data_ptr = (u_char *)image_data;
	for (int j = 0; j < loops; ++j) {
		frame_deInterleave(file_length>>1, lut, (unsigned short *)data_ptr, image_buffer);
		if (twoKFlag) {
			if (horizontal_bin > 1) {
				unsigned short int *sptr, *dptr;
				sptr = image_buffer;
				dptr = image_buffer;
				unsigned int l, m, k;
				unsigned long bin_val;
				for (m = 0; m < file_height; m++) {
					for (l = 0; l < file_width / horizontal_bin; l++) {
						bin_val = 0;
						for (k = 0; k < horizontal_bin; k++)
							bin_val += (unsigned long)*sptr++;
						*dptr++ = (unsigned short)(bin_val / horizontal_bin);
					}
				}
			}
			if (cameraCCDXSize_ != file_width/horizontal_bin) {
				unsigned short int *sptr, *dptr;
				sptr = image_buffer + (file_width/horizontal_bin - cameraCCDXSize_) / 2;
				dptr = image_buffer;
				int l, m;
				for (m = 0; m < file_height; m++) {
					for (l = 0; l < cameraCCDXSize_; l++)
						*dptr++ = *sptr++;
					sptr += file_width/horizontal_bin-cameraCCDXSize_;
				}
			}
		}
		memcpy(final_data_ptr, image_buffer, final_file_length);
		final_data_ptr += final_file_length;
		data_ptr += image_length;
	}
}

void CSciMeasure::clearSMbuffer()
{
	if (first_frame)
		_aligned_free(first_frame);
	if (cds_frame)
		_aligned_free(cds_frame);
	if (image_buffer)
		_aligned_free(image_buffer);
	if (lut)
		_aligned_free(lut);
	if (tmp_data)
		_aligned_free(tmp_data);
	first_frame = NULL;
	cds_frame = NULL;
	image_buffer = NULL;
	lut = NULL;
	tmp_data = NULL;
}


void CSciMeasure::SM_acq_one()
{	
	int num_frames = 1;
	int loops;

	loops = SM_startCamera(num_frames);

	if (loops)
		SM_get_image(loops);
	else
		return;

	SM_stopCamera();

	SM_rearrangeData(num_frames);

	clearSMbuffer();
}

void CSciMeasure::subtractCDS(unsigned short int *image_data, unsigned int loops, unsigned int width, unsigned int height, unsigned int length, int factor, int QUAD, int TWOK, int transitionFlag)
{
	int l, m, n, row, col, doublecols=0, rows=0, cols=0;
	unsigned short int *new_data, *old_data, *reset_data; //, *first_frame;

	if (TWOK) {
		if (factor == 1)
			rows = height * 4;
		else
			rows = (height + 2) / (2*factor) - 2;
		cols = width / 4;
		doublecols = cols * 2;
		if (transitionFlag) {
			new_data = first_frame;
			reset_data = cds_frame + cols;
			old_data = first_frame;
			for (row = rows; row; --row) {
				for (col = cols; col; --col) {
					*new_data++ = 1024 + *old_data++ - *reset_data++;
				}
				new_data += cols;
				old_data += cols;
				reset_data += cols;
			}
			return;
		}
		else {
			new_data = image_data;
			reset_data = image_data + cols;
			if (factor == 1)
				old_data = image_data + length / 2;
			else
				old_data = image_data + (rows + 2)*doublecols;
		}
	}
	else if (QUAD) {
		rows = height / 2;
		cols = width;
		new_data = image_data;
		reset_data = image_data + width;
		old_data = image_data + length / 2;
	}
	else {
		rows = height;
		cols = width / 2;
		new_data = image_data;
		reset_data = image_data + cols;
		old_data = image_data + length / 2;
	}

	if (factor == 1) {
		for (--loops, m = 1; loops > 0; --loops, ++m) {
			for (row = rows; row; --row) {
				for (col = cols; col; --col) {
					*new_data++ = 1024 + *old_data++ - *reset_data++;
				}
				new_data += cols;
				old_data += cols;
				reset_data += cols;
			}
		}
	}
	else {
		for (m = 1; loops; --loops, m += factor) {
			for (n = 4; n; --n) {
				for (l = factor - 1; l; --l, ++m) {
					for (row = rows; row; --row) {
						for (col = cols; col; --col) 
							*new_data++ = 1024 + *old_data++ - *reset_data++;
						new_data += cols;
						old_data += cols;
						reset_data += cols;
					}
					new_data += doublecols * 2;
					old_data += doublecols * 2;
					reset_data += doublecols * 2;
				}
				if (loops > 0) {
					old_data += width * 3 * height / 4 - doublecols;
					for (row = rows; row; --row) {
						for (col = cols; col; --col)
							*new_data++ = 1024 + *old_data++ - *reset_data++;
						new_data += cols;
						old_data += cols;
						reset_data += cols;
					}
					old_data -= width * 3 * height / 4 - doublecols;
					new_data += doublecols;
					old_data += doublecols;
					reset_data += doublecols;
					++m;
					m -= factor;
				}
			}
		}
	}
}
int CSciMeasure::makeLookUpTable(unsigned int *Lut, int image_width, int image_height, int file_width, int file_height, int stripes, int layers, int factor, int QUAD, int CDS, int ROTATE, int TWOK)
{
	int SEGS, file_offset, image_offset, frame_length, image_length, file_length;
	int frame, segment, row, rowIndex, destIndex, rows, cols, swid, dwid, srcIndex;
	static int twokchannel[] = { 3,2,0,1,7,6,4,5,9,8,10,11,13,12,14,15 };
	int *channelOrder = NULL;

	SEGS = stripes*layers;
	image_length = image_width*image_height;
	file_length = file_width*file_height;
	rows = file_height/layers;
	if (TWOK)
		channelOrder = twokchannel;
	swid = image_width / stripes;
	dwid = file_width / stripes;

	if (factor == 10) {
		if (TWOK) {
			frame_length = image_width*(file_height+4)/4;
			for (frame = 0; frame < factor; ++frame) {
				image_offset = frame*frame_length;
				file_offset = frame*file_length;
				for (segment = 0; segment < stripes; ++segment) {
					srcIndex = image_offset + channelOrder[segment] % 4 + (image_length / 4)*(channelOrder[segment] / 4);
					for (row = rows, rowIndex = file_offset; row; --row, rowIndex += file_width) {
						for (destIndex = rowIndex + dwid*segment, cols = dwid; cols > 0; --cols, ++destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS / 4;
						}
						if (CDS)
							srcIndex += dwid*SEGS / 4;
					}
				}
				for (; segment < SEGS; ++segment) {
					srcIndex = image_offset + channelOrder[segment] % 4 + (image_length / 4)*(channelOrder[segment] / 4);
					for (row = rows, rowIndex = file_offset + file_length - file_width; row; --row, rowIndex -= file_width) {
						for (destIndex = rowIndex + dwid*(segment - SEGS / 2), cols = dwid; cols > 0; --cols, ++destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS / 4;
						}
						if (CDS)
							srcIndex += dwid*SEGS / 4;
					}
				}
			}
		}
	}
	else if (TWOK) {
		omp_set_num_threads(omp_get_num_procs());

#pragma omp parallel
	{
		int row, cols, segment, rowIndex, srcIndex, destIndex;

#pragma omp parallel for private(row,cols,segment,rowIndex,srcIndex,destIndex)
		for (segment = 0; segment < stripes; ++segment) {
			srcIndex = channelOrder[segment] % 4 + (image_length / 4)*(channelOrder[segment] / 4);
			for (row = 0, rowIndex = 0; row < rows; ++row, rowIndex += file_width) {
				for (destIndex = rowIndex + dwid*segment, cols = dwid; cols > 0; --cols, ++destIndex) {
					Lut[destIndex] = srcIndex;
					srcIndex += SEGS / 4;
				}
				if (CDS)
					srcIndex += dwid*SEGS / 4;
			}
		}
#pragma omp parallel for private(row,cols,segment,rowIndex,srcIndex,destIndex)
		for (segment = stripes; segment < SEGS; ++segment) {
			srcIndex = channelOrder[segment] % 4 + (image_length / 4)*(channelOrder[segment] / 4);
			for (row = 0, rowIndex = file_length - file_width; row < rows; ++row, rowIndex -= file_width) {
				for (destIndex = rowIndex + dwid*(segment - SEGS / 2), cols = dwid; cols > 0; --cols, ++destIndex) {
					Lut[destIndex] = srcIndex;
					srcIndex += SEGS / 4;
				}
				if (CDS)
					srcIndex += dwid*SEGS / 4;
			}
		}
	}
	}
	else {
		if (CDS) {
			swid = swid / 2;
			image_width = image_width / 2;
			image_length = image_length / 2;
		}
		if (layers == 2) {
			// TOP LAYER
			if (QUAD) {
				for (segment = 0; segment < SEGS; segment += 4) {
					srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
					for (row = 0, rowIndex = 0; row < rows; ++row, rowIndex += image_width) {
						for (destIndex = rowIndex + (segment / 4) * 2 * swid, cols = swid; cols > 0; --cols, ++destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS;
						}
						if (CDS)
							srcIndex += swid*SEGS;
					}
				}
				for (segment = 1; segment < SEGS; segment += 4) {
					srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
					for (row = 0, rowIndex = 0; row < rows; ++row, rowIndex += image_width) {
						for (destIndex = rowIndex + (segment / 4 + 1) * 2 * swid - 1, cols = swid; cols > 0; --cols, --destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS;
						}
						if (CDS)
							srcIndex += swid*SEGS;
					}
				}
				for (segment = 2; segment < SEGS; segment += 4) {
					srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
					for (row = 0, rowIndex = image_length - image_width; row < rows; ++row, rowIndex -= image_width) {
						for (destIndex = rowIndex + (segment / 4) * 2 * swid, cols = swid; cols > 0; --cols, ++destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS;
						}
						if (CDS)
							srcIndex += swid*SEGS;
					}
				}
				for (segment = 3; segment < SEGS; segment += 4) {
					srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
					for (row = 0, rowIndex = image_length - image_width; row < rows; ++row, rowIndex -= image_width) {
						for (destIndex = rowIndex + ((segment / 4) + 1) * 2 * swid - 1, cols = swid; cols > 0; --cols, --destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS;
						}
						if (CDS)
							srcIndex += swid*SEGS;
					}
				}
			}
			else
				for (segment = 1; segment < SEGS; segment += 2) {
					srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
					for (row = rows - 1, rowIndex = (rows - 1)*image_width; row >= rows; --row, rowIndex -= image_width) {
						for (destIndex = rowIndex + (segment / 2)*swid, cols = swid; cols > 0; --cols, ++destIndex) {
							Lut[destIndex] = srcIndex;
							srcIndex += SEGS;
						}
					}
				}
		}
		else { // LAYERS != 2
			if (SEGS == 20) {
				if (ROTATE) { // rotate image
					for (segment = 0; segment < SEGS; ++segment) {
						srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
						for (row = 0, rowIndex = image_height*image_width; row < rows; ++row, rowIndex -= image_width) {
							for (destIndex = rowIndex - segment*swid - 1, cols = swid; cols > 0; --cols, --destIndex) {
								Lut[destIndex] = srcIndex;
								srcIndex += SEGS;
							}
						}
					}
				}
				else {
					for (segment = 0; segment < SEGS; ++segment) {
						srcIndex = (channelOrder == NULL) ? segment : channelOrder[segment];
						for (row = 0, rowIndex = 0; row < rows; ++row, rowIndex += image_width) {
							for (destIndex = rowIndex + segment*swid, cols = swid; cols > 0; --cols, ++destIndex) {
								Lut[destIndex] = srcIndex;
								srcIndex += SEGS;
							}
						}
					}
				}
			}
			else {
				if (ROTATE) { // rotate image
					for (segment = 0; segment < SEGS; ++segment) {
						srcIndex = 4 * (segment % 4) + segment / 4; // DM0018 segment mapping for 16 channels
						for (row = 0, rowIndex = image_height*image_width; row < rows; ++row, rowIndex -= image_width) {
							for (destIndex = rowIndex - segment*swid - 1, cols = swid; cols > 0; --cols, --destIndex) {
								Lut[destIndex] = srcIndex;
								srcIndex += SEGS;
							}
						}
					}
				}
				else {
					srcIndex = 0;
					destIndex = 0;
					if (SEGS == 2) {
						for (row = 0; row < rows; ++row) {
							for (cols = swid; cols > 0; --cols, ++destIndex) {
								Lut[destIndex] = srcIndex;
								srcIndex += SEGS;
							}
							srcIndex -= image_width - 1;
							destIndex += swid-1;
							for (cols = swid; cols > 0; --cols, --destIndex) {
								Lut[destIndex] = srcIndex;
								srcIndex += SEGS;
							}
							srcIndex -= SEGS - 1;
							destIndex += swid+1;
						}
					}
					else {
						for (row = 0; row < rows; ++row) {
							for (segment = 0; segment < SEGS; ++segment) {
								for (cols = swid; cols > 0; --cols, ++destIndex) {
									Lut[destIndex] = srcIndex;
									srcIndex += SEGS;
								}
								srcIndex -= image_width - 1;
							}
							srcIndex += image_width*(1 + CDS) - SEGS;
						}
					}
				}
			}
		}
	}
	return 0;
}

void CSciMeasure::frame_deInterleave(int length, unsigned *lookuptable, unsigned short *old_images, unsigned short *new_images)
{
/*
	for (int i = 0; i < length; ++i)
		*(new_images++) = old_images[*(lookuptable++)];
*/
	int threads;

	omp_set_num_threads(omp_get_num_procs());
	threads = omp_get_num_threads();

#pragma omp parallel
{
	int id;
	unsigned *lut = lookuptable;
	unsigned short int *new_image = new_images, *old_image = old_images;

	id = omp_get_thread_num();
	new_image += id;
	lut += id;
	for (int i = id; i < length; i += threads) {
		*new_image = old_image[*lut];
		new_image += threads;
		lut += threads;
	}
}
}
///////////////////////////////////////////////////////////////////////////////
// CSciMeasure implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
* CSciMeasure constructor.
* Setup default all variables and create device properties required to exist
* before intialization. In this case, no such properties were required. All
* properties will be created in the Initialize() method.
*
* As a general guideline Micro-Manager devices do not access hardware in the
* the constructor. We should do as little as possible in the constructor and
* perform most of the initialization in the Initialize() method.
*/
CSciMeasure::CSciMeasure() :
	CCameraBase<CSciMeasure> (),
	exposureMaximum_(1000.0),
	initialized_(false),
	readoutUs_(0.0),
	bitDepth_(16),
	roiX_(0),
	roiY_(0),
	sequenceStartTime_(0),
	isSequenceable_(false),
	sequenceMaxLength_(100),
	sequenceRunning_(false),
	sequenceIndex_(0),
	binSize_(1),
	cameraCCDXSize_(512),
	cameraCCDYSize_(512),
	stopOnOverflow_(false),
	shouldRotateImages_(false),
	shouldDisplayImageNumber_(false),
	nComponents_(1)	//necessary in order not to get RGB16bit error message
{
   // call the base class method to set-up default error codes/messages
   InitializeDefaultErrorMessages();
   readoutStartTime_ = GetCurrentMMTime();
   thd_ = new MySequenceThread(this);
}

/**
* CSciMeasure destructor.
* If this device used as intended within the Micro-Manager system,
* Shutdown() will be always called before the destructor. But in any case
* we need to make sure that all resources are properly released even if
* Shutdown() was not called.
*/
CSciMeasure::~CSciMeasure()
{
   StopSequenceAcquisition();
   delete thd_;
}

/**
* Obtains device name.
* Required by the MM::Device API.
*/
void CSciMeasure::GetName(char* name) const
{
   // Return the name used to referr to this device adapte
   CDeviceUtils::CopyLimitedString(name, g_CameraDeviceName);
}

/**
* Intializes the hardware.
* Required by the MM::Device API.
* Typically we access and initialize hardware at this point.
* Device properties are typically created here as well, except
* the ones we need to use for defining initialization parameters.
* Such pre-initialization properties are created in the constructor.
* (This device does not have any pre-initialization properties)
*/

int CSciMeasure::Initialize()
{
   if (initialized_)
      return DEVICE_OK;

   SciMeasureHub* pHub = static_cast<SciMeasureHub*>(GetParentHub());
   if (pHub)
   {
      char hubLabel[MM::MaxStrLength];
      pHub->GetLabel(hubLabel);
      SetParentID(hubLabel); // for backward comp.
   }
   else
      LogMessage(NoHubError);

   if (!SM_check_camera())
	   return DEVICE_ERR;
   cameraCCDXSize_ = file_width;
   cameraCCDYSize_ = file_height;

   // set property list
   // -----------------

   // Name
   int nRet = CreateStringProperty(MM::g_Keyword_Name, g_CameraDeviceName, true);
   if (DEVICE_OK != nRet)
      return nRet;

   // Description
   nRet = CreateStringProperty(MM::g_Keyword_Description, "SciMeasure Camera Device Adapter", true);
   if (DEVICE_OK != nRet)
      return nRet;

   // CameraName
   nRet = CreateStringProperty(MM::g_Keyword_CameraName, "SciMeasureDV2KCamera", true);
   assert(nRet == DEVICE_OK);

   // CameraID
   nRet = CreateStringProperty(MM::g_Keyword_CameraID, "DK2K-V1", true);
   assert(nRet == DEVICE_OK);

   // binning
   CPropertyAction *pAct = new CPropertyAction (this, &CSciMeasure::OnBinning);
   nRet = CreateIntegerProperty(MM::g_Keyword_Binning, ccd_lib_bin[curConfig], true, pAct);
   assert(nRet == DEVICE_OK);

	// pixel type
	pAct = new CPropertyAction (this, &CSciMeasure::OnPixelType);
	nRet = CreateStringProperty(MM::g_Keyword_PixelType, g_PixelType_16bit, true, pAct);
	assert(nRet == DEVICE_OK);

   // exposure
	frame_interval = 1.0/sm_lib_rates[curConfig];
	nRet = CreateFloatProperty(MM::g_Keyword_Exposure, frame_interval, false);
	assert(nRet == DEVICE_OK);
	SetPropertyLimits(MM::g_Keyword_Exposure, 0.0, exposureMaximum_);

    // operational mode: 8 sequences
	pAct = new CPropertyAction (this, &CSciMeasure::OnReadoutSequence);
	CreateProperty(g_ReadoutPropertyName, config_list[0], MM::String, false, pAct);
	for (int i = 0; i < 8; ++i) {
		if (!strcmp(config_list[i], ""))
			break;
		if (!strstr(config_list[i], "!"))
			AddAllowedValue(g_ReadoutPropertyName, config_list[i]);
	}
	
	// CMOS readout size 
	pAct = new CPropertyAction (this, &CSciMeasure::OnCameraXSize);
	CreateIntegerProperty("CameraXSize", 512, false, pAct);
	pAct = new CPropertyAction (this, &CSciMeasure::OnCameraYSize);
	CreateIntegerProperty("CameraYSize", 512, true, pAct);

	// NDR frame cycle 
	pAct = new CPropertyAction (this, &CSciMeasure::OnNDRFrames);
	CreateIntegerProperty(g_NDRCycleName, 512, false, pAct);
 
	// Trigger device
	pAct = new CPropertyAction (this, &CSciMeasure::OnTriggerMode);
	CreateStringProperty(g_TriggerName, triggerMode0, false, pAct);
	AddAllowedValue(g_TriggerName, triggerMode0);
	AddAllowedValue(g_TriggerName, triggerMode1);
	AddAllowedValue(g_TriggerName, triggerMode2);

   // synchronize all properties
   // --------------------------
   nRet = UpdateStatus();
   if (nRet != DEVICE_OK)
      return nRet;


   // setup the buffer
   // ----------------
   nRet = ResizeImageBuffer();
   if (nRet != DEVICE_OK)
      return nRet;

#ifdef TESTRESOURCELOCKING
   TestResourceLocking(true);
   LogMessage("TestResourceLocking OK",true);
#endif


   initialized_ = true;




   // initialize image buffer
   GenerateEmptyImage(img_);
   return DEVICE_OK;


}

/**
* Shuts down (unloads) the device.
* Required by the MM::Device API.
* Ideally this method will completely unload the device and release all resources.
* Shutdown() may be called multiple times in a row.
* After Shutdown() we should be allowed to call Initialize() again to load the device
* without causing problems.
*/
int CSciMeasure::Shutdown()
{
   initialized_ = false;
   return DEVICE_OK;
}

/**
* Performs exposure and grabs a single image.
* This function should block during the actual exposure and return immediately afterwards 
* (i.e., before readout).  This behavior is needed for proper synchronization with the shutter.
* Required by the MM::Camera API.
*/
int CSciMeasure::SnapImage()
{
	return SM_snap(img_);
}

/**
* Returns pixel data.
* Required by the MM::Camera API.
* The calling program will assume the size of the buffer based on the values
* obtained from GetImageBufferSize(), which in turn should be consistent with
* values returned by GetImageWidth(), GetImageHight() and GetImageBytesPerPixel().
* The calling program allso assumes that camera never changes the size of
* the pixel buffer on its own. In other words, the buffer can change only if
* appropriate properties are set (such as binning, pixel type, etc.)
*/
const unsigned char* CSciMeasure::GetImageBuffer()
{
   MMThreadGuard g(imgPixelsLock_);
   MM::MMTime readoutTime(readoutUs_);
   while (readoutTime > (GetCurrentMMTime() - readoutStartTime_)) {}		
   unsigned char *pB = (unsigned char*)(img_.GetPixels());
   return pB;
}

/**
* Returns image buffer X-size in pixels.
* Required by the MM::Camera API.
*/
unsigned CSciMeasure::GetImageWidth() const
{
   return img_.Width();
}

/**
* Returns image buffer Y-size in pixels.
* Required by the MM::Camera API.
*/
unsigned CSciMeasure::GetImageHeight() const
{
   return img_.Height();
}

/**
* Returns image buffer pixel depth in bytes.
* Required by the MM::Camera API.
*/
unsigned CSciMeasure::GetImageBytesPerPixel() const
{
   return img_.Depth();
} 

/**
* Returns the bit depth (dynamic range) of the pixel.
* This does not affect the buffer size, it just gives the client application
* a guideline on how to interpret pixel values.
* Required by the MM::Camera API.
*/
unsigned CSciMeasure::GetBitDepth() const
{
   return bitDepth_;
}

/**
* Returns the size in bytes of the image buffer.
* Required by the MM::Camera API.
*/
long CSciMeasure::GetImageBufferSize() const
{
   return img_.Width() * img_.Height() * GetImageBytesPerPixel();
}

/**
* Sets the camera Region Of Interest.
* Required by the MM::Camera API.
* This command will change the dimensions of the image.
* Depending on the hardware capabilities the camera may not be able to configure the
* exact dimensions requested - but should try do as close as possible.
* If the hardware does not have this capability the software should simulate the ROI by
* appropriately cropping each frame.
* This demo implementation ignores the position coordinates and just crops the buffer.
* @param x - top-left corner coordinate
* @param y - top-left corner coordinate
* @param xSize - width
* @param ySize - height
*/
int CSciMeasure::SetROI(unsigned x, unsigned y, unsigned xSize, unsigned ySize)
{
   if (xSize == 0 && ySize == 0)
   {
      // effectively clear ROI
      ResizeImageBuffer();
      roiX_ = 0;
      roiY_ = 0;
   }
   else
   {
      // apply ROI
      img_.Resize(xSize, ySize);
      roiX_ = x;
      roiY_ = y;
   }
   return DEVICE_OK;
}

/**
* Returns the actual dimensions of the current ROI.
* Required by the MM::Camera API.
*/
int CSciMeasure::GetROI(unsigned& x, unsigned& y, unsigned& xSize, unsigned& ySize)
{
   x = roiX_;
   y = roiY_;

   xSize = img_.Width();
   ySize = img_.Height();

   return DEVICE_OK;
}

/**
* Resets the Region of Interest to full frame.
* Required by the MM::Camera API.
*/
int CSciMeasure::ClearROI()
{
   ResizeImageBuffer();
   roiX_ = 0;
   roiY_ = 0;
      
   return DEVICE_OK;
}

/**
* Returns the current exposure setting in milliseconds.
* Required by the MM::Camera API.
*/
double CSciMeasure::GetExposure() const
{
   char buf[MM::MaxStrLength];
   int ret = GetProperty(MM::g_Keyword_Exposure, buf);
   if (ret != DEVICE_OK)
      return 0.0;
   return atof(buf);
}

/**
 * Returns the current exposure from a sequence and increases the sequence counter
 * Used for exposure sequences
 */
double CSciMeasure::GetSequenceExposure() 
{
   if (exposureSequence_.size() == 0) 
      return this->GetExposure();

   double exposure = exposureSequence_[sequenceIndex_];

   sequenceIndex_++;
   if (sequenceIndex_ >= exposureSequence_.size())
      sequenceIndex_ = 0;

   return exposure;
}

/**
* Sets exposure in milliseconds.
* Required by the MM::Camera API.
*/
void CSciMeasure::SetExposure(double exp)
{
	if (exp < 1.0/sm_lib_rates[curConfig])
		exp = 1.0/sm_lib_rates[curConfig];
	SetProperty(MM::g_Keyword_Exposure, CDeviceUtils::ConvertToString(exp));
//	GetCoreCallback()->OnExposureChanged(this, exp);;
}

/**
* Returns the current binning factor.
* Required by the MM::Camera API.
*/
int CSciMeasure::GetBinning() const
{
   char buf[MM::MaxStrLength];
   int ret = GetProperty(MM::g_Keyword_Binning, buf);
   if (ret != DEVICE_OK)
      return 1;
   return atoi(buf);
}

/**
* Sets binning factor.
* Required by the MM::Camera API.
*/
int CSciMeasure::SetBinning(int binF)
{
   return SetProperty(MM::g_Keyword_Binning, CDeviceUtils::ConvertToString(binF));
}


/**
 * Required by the MM::Camera API
 * Please implement this yourself and do not rely on the base class implementation
 * The Base class implementation is deprecated and will be removed shortly
 */
int CSciMeasure::StartSequenceAcquisition(double interval)
{
   return StartSequenceAcquisition(LONG_MAX, interval, false);            
}

/**                                                                       
* Stop and wait for the Sequence thread finished                                   
*/                                                                        
int CSciMeasure::StopSequenceAcquisition()                                     
{
	if (!thd_->IsStopped()) {
		thd_->Stop();                                                       
		thd_->wait(); 
	}                                                                      

	if (pdv_p[0] != NULL) {
		if (!liveFlag) {
			SM_stopCamera();
			clearSMbuffer();
			if (NDR_pos)
				NDR_pos = setNDR_pos(NDR_pos);
		}
	SM_pdv_close();
	}
	return DEVICE_OK;                                                      
} 

/**
* Simple implementation of Sequence Acquisition
* A sequence acquisition should run on its own thread and transport new images
* coming of the camera into the MMCore circular buffer.
*/
int CSciMeasure::StartSequenceAcquisition(long numImages, double interval_ms, bool stopOnOverflow)
{
	if (IsCapturing())
		return DEVICE_CAMERA_BUSY_ACQUIRING;

	stack_ptr = NULL;
	SM_pdv_open();
	if (numImages < 200000)
	{
		NDR_pos = setNDR_pos(0);
		SM_startCamera(numImages);
		liveFlag = 0;
	}
	else {
		sequenceStartTime_ = GetCurrentMMTime();
		liveFlag = 1;
	}

	int ret = GetCoreCallback()->PrepareForAcq(this);
	if (ret != DEVICE_OK)
		return ret;
	imageCounter_ = 0;
	thd_->Start(numImages,interval_ms);
	stopOnOverflow_ = stopOnOverflow;
	return DEVICE_OK;
}

/*
 * Inserts Image and MetaData into MMCore circular Buffer
 */
int CSciMeasure::InsertImage()
{
	Metadata md;

	char label[MM::MaxStrLength];
	this->GetLabel(label);
 	md.put("Camera", label);
	md.put(MM::g_Keyword_Metadata_StartTime, CDeviceUtils::ConvertToString(sequenceStartTime_.getMsec()));
	if (liveFlag)
		md.put(MM::g_Keyword_Elapsed_Time_ms, CDeviceUtils::ConvertToString((this->GetCurrentMMTime() - sequenceStartTime_).getMsec()));
	else
		md.put(MM::g_Keyword_Elapsed_Time_ms, CDeviceUtils::ConvertToString(GetSequenceExposure()*imageCounter_));
	md.put(MM::g_Keyword_Metadata_ROI_X, CDeviceUtils::ConvertToString( (long) roiX_)); 
	md.put(MM::g_Keyword_Metadata_ROI_Y, CDeviceUtils::ConvertToString( (long) roiY_)); 

	imageCounter_++;

	MMThreadGuard g(imgPixelsLock_);

   const unsigned char* pI;
   pI = GetImageBuffer();

   unsigned int w = GetImageWidth();
   unsigned int h = GetImageHeight();
   unsigned int b = GetImageBytesPerPixel();

   int ret = GetCoreCallback()->InsertImage(this, pI, w, h, b, md.Serialize().c_str());
   if (!stopOnOverflow_ && ret == DEVICE_BUFFER_OVERFLOW)
   {
      // do not stop on overflow - just reset the buffer
      GetCoreCallback()->ClearImageBuffer(this);
      // don't process this same image again...
      return GetCoreCallback()->InsertImage(this, pI, w, h, b, md.Serialize().c_str(), false);
   }
   else
   {
      return ret;
   }
}

/*
 * Do actual capturing
 * Called from inside the thread  
 */
int CSciMeasure::RunSequenceOnThread(MM::MMTime startTime)
{
	int ret=DEVICE_ERR;
 
	if (liveFlag) {
		SM_OneFrame(img_);
	}
	else {
		int image_len = image_width*image_height*2;
		if (imageCounter_ == 0) {
			sequenceStartTime_ = GetCurrentMMTime();
		}
		if (!(imageCounter_ % factor)) {
			SM_wait_image();
			if (twoKFlag && factor > 1)
				SM_2KMultiF_rearrange(factor);
			if (imageCounter_ && cdsFlag) {
				int file_length = file_width*file_height*2;
				int final_file_length = cameraCCDXSize_*file_height*2;
				memcpy(first_frame, image_data, image_len);
				subtractCDS(image_data, 1, image_width, image_height, image_len, 1, quadFlag, twoKFlag, 1);
				frame_deInterleave(file_length>>1, lut, (unsigned short *)first_frame, image_buffer);
				if (twoKFlag) {
					if (horizontal_bin > 1) {
						unsigned short int *sptr, *dptr;
						sptr = image_buffer;
						dptr = image_buffer;
						unsigned int l, m, k;
						unsigned long bin_val;
						for (m = 0; m < file_height; m++) {
							for (l = 0; l < file_width / horizontal_bin; l++) {
								bin_val = 0;
								for (k = 0; k < horizontal_bin; k++)
									bin_val += (unsigned long)*sptr++;
								*dptr++ = (unsigned short)(bin_val / horizontal_bin);
							}
						}
					}
					if (cameraCCDXSize_ != file_width/horizontal_bin) {
						unsigned short int *sptr, *dptr;
						sptr = image_buffer + (file_width/horizontal_bin - cameraCCDXSize_) / 2;
						dptr = image_buffer;
						int l, m;
						for (m = 0; m < file_height; m++) {
							for (l = 0; l < cameraCCDXSize_; l++)
								*dptr++ = *sptr++;
							sptr += file_width/horizontal_bin-cameraCCDXSize_;
						}
					}
				}
				memcpy(first_frame, image_buffer, final_file_length);
			}
			if (cdsFlag) {
				u_char *pre_ptr = (u_char *)image_data + image_len*(factor-1);
				memcpy(cds_frame, pre_ptr, image_len);
			}
			int factor_sv = factor;
			int image_width_sv = image_width;
			SM_rearrangeData(factor);
			image_width = image_width_sv;
			factor = factor_sv;
			stack_ptr = (u_char *)image_data;
		}
		if (stack_ptr) {
			unsigned short* pBuf = (unsigned short*) const_cast<unsigned char*>(img_.GetPixels());
			long img_len = img_.Height()*img_.Width()*img_.Depth();
			if (cdsFlag) {
				if (imageCounter_ % factor) {
					memcpy(pBuf, stack_ptr, img_len);
					stack_ptr += img_len;
				}
				else {
					if (imageCounter_ == 0)
						memcpy(pBuf, stack_ptr, img_len);
					else
						memcpy(pBuf, first_frame, img_len);
				}
			}
			else {
				memcpy(pBuf, stack_ptr, img_len);
				stack_ptr += img_len;
			}
		}
	}
	ret = InsertImage();

	return ret;
};

bool CSciMeasure::IsCapturing() {
   return !thd_->IsStopped();
}

/*
 * called from the thread function before exit 
 */
void CSciMeasure::OnThreadExiting() throw()
{
   try
   {
      LogMessage(g_Msg_SEQUENCE_ACQUISITION_THREAD_EXITING);
      GetCoreCallback()?GetCoreCallback()->AcqFinished(this,0):DEVICE_OK;
   }
   catch(...)
   {
      LogMessage(g_Msg_EXCEPTION_IN_ON_THREAD_EXITING, false);
   }
}


MySequenceThread::MySequenceThread(CSciMeasure* pCam)
   :intervalMs_(default_intervalMS)
   ,numImages_(default_numImages)
   ,imageCounter_(0)
   ,stop_(true)
   ,suspend_(false)
   ,camera_(pCam)
   ,startTime_(0)
   ,actualDuration_(0)
   ,lastFrameTime_(0)
{};

MySequenceThread::~MySequenceThread() {};

void MySequenceThread::Stop() {
   MMThreadGuard g(this->stopLock_);
   stop_=true;
}

void MySequenceThread::Start(long numImages, double intervalMs)
{
   MMThreadGuard g1(this->stopLock_);
   MMThreadGuard g2(this->suspendLock_);
   numImages_=numImages;
   intervalMs_=intervalMs;
   imageCounter_=0;
   stop_ = false;
   suspend_=false;
   activate();
   actualDuration_ = 0;
   startTime_= camera_->GetCurrentMMTime();
   lastFrameTime_ = 0;
}

bool MySequenceThread::IsStopped(){
   MMThreadGuard g(this->stopLock_);
   return stop_;
}

void MySequenceThread::Suspend() {
   MMThreadGuard g(this->suspendLock_);
   suspend_ = true;
}

bool MySequenceThread::IsSuspended() {
   MMThreadGuard g(this->suspendLock_);
   return suspend_;
}

void MySequenceThread::Resume() {
   MMThreadGuard g(this->suspendLock_);
   suspend_ = false;
}

int MySequenceThread::svc(void) throw()
{
   int ret=DEVICE_ERR;
   try 
   {
      do
      {  
         ret = camera_->RunSequenceOnThread(startTime_);
      } while (DEVICE_OK == ret && !IsStopped() && imageCounter_++ < numImages_-1);
      if (IsStopped())
         camera_->LogMessage("SeqAcquisition interrupted by the user\n");
   }catch(...){
      camera_->LogMessage(g_Msg_EXCEPTION_IN_THREAD, false);
   }
   stop_=true;
   actualDuration_ = camera_->GetCurrentMMTime() - startTime_;
   camera_->OnThreadExiting();
   return ret;
}


///////////////////////////////////////////////////////////////////////////////
// CSciMeasure Action handlers
///////////////////////////////////////////////////////////////////////////////

/**
* Handles "Binning" property.
*/

int CSciMeasure::OnBinning(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
		pProp->Set(long(ccd_lib_bin[curConfig]));
	return DEVICE_OK;
}

/**
* Set camera pixel type. 
* We support only 16-bit mode here (14 bit A/D).
*/
int CSciMeasure::OnPixelType(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
		pProp->Set(g_PixelType_16bit);
	return DEVICE_OK;
}

/**
* Set readout Sequence selector
*/
int CSciMeasure::OnReadoutSequence(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	ostringstream command; command.str("");
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(config_list[curConfig]); 
	}
	else if (eAct == MM::AfterSet) {
		string tmpstr;
		pProp->Get(tmpstr);
		int lastConfig = curConfig;
		for (int i = 0; i <8; ++i) {
			if (tmpstr.compare(config_list[i]) == 0) {
				curConfig = i;
				if (curConfig != lastConfig) {
					SM_initCamera();
					cameraCCDXSize_ = max(file_height, min(file_width/horizontal_bin, cameraCCDXSize_));
					cameraCCDYSize_ = file_height;
					ResizeImageBuffer();
				}
				break;
			}
		}
	}
	return DEVICE_OK;
}

int CSciMeasure::OnCameraXSize(MM::PropertyBase* pProp , MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
		pProp->Set(cameraCCDXSize_);
	else if (eAct == MM::AfterSet)
	{
		long value;
		pProp->Get(value);
		if ( (value < 16) || (image_width/(1+cdsFlag) < value))
			value = image_width/(1+cdsFlag);
		if ( value != cameraCCDXSize_)
		{
			cameraCCDXSize_ = value;
			img_.Resize(cameraCCDXSize_/binSize_, cameraCCDYSize_/binSize_);
		}
	}
	return DEVICE_OK;
}

int CSciMeasure::OnNDRFrames(MM::PropertyBase* pProp , MM::ActionType eAct)
{
	char command[256];

	if (eAct == MM::BeforeGet) {
		if (ndr_lib[curConfig] == 1)
			pProp->Set(long(NDR_num_f));
		else
			pProp->Set(0.0);
	}
	else if (eAct == MM::AfterSet)
	{
		long value;
		pProp->Get(value);
		if ( value != NDR_num_f && ndr_lib[curConfig] == 1)
		{
			NDR_num_f = max(1, min(value, NDR_max_lib[curConfig]));
		}
	}
	return DEVICE_OK;
}

int CSciMeasure::OnCameraYSize(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
		pProp->Set(cameraCCDYSize_);
	return DEVICE_OK;
}

int CSciMeasure::OnTriggerMode(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		switch (sm_triggerMode) 
		{
		case 0: pProp->Set(triggerMode0); break;
		case 1: pProp->Set(triggerMode1); break;
		case 2: pProp->Set(triggerMode2); break;
		default:pProp->Set(triggerMode0); break;
		}
	}
	else if (eAct == MM::AfterSet)
	{
		string tmpstr;
		pProp->Get(tmpstr);
		if (tmpstr.compare(triggerMode1) == 0)
			sm_triggerMode = 1;
		else if (tmpstr.compare(triggerMode2) == 0) {
			sm_triggerMode = 2;
		}
		else
			sm_triggerMode = 0;
/*		SM_pdv_open();
		SM_set_trigger(0);
		SM_pdv_close();*/
	}
	return DEVICE_OK;
}

///////////////////////////////////////////////////////////////////////////////
// Private CSciMeasure methods
///////////////////////////////////////////////////////////////////////////////

/**
* Sync internal image buffer size to the chosen property values.
*/
int CSciMeasure::ResizeImageBuffer()
{
	// resize internal buffers
	// NOTE: we are assuming 16-bit pixel type
	const int byteDepth = 2;
	img_.Resize(cameraCCDXSize_/binSize_, cameraCCDYSize_/binSize_, byteDepth);
	return DEVICE_OK;
}

void CSciMeasure::GenerateEmptyImage(ImgBuffer& img)
{
   MMThreadGuard g(imgPixelsLock_);
   if (img.Height() == 0 || img.Width() == 0 || img.Depth() == 0)
      return;
   unsigned char* pBuf = const_cast<unsigned char*>(img.GetPixels());
   memset(pBuf, 0, img.Height()*img.Width()*img.Depth());
}

void CSciMeasure::TestResourceLocking(const bool recurse)
{
   if(recurse)
      TestResourceLocking(false);
}

int SciMeasureHub::Initialize()
{
  	initialized_ = true;
 
	return DEVICE_OK;
}

