#include "Cairn_MFC.h"
#include "../../MMDevice/ModuleInterface.h"
#include <sstream>
#include <cctype>
#include <algorithm>


const char* g_CairnMFCDeviceName = "Cairn Motorised Focus Control";
const char* g_CairnMFCUseRemote = "Use Remote";

///////////////////////////////////////////////////////////////////////////////
// Exported MMDevice API
///////////////////////////////////////////////////////////////////////////////
MODULE_API void InitializeModuleData()
{
   RegisterDevice(g_CairnMFCDeviceName, MM::StageDevice, "Cairn MFC Z-Stage");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
   if (deviceName == 0)
      return 0;

   if (strcmp(deviceName, g_CairnMFCDeviceName) == 0)
   {
      Cairn_MFC* s = new Cairn_MFC();
      return s;
   }

   return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}

// General utility function:
int ClearPort(MM::Device& device, MM::Core& core, std::string port)
{
   // Clear contents of serial port 
   const unsigned int bufSize = 255;
   unsigned char clear[bufSize];                      
   unsigned long read = bufSize;
   int ret;                                                                   
   while (read == bufSize)                                                   
   {                                                                     
      ret = core.ReadFromSerial(&device, port.c_str(), clear, bufSize, read);
      if (ret != DEVICE_OK)                               
         return ret;                                               
   }
   return DEVICE_OK;                                                           
} 
 

Cairn_MFC::Cairn_MFC()
	: m_port("Undefined"),
	m_initialised(false),
	m_useRemote(false)
{
   InitializeDefaultErrorMessages();

   // Name
   CreateProperty(MM::g_Keyword_Name, g_CairnMFCDeviceName, MM::String, true);

  // Description
   CreateProperty(MM::g_Keyword_Description, "Motorised microscope focus control", MM::String, true);

   // Port
   CPropertyAction* pActPort = new CPropertyAction (this, &Cairn_MFC::OnPort);
   CreateProperty(MM::g_Keyword_Port, "Undefined", MM::String, false, pActPort, true);

   // Use Remote
   CPropertyAction* pActRemote = new CPropertyAction (this, &Cairn_MFC::OnUseRemote);
   CreateProperty(g_CairnMFCUseRemote, "1", MM::Integer, false, pActRemote);
   AddAllowedValue(g_CairnMFCUseRemote, "1");
   AddAllowedValue(g_CairnMFCUseRemote, "0");
   SetProperty(g_CairnMFCUseRemote, "1");
}


Cairn_MFC::~Cairn_MFC()
{
   Shutdown();
}


int Cairn_MFC::Initialize()
{
	m_initialised = true;

	return 0;
}

int Cairn_MFC::Shutdown()
{

	return 0;
}

void Cairn_MFC::GetName(char* pszName) const
{

}

bool Cairn_MFC::Busy()
{

	return 0;
}

int Cairn_MFC::SetPositionUm(double pos)
{
	std::stringstream ss;
	ss << "SET " << pos << ";";
	
	if (int ret = SendSerialCommand(m_port.c_str(), ss.str().c_str(), "") != DEVICE_OK)
		return ret;

	CDeviceUtils::SleepMs(12 * (abs(pos)/1000.0));

	std::string answer;
	int ret = DEVICE_OK;
	while (ret == DEVICE_OK) 
	{
		ret = GetSerialAnswer(m_port.c_str(), "\r\n", answer);
		std::transform(answer.begin(), answer.end(), answer.begin(), std::toupper);
		if (answer.length() > 4 && answer.substr(0, 3) == "SET")
			break;
	}

	return DEVICE_OK;
}

int Cairn_MFC::GetPositionUm(double& pos)
{
	if (int ret = SendSerialCommand(m_port.c_str(), "GET;", "") != DEVICE_OK)
		return ret;

	CDeviceUtils::SleepMs(50);

	std::string answer;
	int ret = DEVICE_OK;
	while (ret == DEVICE_OK) 
	{
		ret = GetSerialAnswer(m_port.c_str(), "\r\n", answer);
		std::transform(answer.begin(), answer.end(), answer.begin(), std::toupper);
		if (answer.length() > 4 && answer.substr(0, 3) == "GET")
		{
			std::stringstream ss;
			ss << answer.substr(3);

			ss >> pos;
			break;
		}
	}

	return DEVICE_OK;
}


int Cairn_MFC::SetPositionSteps(long steps)
{
	std::stringstream ss;
	ss << std::setprecision(0) << "SET " << steps << ";";
	
	if (int ret = SendSerialCommand(m_port.c_str(), ss.str().c_str(), "") != DEVICE_OK)
		return ret;

	CDeviceUtils::SleepMs(12 * (abs(steps)/1000.0f));

	std::string answer;
	int ret = DEVICE_OK;
	while (ret == DEVICE_OK) 
	{
		ret = GetSerialAnswer(m_port.c_str(), "\r\n", answer);
		std::transform(answer.begin(), answer.end(), answer.begin(), std::toupper);
		if (answer.length() > 4 && answer.substr(0, 3) == "SET")
			break;
	}

	return DEVICE_OK;
}

int Cairn_MFC::GetPositionSteps(long& steps)
{
	if (int ret = SendSerialCommand(m_port.c_str(), "GET;", "") != DEVICE_OK)
		return ret;

	CDeviceUtils::SleepMs(50);

	std::string answer;
	int ret = DEVICE_OK;
	while (ret == DEVICE_OK) 
	{
		ret = GetSerialAnswer(m_port.c_str(), "\r\n", answer);
		std::transform(answer.begin(), answer.end(), answer.begin(), std::toupper);
		if (answer.length() > 4 && answer.substr(0, 3) == "GET")
		{
			std::stringstream ss;
			ss << answer.substr(3);

			ss >> steps;
			break;
		}
	}

	return DEVICE_OK;
}


int Cairn_MFC::SetOrigin()
{
	return SendSerialCommand(m_port.c_str(), "SAVE 0", "");
}


int Cairn_MFC::GetLimits(double& min, double& max)
{
	min = -32000; //Doesn't seem to do anything
	max = 32000;
	return 0;
}

int Cairn_MFC::OnPort(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(m_port.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		if (m_initialised)
		{
			// revert
			pProp->Set(m_port.c_str());
			return 1;// ERR_PORT_CHANGE_FORBIDDEN;
		}
		pProp->Get(m_port);
	}

	return DEVICE_OK;
}

int Cairn_MFC::OnUseRemote(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set((long)m_useRemote);
	}
	else if (eAct == MM::AfterSet)
	{
		long val;
		pProp->Get(val);
		if (val)
		{
			m_useRemote = true;
			if (m_initialised)
				SendSerialCommand(m_port.c_str(), "REM_ON;", "");
		}
		else
		{
			m_useRemote = false;
			if (m_initialised)
				SendSerialCommand(m_port.c_str(), "REM_OFF;", "");
		}
	}

	return DEVICE_OK;
}