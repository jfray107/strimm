#ifndef CAIRN_MFC_H
#define CAIRN_MFC_H

#include "../../MMDevice/MMDevice.h"
#include "../../MMDevice/DeviceBase.h"
#include <string>

class Cairn_MFC : public CStageBase<Cairn_MFC>
{
public:
	Cairn_MFC();
	~Cairn_MFC();

  
	// Device API
	// ----------
	int Initialize();
	int Shutdown();

	void GetName(char* pszName) const;
	bool Busy();

	// Stage API
	// ---------
	int SetPositionUm(double pos);
	int GetPositionUm(double& pos);
	int SetPositionSteps(long steps);
	int GetPositionSteps(long& steps);
	int SetOrigin();
	int GetLimits(double& min, double& max);

	int IsStageSequenceable(bool& isSequenceable) const {isSequenceable = false; return DEVICE_OK;}
	bool IsContinuousFocusDrive() const {return false;}



	int OnPort(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnUseRemote(MM::PropertyBase* pProp, MM::ActionType eAct);

private:
	std::string m_port;
	bool m_initialised;
	bool m_useRemote;
};

#endif // !CAIRN_MFC_H
