
#ifndef _MMERRORS_H_
#define _MMERRORS_H_
#include "optospin.h"


void debg_callback( t_rtr_hnd handle , int *user , char *msg  );
void usb_callback( t_rtr_hnd handle , int *user , char *msg  );
bool ErrorCheck ( int rValue , bool ShowMessage );

#endif