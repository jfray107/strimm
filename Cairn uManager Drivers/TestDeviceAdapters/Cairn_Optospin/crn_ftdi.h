
#ifdef WINDOWS
	#ifndef _WINDOWS
		#define _WINDOWS                            //Imports ftd2xx's WinTypes.
	#endif
    #include "../../../3rdparty/CairnOptospin/FTDI/Windows/ftd2xx.h"
	#include <WinBase.h>
	#include <Windows.h>
#endif

#ifdef LINUX
//    #include "FTDI/linux/libftd2xx1.0.4/ftd2xx.h"
    #include "../../../3rdparty/CairnOptospin/FTDI/linux/libftd2xx1.1.12/release/ftd2xx.h"
// N.B. For Linux the drivers normally need root access.
// Generally this is a rubish idea. Workaround:
// add lines:
//      ATTRS{idVendor}=="156b", ATTRS{idProduct}=="0003", GROUP="plugdev"
//      SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="156b", ATTRS{idProduct}=="0003", MODE="0666"
// to a file in file:
//      gedit /lib/udev/rules.d/51-cairn.rules
#endif


/*
 Two unsigned char USB message IDs.
 These are reversed relative to the PICs usage as Intel is little endian.
*/
#define    FTDI_OSPN_RESTART                     0x0000
#define    FTDI_OSPN_WRITE_PROG                  0x2000
#define    FTDI_OSPN_VERSION                     0x4000
#define    FTDI_OSPN_INIT_USB                    0x4400
#define    FTDI_OSPN_SET_SPD_X100                0x4800
#define    FTDI_OSPN_SET_REV_INTRV               0x4C00
#define    FTDI_OSPN_RD_REV_INTRV                0x5000
#define    FTDI_OSPN_RD_RTR_PRSNT                0x5400
#define    FTDI_OSPN_SEL_SPIN_RTRS               0x5800
#define    FTDI_OSPN_RD_SPIN_RTRS                0x5C00
#define    FTDI_OSPN_SPIN_RTRS                   0x6000
#define    FTDI_OSPN_STOP_RTRS                   0x6400
#define    FTDI_OSPN_IGN_TTL                     0x6800
#define    FTDI_OSPN_USE_TTL                     0x6C00
#define    FTDI_OSPN_SET_R1R2_MODE               0x7000
#define    FTDI_OSPN_SET_R3R4_MODE               0x7400
#define    FTDI_OSPN_GET_R1R2_MODE               0x7800
#define    FTDI_OSPN_GET_R3R4_MODE               0x7C00
//dep #define    FTDI_OSPN_SET_R3R4_COMB               0x8000
//dep #define    FTDI_OSPN_SET_R3R4_INRT               0x8400
#define    FTDI_OSPN_USB_GO                      0x8800
//dep #define    FTDI_OSPN_USB_INDP_GO                 0x8C00
//dep #define    FTDI_OSPN_STEP_CW                     0x9000
//dep #define    FTDI_OSPN_STEP_CCW                    0x9400
#define    FTDI_OSPN_GET_ROTOR_POSNS             0x9800
#define    FTDI_OSPN_GET_INDEP_ROTOR_POSNS       0x9C00
#define    FTDI_OSPN_WRITE_FILTER_EEPROM         0xA000
#define    FTDI_OSPN_READ_FILTER_EEPROM          0xA400
#define    FTDI_OSPN_MEASURE_TEMPERATURES        0xA800
#define    FTDI_OSPN_READ_TEMPERATURES           0xAC00
#define    FTDI_OSPN_SET_STEP_POWER              0xB000
#define    FTDI_OSPN_READ_STEP_POWER             0xB400
#define    FTDI_OSPN_WRITE_RAM                   0xB800
#define    FTDI_OSPN_READ_RAM                    0xBC00
#define    FTDI_OSPN_READ_PROG                   0xC000
#define    FTDI_OSPN_GET_READY_STATUS            0xC400
#define    FTDI_OSPN_GET_POWER_STATUS            0xC800
#define    FTDI_OSPN_SWITCH_ON                   0xCC00
#define    FTDI_OSPN_SWITCH_OFF                  0xD000
#define    FTDI_OSPN_GET_ROTOR_STATUS            0xD400
#define	   FTDI_OSPN_SET_BRAKE_TIME				 0xD800
#define    FTDI_OSPN_GET_BRAKE_TIME				 0xDC00
#define    FTDI_OSPN_DISABLE_DECEL_FB            0xE000
#define    FTDI_OSPN_ENABLE_DECEL_FB             0xE400
//#define    FTDI_OSPN_


#define     FTDI_DEFAULT_READ         1500
#define     FTDI_DEFAULT_WRITE        500

int _FTDI_Get_Device_Count ( unsigned int * available );
int _FTDI_Get_Device_Info ( unsigned int index , char * * serial , char * * descr );
int _FTDI_Open_Channel ( char * serial , FT_HANDLE * ftdi );
int _FTDI_Set_Timeouts (  t_rtr_hnd handle , FT_HANDLE ft_hnd , int read , int write );
int _FTDI_Default_Timeouts (  t_rtr_hnd handle , FT_HANDLE ft_hnd );
int _FTDI_Issue_FW_Reset ( t_rtr_hnd handle , FT_HANDLE ft_hnd );
int _FTDI_Rx ( t_rtr_hnd handle , FT_HANDLE FT , int size , unsigned char * buffer );
int _FTDI_Tx ( t_rtr_hnd handle , FT_HANDLE FT , int size , unsigned char * buffer );
int _FTDI_Purge_IO ( FT_HANDLE ft_handle );
void _FTDI_Lib_Term ( void );




