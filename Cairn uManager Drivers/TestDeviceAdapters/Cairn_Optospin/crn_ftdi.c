#include "platform.h"                               // Platform specific code
#include "optospin.h"
#include "debugs.h"
#include "math.h"
#include "e_strs.h"
#include "errors.h"
#include "string.h"
#include "crn_ftdi.h"

FT_STATUS ftStatus;
FT_DEVICE_LIST_INFO_NODE *FTdevInfo = NULL;
DWORD FTnumDevs = 0 ;


void _FTDI_Error( t_rtr_hnd handle , FT_STATUS status , char * message , t_err_lvl lvl )
{
    char * errstr = ( char * ) malloc ( strlen ( message ) + 128 );
    errstr[0] = 0;
    strcat ( errstr , message );
    switch ( status )
    {
        case FT_OK:
          strcat ( errstr , "." );
          break;
        case FT_INVALID_HANDLE:
          strcat ( errstr , ", invalid usb device handle." );
          break;
        case FT_DEVICE_NOT_FOUND:
          strcat ( errstr , ", usb device not found." );
          break;
        case FT_DEVICE_NOT_OPENED:
          strcat ( errstr , ", usb device not opened." );
          break;
        case FT_IO_ERROR:
          strcat ( errstr , ", usb i/o error." );
          break;
        case FT_INSUFFICIENT_RESOURCES:
          strcat ( errstr , ", usb insufficient resources." );
          break;
        case FT_INVALID_PARAMETER:
          strcat ( errstr , ", usb driver invalid parameter." );
          break;
        case FT_INVALID_BAUD_RATE:
          strcat ( errstr , ", usb driver invalid baud rate." );
          break;
        case FT_DEVICE_NOT_OPENED_FOR_ERASE:
          strcat ( errstr , ", usb device not opened for erase." );
          break;
        case FT_DEVICE_NOT_OPENED_FOR_WRITE:
          strcat ( errstr , ", usb device not opened for write." );
          break;
        case FT_FAILED_TO_WRITE_DEVICE:
          strcat ( errstr , ", usb driver failed to write." );
          break;
        case FT_EEPROM_READ_FAILED:
          strcat ( errstr , ", usb driver eeprom read fail." );
          break;
        case FT_EEPROM_WRITE_FAILED:
          strcat ( errstr , ", usb driver eeprom write fail." );
          break;
        case FT_EEPROM_ERASE_FAILED:
          strcat ( errstr , ", usb driver eeprom erase fail." );
          break;
	    case FT_EEPROM_NOT_PRESENT:
          strcat ( errstr , ", usb eeprom not present." );
          break;
	    case FT_EEPROM_NOT_PROGRAMMED:
          strcat ( errstr , ", usb eeprom not programmed." );
          break;
	    case FT_INVALID_ARGS:
          strcat ( errstr , ", usb driver invalid arguments." );
          break;
	    case FT_NOT_SUPPORTED:
          strcat ( errstr , ", usb driver call not supported." );
          break;
	    case FT_OTHER_ERROR:
          strcat ( errstr , ", usb driver unknown error." );
          break;
// Don't know where this went, seemed to disapear in an upgrade?
// Platform specific?
//	    case FT_DEVICE_LIST_NOT_READY:
//          strcat ( buffer , ", device list not ready." );
//          break;
        default:
          strcat ( errstr , ", usb driver unspecified error code." );
          break;
    }
    add_err_str ( errstr , lvl , 0 );
    _do_debug ( handle , errstr );
    return;
}

void _FTDI_Lib_Term ( void )
{
    if ( FTdevInfo )
    {
        free ( FTdevInfo );
        FTdevInfo = NULL;
    }
    return;
}

int _FTDI_Get_Device_Count ( unsigned int * available )
{
	unsigned int j;
    if ( FTdevInfo )
    {
        free ( FTdevInfo );
        FTdevInfo = NULL;
    }
    ftStatus = FT_CreateDeviceInfoList ( &FTnumDevs );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( NULL , ftStatus , "Unable to create USB device listing" , ERR_LVL_ABORT );
        return RTR_ERR_IO;
    }
    if ( !FTnumDevs ) return RTR_OK;
    FTdevInfo = ( FT_DEVICE_LIST_INFO_NODE* )malloc ( sizeof( FT_DEVICE_LIST_INFO_NODE ) * FTnumDevs );
    ftStatus = FT_GetDeviceInfoList ( FTdevInfo , &FTnumDevs );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( NULL , ftStatus , "Unable to retrieve USB device listing" , ERR_LVL_ABORT );
        return RTR_ERR_IO;
    }
    for (j = 0; j < FTnumDevs; j++)
    {
        if ( 0x156B0003 == FTdevInfo[j].ID )
        {
            if ( ( FTdevInfo[j].Flags & FT_FLAGS_OPENED ) != FT_FLAGS_OPENED )
            {
                (*available)++;
            }
        }
    }
    return RTR_OK;
}

int _FTDI_Get_Device_Info ( unsigned int index , char * * serial , char * * descr )
{
    unsigned int found;
    unsigned int i;
    if ( ( index > FTnumDevs ) | ( index == 0 ) )
    {
        _internal_error ( RTR_ERR_INVALID_PARAM , "Can't get Optospin information for index" , ERR_LVL_ABORT );
        return RTR_ERR_INVALID_PARAM;
    }
    found = 1;
    if ( ( index > FTnumDevs ) | ( index == 0 ) )
    {
        _internal_error ( RTR_ERR_INVALID_PARAM , "Can't get Optospin information for index" , ERR_LVL_ABORT );
        return RTR_ERR_INVALID_PARAM;
    }
    found = 1;
	for (i = 0; i < FTnumDevs; i++)
	{
        if ( 0x156B0003 == FTdevInfo[i].ID )
        {
            if ( found == index )
            {
                *serial = (char*)&FTdevInfo[i].SerialNumber;
                *descr = (char*)&FTdevInfo[i].Description;
                return RTR_OK;
            }
            found++;
        }
	}
    _internal_error ( RTR_ERR_INVALID_PARAM , "Can't find USB device specified" , ERR_LVL_ABORT );
    return RTR_ERR_INVALID_PARAM;
}

char buf_kilo1[1024];
char buf_kilo2[1024];

int _FTDI_Tx ( t_rtr_hnd handle , FT_HANDLE FT , int size , unsigned char * buffer )
{
    int i;
    DWORD written;
    if ( _usb_callback_installed ( ) )
    {
        buf_kilo1[0] = 0;
        sprintf( buf_kilo1 , ">" );
        for ( i=0 ; i<size ; i++ )
        {
            buf_kilo2[0]=0;
            sprintf( buf_kilo2 , "0x%x," , buffer[i] );
            strcat( buf_kilo1 , buf_kilo2 );
        }
        _do_usb( NULL , buf_kilo1 );
    }
    ftStatus = FT_Write( FT , buffer , size , &written );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( handle , ftStatus , "USB write failed" , ERR_LVL_ABORT );
        return RTR_ERR_IO;
    }
    i = written;
    if ( i != size )
    {
        _FTDI_Error ( handle , ftStatus , "USB write incomplete" , ERR_LVL_ABORT );
        return RTR_ERR_INCOMPLETE;
    }
    return RTR_OK;
}

int _FTDI_Rx ( t_rtr_hnd handle , FT_HANDLE FT , int size , unsigned char * buffer )
{
    DWORD read;
    int i;
    ftStatus = FT_Read( FT , buffer , size , &read );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( handle , ftStatus , "USB read failed" , ERR_LVL_ABORT );
        return RTR_ERR_IO;
    }
    i = read;
    if ( i != size )
    {
        _FTDI_Error ( handle , ftStatus , "USB read incomplete" , ERR_LVL_ABORT );
        return RTR_ERR_INCOMPLETE;
    }
    if ( _usb_callback_installed ( ) )
    {
        buf_kilo1[0] = 0;
        sprintf( buf_kilo1 , "<" );
        for ( i=0 ; i<size ; i++ )
        {
            buf_kilo2[0]=0;
            sprintf( buf_kilo2 , "0x%x," , buffer[i] );
            strcat( buf_kilo1 , buf_kilo2 );
        }
        _do_usb( NULL , buf_kilo1 );
    }
    return RTR_OK;
}

int _FTDI_Issue_FW_Reset ( t_rtr_hnd handle , FT_HANDLE ft_hnd )
{
    unsigned short msg;
	int err;
    msg = FTDI_OSPN_RESTART;
    err = _FTDI_Tx ( handle , ft_hnd , 2 , (unsigned char *)&msg );
    if ( RTR_OK != err )
    {
        _internal_error ( err , "Unable to firmware reset the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}


int _FTDI_Set_Timeouts (  t_rtr_hnd handle , FT_HANDLE ft_hnd , int read , int write )
{
    ftStatus = FT_SetTimeouts( ft_hnd , read , write );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( handle , ftStatus , "Failed to set USB IO timeouts" , ERR_LVL_ABORT );
        return RTR_ERR_TIMEOUT;
    }
    return RTR_OK;
}

int _FTDI_Default_Timeouts (  t_rtr_hnd handle , FT_HANDLE ft_hnd )
{
    ftStatus = FT_SetTimeouts( ft_hnd , FTDI_DEFAULT_READ , FTDI_DEFAULT_WRITE );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( handle , ftStatus , "Failed to set default USB IO timeouts" , ERR_LVL_ABORT );
        return RTR_ERR_TIMEOUT;
    }
    return RTR_OK;
}

int _FTDI_Purge_IO ( FT_HANDLE ft_handle )
{
    FT_STATUS ftStatus = FT_Purge( ft_handle , FT_PURGE_RX | FT_PURGE_TX );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( NULL , ftStatus , "Failed to purge USB pipelines" , ERR_LVL_ABORT );
        return RTR_ERR_IO;
    }
    return RTR_OK;
}

int _FTDI_Open_Channel ( char * serial , FT_HANDLE * ftdi )
{
    // FTDI: open a connection to the USB chip.
    int res = RTR_ERR_IO;   // Default error code
    _do_debug ( NULL , "Opening FTDI device by serial number." );
    ftStatus = FT_OpenEx ( serial , FT_OPEN_BY_SERIAL_NUMBER , ftdi );
    if ( FT_OK != ftStatus )
    {
        _FTDI_Error ( NULL , ftStatus , "Failed to open USB device specified" , ERR_LVL_ABORT );
        return res;
    }
    _do_debug ( NULL , "Setting FTDI device timeouts." );
    ftStatus = FT_SetTimeouts ( *ftdi , FTDI_DEFAULT_READ , FTDI_DEFAULT_WRITE );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( NULL , ftStatus , "Failed to set USB IO timeouts" , ERR_LVL_ABORT );
        FT_Close( *ftdi );
        return res;
    }
    _do_debug ( NULL , "Initialising Optospin USB I/O." );
    ftStatus = FT_Purge( *ftdi , FT_PURGE_TX );
    if ( ftStatus != FT_OK )
    {
        _FTDI_Error ( NULL , ftStatus , "Failed to purge USB pipelines" , ERR_LVL_ABORT );
        FT_Close( *ftdi );
        return res;
    }
    return RTR_OK;
}
