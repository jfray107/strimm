/*
 	Description:	Optospin control library header
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2012 - 2014 Andrew Hill

    This file is part of optospin.dll and liboptospin.so

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef OPTOSPIN_H
#define OPTOSPIN_H

#ifdef _MSC_VER
    #pragma warning( disable : 4214 34 )  // Disable FTDI bit usage warning.
    #pragma warning( disable : 4996 34 )  // Disable some MS embrace, extend, extinguish.
    #define _CRT_SECURE_NO_DEPRECATE      // Disable more MS embrace, extend, extinguish.
    #define _CRT_SECURE_NO_WARNING        // Disable more MS embrace, extend, extinguish.
#endif

#ifdef _WIN32
	#define WINDOWS
	#define _PLAT_CDECL _cdecl
#endif // _WIN32
#ifdef __linux__
    #define LINUX
    #define _PLAT_CDECL
#endif // __linux__
#ifdef __APPLE__
    #define APPLE
    #define _PLAT_CDECL
#endif // __APPLE__


#ifndef LIB_CALL
	#ifdef BUILD_DLL
		#define LIB_CALL __declspec(dllexport)
	#else
		#define LIB_CALL __declspec(dllimport)
	#endif
#endif

/* Error codes potentially returned from the library. - AAH */
#define     RTR_OK                      0
#define     RTR_ERR_IO                  -1
#define     RTR_ERR_TIMEOUT             -2
#define     RTR_ERR_INCOMPLETE          -3
#define     RTR_ERR_NOT_FOUND           -4
#define     RTR_ERR_INVALID_HND         -5
#define     RTR_ERR_INVALID_PARAM       -6
#define     RTR_ERR_UNSUPPORTED         -7
#define     RTR_ERR_ACK                 -8
#define     RTR_ERR_DUMMY_FLUMOXED      -9
#define     RTR_ERR_ROTOR               -10
#define     RTR_ERR_BUFFER_OVERFLOW     -11
#define     RTR_ERR_NOT_INIT            -12
#define     RTR_ERR_ROTOR_STEPPING      -13
#define		RTR_ERR_ROTOR_WAITING		-14
#define     RTR_ERR_MESSAGE             -15
#define		RTR_ERR_INSUFFICIENT_HARDWARE -16

/* Possible error condition level indicators. - AAH */
#define     ERR_LVL_NONE				0
#define     ERR_LVL_HALT				1
#define     ERR_LVL_INFORM				2
#define     ERR_LVL_SKIP				4
#define     ERR_LVL_ABORT				8
#define     ERR_LVL_NOTE				16
#define     ERR_LVL_ALL					32

/* Library flags. */
#define		RTR_MIN_SPIN_SPEED_HZ		1
#define		RTR_MAX_SPIN_SPEED_HZ		100

#define     RTR_WHEEL_1                 0x00000001
#define     RTR_WHEEL_2                 0x00000002
#define     RTR_WHEEL_3                 0x00000004
#define     RTR_WHEEL_4                 0x00000008

#define     RTR_MODE_NORMAL             0x00000000
#define     RTR_MODE_COMBINED_A         0x00000010
#define     RTR_MODE_INERTIAL_A         0x00000020
#define     RTR_MODE_RESERVED_ERROR_A   0x00000030
#define     RTR_MODE_A_BITS             RTR_MODE_RESERVED_ERROR_A


#define     RTR_MODE_COMBINED_B         0x00000040
#define     RTR_MODE_INERTIAL_B         0x00000080
#define     RTR_MODE_RESERVED_ERROR_B   0x000000C0
#define     RTR_MODE_B_BITS             RTR_MODE_RESERVED_ERROR_B


#define     RTR_MODE_INVALID            0x80000000

#define		t_bool_true					1
#define		t_bool_false				0

#define     INVALID_RTR_HANDLE          NULL


/* Rotor system handle used once a system is opened & connected - AAH */
typedef void * t_rtr_hnd;

#ifdef _MSC_VER
	typedef void (_cdecl * t_debg_callback) (t_rtr_hnd handle , int *user , char *msg);
	typedef void (_cdecl * t_usb_callback) (t_rtr_hnd handle , int *user , char *msg);
	typedef void (_cdecl * t_prog_callback) (t_rtr_hnd handle , int value , int of );
#else
	typedef void (_PLAT_CDECL * t_debg_callback) (t_rtr_hnd handle , int *user , char *msg);
	typedef void (_PLAT_CDECL * t_usb_callback) (t_rtr_hnd handle , int *user , char *msg);
	typedef void (_PLAT_CDECL * t_prog_callback) (t_rtr_hnd handle , int value , int of );
#endif

typedef unsigned int t_err_lvl;
typedef unsigned int t_whl_idx;
typedef unsigned int t_wheels;
typedef unsigned char t_temp;
typedef unsigned char t_whl_pos;
typedef unsigned char t_bool;
typedef unsigned int t_rtr_mode;


/*
 Define LOCAL_OPTOSPIN_CODE if you are using this header with all the
 c code included in your project. E.g the MicroManager Device. - AAH
*/
#ifdef LOCAL_OPTOSPIN_CODE
	#define CALL
#else
	#define CALL LIB_CALL
#endif

#ifdef _WIN32
    #ifdef LOCAL_OPTOSPIN_CODE
        #define CNV
	#else
		#define CNV _PLAT_CDECL
	#endif
#endif

#ifndef CNV
    #define CNV
#endif

#ifdef __cplusplus
extern "C" {
#endif

CALL int CNV rtr_do_lib_init ( unsigned char *maj , unsigned char *min );
CALL int CNV rtr_do_lib_term ( void );

CALL int CNV rtr_get_error_str ( char ** str , t_err_lvl *lvl);
CALL int CNV rtr_do_clr_err_strs ( t_err_lvl lvl );

CALL int CNV rtr_get_device_count ( unsigned int * available );
CALL int CNV rtr_get_device_info ( unsigned int index , char * * serial , char * * descr );
CALL int CNV rtr_get_device_by_serial ( char *serial , t_rtr_hnd *handle , t_bool reset );
CALL int CNV rtr_do_device_close ( t_rtr_hnd handle );

CALL int CNV rtr_get_max_wheel_count ( t_rtr_hnd handle , t_whl_idx *max );
CALL int CNV rtr_get_fw_version ( t_rtr_hnd handle , unsigned char *maj , unsigned char *min );
CALL int CNV rtr_get_wheel_count ( t_rtr_hnd handle , t_whl_idx *count );

CALL int CNV rtr_set_mode ( t_rtr_hnd handle , t_rtr_mode mode );
CALL int CNV rtr_get_mode ( t_rtr_hnd handle , t_rtr_mode *mode );

CALL int CNV rtr_get_wheel_position_max ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos *pos );
CALL int CNV rtr_get_wheel_position ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos *pos );
CALL int CNV rtr_get_wheel_positions ( t_rtr_hnd handle , t_whl_pos *pos );
CALL int CNV rtr_set_wheel_positions ( t_rtr_hnd handle , t_whl_pos *pos );
CALL int CNV rtr_set_ascii_wheel_positions ( t_rtr_hnd handle , char * data , int len , char base );

CALL int CNV rtr_do_wait_ready_to_move ( t_rtr_hnd handle , t_bool* ready );
CALL int CNV rtr_get_ready_to_move ( t_rtr_hnd handle , t_bool* ready );
//CALL int CNV rtr_do_wait_ready_to_acqu ( t_rtr_hnd handle );
CALL int CNV rtr_get_ready_to_acqu ( t_rtr_hnd handle , t_bool* ready );
CALL int CNV rtr_do_wait_for_sync ( t_rtr_hnd handle , int timeout_ms );

CALL int CNV rtr_set_brake_time(t_rtr_hnd handle , t_whl_idx wheel , unsigned char * centiseconds );
CALL int CNV rtr_get_brake_time(t_rtr_hnd handle , t_whl_idx wheel , unsigned char * centiseconds );

CALL int CNV rtr_set_spin_wheel ( t_rtr_hnd handle , t_whl_idx wheel , t_bool would_spin );
CALL int CNV rtr_set_spin_wheels ( t_rtr_hnd handle , t_wheels wheels );
CALL int CNV rtr_get_spin_wheel ( t_rtr_hnd handle , t_whl_idx wheel , t_bool *would_spin );
CALL int CNV rtr_get_spin_wheels ( t_rtr_hnd handle , t_wheels * wheels );
CALL int CNV rtr_set_speed ( t_rtr_hnd handle , double speed );
CALL int CNV rtr_set_interval ( t_rtr_hnd handle , unsigned int microsec );
CALL int CNV rtr_get_spin_params ( t_rtr_hnd handle , unsigned int *microsec , t_bool *spinning ,t_bool *syncing , t_bool *external );
CALL int CNV rtr_do_spin_wheels ( t_rtr_hnd handle  );
CALL int CNV rtr_do_stop_wheels ( t_rtr_hnd handle );
CALL int CNV rtr_get_spinning ( t_rtr_hnd handle , t_bool* spinning );

CALL int CNV rtr_get_temps ( t_rtr_hnd handle , t_whl_idx wheel , t_temp *temp );
CALL int CNV rtr_do_measure_temps ( t_rtr_hnd handle );

CALL int CNV rtr_set_position_data ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos pos , char *data );
CALL int CNV rtr_get_position_data ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos pos , char **data );

CALL int CNV rtr_do_ignore_ttl ( t_rtr_hnd handle , t_bool ignore );

CALL int CNV rtr_set_prog_hook ( t_prog_callback callback  );


#ifdef OPTOSPIN_ADVANCED
/*
 I suspect the following should remain undocumented; for Cairn eyes only.
 They will probably do more harm than good otherwise. - AAH
*/
typedef unsigned char t_rtr_status;
CALL int CNV rtr_get_status ( t_rtr_hnd handle , t_rtr_status *status );


// FTDI library timeout data
CALL int CNV rtr_set_usb_timeouts ( t_rtr_hnd handle , int read , int write  );
CALL int CNV rtr_do_usb_timeout_default ( t_rtr_hnd handle );

// Rotor power
typedef unsigned char t_pwr_lvl;
typedef unsigned char t_decel_fact;
CALL int CNV rtr_get_step_power ( t_rtr_hnd handle , t_whl_idx wheel , t_pwr_lvl * power , t_decel_fact * decel_fact);
CALL int CNV rtr_set_step_power ( t_rtr_hnd handle , t_whl_idx wheel , t_pwr_lvl power , t_decel_fact decel_fact );
CALL int CNV rtr_set_min_max_power ( t_pwr_lvl min , t_pwr_lvl max  );

/* Major sledge hammer reset. You need to close and open again after this! - AAH */
CALL int CNV rtr_do_fw_reset ( t_rtr_hnd handle );

/* Pretty useless as the wheels wander off for some arbitrary
   long period to find pos 1. - AAH */
CALL int CNV rtr_do_reinitialise ( t_rtr_hnd handle );


// Callbacks for debugging porpoises
CALL int CNV rtr_do_clear_all_hooks ( void );
CALL int CNV rtr_set_debug_hook ( t_debg_callback callback );
CALL int CNV rtr_set_usb_hook ( t_usb_callback callback );
CALL int CNV rtr_do_clear_usb_hook ( void );
CALL int CNV rtr_do_clear_debug_hook ( void );

// Readout or overwrite the pics flash program memory
CALL int CNV rtr_get_firmware ( t_rtr_hnd handle , unsigned char * buffer , int * buf_size );
CALL int CNV rtr_set_firmware ( t_rtr_hnd handle , unsigned char * buffer );

// Spin music test
CALL int CNV rtr_do_ode( t_rtr_hnd handle );

CALL double CNV rtr_time_stamp ( void );
CALL int CNV rtr_set_deceleration_feedback_on(t_rtr_hnd handle , t_whl_idx wheel );
CALL int CNV rtr_set_deceleration_feedback_off(t_rtr_hnd handle , t_whl_idx wheel );


#endif





#ifdef __cplusplus
}
#endif


#endif  /* OPTOSPIN_H */
