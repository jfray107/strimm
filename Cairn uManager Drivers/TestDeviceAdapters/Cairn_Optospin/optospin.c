/*
 	Description:	Optospin control library
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2010 - 2011 Andrew Hill

    This file is part of optospin.dll and liboptospin.so

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


/* Notes:

To get this working with the linux version of ftd2xx on my Ubuntu system I had
to create the file /etc/udev/rules.d/85-ftdi.rules and add the line:
SYSFS{idVendor}=="156b", SYSFS{idProduct}=="0003", MODE="666", GROUP="plugdev"
to it AND then reboot. Otherwise libusb didn't have the correct permissions to
talk to the ftdi chip at /dev/bus/usb/00N/00N. - AAH

*/
#ifndef OPTOSPIN_ADVANCED
	#define OPTOSPIN_ADVANCED
#endif
#include "platform.h"                               // Platform specific code
#include "optospin.h"
#include "e_strs.h"
#include "errors.h"
#include "debugs.h"
#include "crn_ftdi.h"
#include "math.h"

#define    LIBRARY_MAJ_VERSION                   1
#define    LIBRARY_MIN_VERSION                   1

/*
1.1 - First release along with MicroManager Device
*/

int _lib_inited = 0;
#define		RTR_BOX_MAX_WHEEL_COUNT		4

//Default read and write timeouts.
int _read_timeout = FTDI_DEFAULT_READ;
int _write_timeout = FTDI_DEFAULT_WRITE;

// This checks tha validity of a rotor handle sent to the library...
// ..and returns an FTDI handle if it was valid.
#define    VAR_ME				 protor_details me;
#define    CHECK_ME      		 me = _is_a_rotor ( handle ); \
                                 if ( NULL == me ) \
                                 { \
                                    _internal_error ( RTR_ERR_INVALID_HND , "Failed valid handle check" , ERR_LVL_ABORT ); \
                                    return RTR_ERR_INVALID_HND; \
                                 };

#define	   VAR_FT   			 FT_HANDLE ft_hnd = 0;
#define    CHECK_FT              ft_hnd = me->ft_handle;



// This checks an unsigned int "wheel" index
#define     CHECK_WHEEL         if ( ( wheel < 1 ) || ( wheel > RTR_BOX_MAX_WHEEL_COUNT ) ) \
                                { \
                                    _internal_error ( RTR_ERR_INVALID_PARAM , "Invalid wheel number used" , ERR_LVL_ABORT ); \
                                    return RTR_ERR_INVALID_PARAM; \
                                }

#define _DEFAULT_POWER 0x80
t_pwr_lvl MIN_POWER = 0x10;
t_pwr_lvl MAX_POWER = 0xF0;
t_pwr_lvl DEFAULT_POWER = _DEFAULT_POWER;

#define     CHECK_POWER         if ( power < MIN_POWER ) power = MIN_POWER; \
                                if ( power > MAX_POWER ) power = MAX_POWER;
//This checks an "pos" filter position index
// this should be extracted from the PIC and/or stored in the data structure
#define     CHECK_POS           if ( ( pos < 1 ) || ( pos > 6 ) ) \
                                { \
                                    _internal_error ( RTR_ERR_INVALID_PARAM , "Invalid wheel position parameter" , ERR_LVL_ABORT ); \
                                    return RTR_ERR_INVALID_PARAM; \
                                }
#define     CHECK_INIT          if ( !_lib_inited ) \
                                { \
                                    _internal_error ( RTR_ERR_NOT_INIT , "Library call rtr_init_lib needed" , ERR_LVL_ABORT ); \
                                    return RTR_ERR_NOT_INIT; \
                                }

DWORD list_count;
FT_STATUS ftStatus;
FT_DEVICE_LIST_INFO_NODE *list;
DWORD flags;
DWORD id;
DWORD type;
DWORD loc_id;
char ser_num[17];
char desc[65];

int buf_size = 0;
unsigned char * buf_data = NULL;
unsigned char * buf_header = NULL;
#define DEFAULT_BUFFER_SIZE 64

//typedef void (_cdecl* prog_callback) ( t_rtr_hnd handle , int value , int of  );
t_prog_callback _progress = NULL;

#define BOX_DETAILS_VERSION     1
#define RTR_TYPE_FTDI           1

typedef struct _rotor_details	rotor_details;
typedef rotor_details *			protor_details;
struct _rotor_details
{
    char			ID[5];
    int				type;
    int				ver;
    unsigned char	fw_ver_maj;
    unsigned char	fw_ver_min;
    char			serial[16];
    FT_HANDLE		ft_handle;
    unsigned int	wh_count;
    int				wh_spinning;
	int				whl_found[RTR_BOX_MAX_WHEEL_COUNT];
    t_whl_pos		whl_pos[RTR_BOX_MAX_WHEEL_COUNT];                  /* Contains each wheels current filter position. */
    t_whl_pos		whl_max[RTR_BOX_MAX_WHEEL_COUNT];                  /* Contains each wheels total filter count. */
    t_bool			wh_spins[RTR_BOX_MAX_WHEEL_COUNT];                  /* Contains each wheels spin flag. */
    t_rtr_mode		mode;
    protor_details	next;
};
protor_details _rtr_details_list = NULL;

protor_details _create_new_rotor_details ( unsigned char rtrs ,  char * serial , FT_HANDLE ft_handle )
{
    protor_details new_details;
    protor_details temp;
    int i;
	int cnt;
	unsigned char flg;
    new_details = ( protor_details ) malloc ( sizeof ( rotor_details ) );
    new_details->next = NULL;
    new_details->ID[0] = 'C';
    new_details->ID[1] = 'r';
    new_details->ID[2] = 'n';
    new_details->ID[3] = 'S';
    new_details->ID[4] = 'p';
    new_details->ver = BOX_DETAILS_VERSION;
    new_details->fw_ver_maj = 0;
    new_details->fw_ver_min = 0;
    new_details->wh_spinning = 0;

    new_details->type = RTR_TYPE_FTDI;
    new_details->ft_handle = ft_handle;
    memcpy ( &new_details->serial[0] , serial , 16 );
    new_details->mode = RTR_MODE_NORMAL;

	cnt = 0;
	flg = 0x01;
    for ( i=0 ; i<RTR_BOX_MAX_WHEEL_COUNT ; i++)
    {
		new_details->whl_pos[i] = 0;
        new_details->whl_max[i] = 0;
        new_details->wh_spins[i] = 1;
        if ( rtrs & flg )
        {
            new_details->whl_found[i] = 1;
            cnt++;
        }else{
            new_details->whl_found[i] = 0;
        }
        flg = flg << 1;
    }

//	if ( 0 != ( rtrs & 0x01 ) ) { new_details->whl_found[0] = 1; cnt++; }
//	if ( 0 != ( rtrs & 0x02 ) ) { new_details->whl_found[1] = 1; cnt++; }
//  if ( 0 != ( rtrs & 0x04 ) ) { new_details->whl_found[2] = 1; cnt++; }
//  if ( 0 != ( rtrs & 0x08 ) ) { new_details->whl_found[3] = 1; cnt++; }

    new_details->wh_count = cnt;
    //Add the details of this rotor to our list
    if ( _rtr_details_list == NULL )
    {
        _rtr_details_list = new_details;
    }else{
        temp = _rtr_details_list;
        while ( temp->next != NULL )
        {
            temp = temp->next;
        }
        temp->next = new_details;
    }
    _do_debug ( new_details , "New rotor structure created and populated.");
    return new_details;
}

void _destroy_rotor_details ( protor_details me )
{
    protor_details temp;
    if ( me == _rtr_details_list )
    {
        _rtr_details_list = me->next;
        me->next = NULL;
    }else{
        temp = _rtr_details_list;
        while ( temp )
        {
            if ( me == temp->next )
            {
                temp->next = me->next;
                me->next = NULL;
                break;
            }
        }
    }
    free ( me );
    _do_debug ( me , "Rotor structure deallocated.");
    return;
}

// Possible rotor return codes
#define RTR_USB_RTN_OK              0xFF

#define RTR_USB_RTN_INIT_BUSY       0x00
#define RTR_USB_RTN_SPINNING        0x01
#define RTR_USB_RTN_ALREADY         0x02
#define RTR_USB_RTN_TTL             0x03
#define RTR_USB_RTN_STEPPING        0x04
#define RTR_USB_RTN_NO_WHEELS       0x05
#define RTR_USB_RTN_PROTECTED       0x06
#define RTR_USB_RTN_POST_STEP_REST  0x07

int dummy_is_spinning = 0;
unsigned int dummy_mode;
double dummy_speed;
unsigned char dummy_spinners = 0xF;
unsigned char dummy_pos1 = 0;
unsigned char dummy_pos2 = 0;
unsigned char dummy_pos3 = 0;
unsigned char dummy_pos4 = 0;
unsigned char dummy_pwr1 = _DEFAULT_POWER;
unsigned char dummy_pwr2 = _DEFAULT_POWER;
unsigned char dummy_pwr3 = _DEFAULT_POWER;
unsigned char dummy_pwr4 = _DEFAULT_POWER;
unsigned int dummy_brake_time1 = 25;
unsigned int dummy_brake_time2 = 25;
unsigned int dummy_brake_time3 = 25;
unsigned int dummy_brake_time4 = 25;
char * dummy_name = NULL;
int DUMMY = 1;
void * DUMMY_FT_HANDLE = (void *) (unsigned long long) 1;
#define DUMMY_NAME "DUMMY"


int _dummy_mesg ( unsigned short msg_id )
{
        int i;
        unsigned int tmp;
        unsigned char n1n0,n3n2;
        switch ( msg_id )
        {
            case FTDI_OSPN_RESTART:
                break;
            case FTDI_OSPN_WRITE_PROG:
                if ( buf_data[1] >= 0xF0 )
                {
                    _internal_error( RTR_ERR_ROTOR , "Attempted protected code write" , ERR_LVL_ABORT );
                }
                break;
            case FTDI_OSPN_VERSION:
                buf_data[0] = 9;
                buf_data[1] = 9;
                break;
            case FTDI_OSPN_INIT_USB:
                break;
            case FTDI_OSPN_SET_SPD_X100:
                tmp = buf_data[0];
                tmp = tmp << 8;
                tmp = tmp + buf_data[1];
                dummy_speed = tmp / 100;
                break;
            case FTDI_OSPN_SET_REV_INTRV:
                tmp = buf_data[0];
                tmp = tmp << 8;
                tmp = tmp + buf_data[1];
                tmp = tmp << 8;
                tmp = tmp + buf_data[2];
                dummy_speed = 1000 / tmp;
                break;
            case FTDI_OSPN_RD_REV_INTRV:
                tmp = floor ( 1000 / dummy_speed );
                buf_data[3] = tmp & 0x000000FF;
                tmp = tmp >> 8;
                buf_data[4] = tmp & 0x000000FF;
                tmp = tmp >> 8;
                buf_data[5] = tmp & 0x000000FF;

                if ( dummy_is_spinning )
                {
                    buf_data[0] = 0xFF;
                    buf_data[1] = 0xFF;
                    buf_data[2] = 0;

                }else{
                    buf_data[0] = 0;
                    buf_data[1] = 0;
                    buf_data[2] = 0;
                }
                break;
            case FTDI_OSPN_RD_RTR_PRSNT:
                buf_data[0] = RTR_WHEEL_1 | RTR_WHEEL_2 | RTR_WHEEL_3 | RTR_WHEEL_4;
                break;
            case FTDI_OSPN_SEL_SPIN_RTRS:
                dummy_spinners = buf_data[0];
                break;
            case FTDI_OSPN_RD_SPIN_RTRS:
                buf_data[0] = dummy_spinners;
                break;
            case FTDI_OSPN_SPIN_RTRS:
                dummy_is_spinning = 1;
                break;
            case FTDI_OSPN_STOP_RTRS:
                dummy_is_spinning = 0;
                break;
            case FTDI_OSPN_IGN_TTL:
                break;
            case FTDI_OSPN_USE_TTL:
                break;
            case FTDI_OSPN_SET_R1R2_MODE:
            case FTDI_OSPN_SET_R3R4_MODE:
            case FTDI_OSPN_GET_R1R2_MODE:
            case FTDI_OSPN_GET_R3R4_MODE:
                //Since I can't do non-volatile here we might as well ignore this
                break;
			case FTDI_OSPN_USB_GO:
                dummy_pos1 = buf_data[0] & 0x0F;
                dummy_pos2 = ( buf_data[0] & 0xF0 ) >> 4;
                dummy_pos3 = buf_data[1] & 0x0F;
                dummy_pos4 = ( buf_data[1] & 0xF0 ) >> 4;
                break;
			case FTDI_OSPN_GET_ROTOR_POSNS:
                n1n0 = ( ( dummy_pos2 & 0x0F ) << 4 ) + ( dummy_pos1 & 0x0F ) ;
                n3n2 = ( ( dummy_pos4 & 0x0F ) << 4 ) + ( dummy_pos3 & 0x0F ) ;
                buf_data[0] = n1n0;
                buf_data[1] = n3n2;
                break;
            case FTDI_OSPN_WRITE_FILTER_EEPROM:

                break;
            case FTDI_OSPN_READ_FILTER_EEPROM:
                dummy_name[0] = 0;
                sprintf( dummy_name , "Dummy Wheel %d, Position %d." , buf_data[0]+1 , buf_data[1]+1 );
                strcpy( (char *)&buf_data[0] , dummy_name );
                //strcpy( &buf_data[0] , dummy_filter );
                break;
            case FTDI_OSPN_MEASURE_TEMPERATURES:
                break;
            case FTDI_OSPN_READ_TEMPERATURES:
                buf_data[0] = 31;
                buf_data[1] = 32;
                buf_data[2] = 33;
                buf_data[3] = 34;
                break;
            case FTDI_OSPN_SET_STEP_POWER:
                switch ( buf_data[0] )
                {
                    case 0: dummy_pwr1 = buf_data[0]; break;
                    case 1: dummy_pwr2 = buf_data[0]; break;
                    case 2: dummy_pwr3 = buf_data[0]; break;
                    case 3: dummy_pwr4 = buf_data[0]; break;
                }
                break;
            case FTDI_OSPN_READ_STEP_POWER:
                switch ( buf_data[0] )
                {
                    case 0: buf_data[0] = dummy_pwr1; break;
                    case 1: buf_data[0] = dummy_pwr2; break;
                    case 2: buf_data[0] = dummy_pwr3; break;
                    case 3: buf_data[0] = dummy_pwr4; break;
                }
                break;
            case FTDI_OSPN_WRITE_RAM:
                break;
            case FTDI_OSPN_READ_RAM:
                buf_data[0] = 0xAA;
                break;
            case FTDI_OSPN_READ_PROG:
                for ( i=0 ; i<64 ; i++ )
                {
                    buf_data[i] = i & 0xFF;
                }
                break;
            case FTDI_OSPN_GET_READY_STATUS:
                buf_data[0] = 1;
                break;
            case FTDI_OSPN_GET_POWER_STATUS:
                buf_data[0] = 1;
                break;
            case FTDI_OSPN_SWITCH_ON:
                break;
            case FTDI_OSPN_SWITCH_OFF:
                break;
            case FTDI_OSPN_GET_ROTOR_STATUS:
                buf_data[0] = 0xFF;
                break;

            case FTDI_OSPN_SET_BRAKE_TIME:
                switch ( buf_data[0] )
                {
                    case 0: dummy_brake_time1 = buf_data[1]; break;
                    case 1: dummy_brake_time2 = buf_data[1]; break;
                    case 2: dummy_brake_time3 = buf_data[1]; break;
                    case 3: dummy_brake_time4 = buf_data[1]; break;
                }
                break;
            case FTDI_OSPN_GET_BRAKE_TIME:
                switch ( buf_data[0] )
                {
                    case 0: buf_data[0] = dummy_brake_time1; break;
                    case 1: buf_data[0] = dummy_brake_time2; break;
                    case 2: buf_data[0] = dummy_brake_time3; break;
                    case 3: buf_data[0] = dummy_brake_time4; break;
                }
                break;
            default:
                return RTR_ERR_DUMMY_FLUMOXED;
        }
        return RTR_OK;
}

protor_details DO_ALWAYS_INLINE _is_a_rotor ( t_rtr_hnd handle )
{
    protor_details temp;
    if ( DUMMY )
    {
        if ( NULL == handle )
        {
            return (protor_details) DUMMY_FT_HANDLE;
        }
    }
    temp = ( protor_details )handle;
    if ( NULL == temp ) return NULL;
    if ( 'C' != temp->ID[0] ) return NULL;
    if ( 'r' != temp->ID[1] ) return NULL;
    if ( 'n' != temp->ID[2] ) return NULL;
    if ( 'S' != temp->ID[3] ) return NULL;
    if ( 'p' != temp->ID[4] ) return NULL;
    return temp;
}

double _min_spin_speed = RTR_MIN_SPIN_SPEED_HZ;
double _max_spin_speed = RTR_MAX_SPIN_SPEED_HZ;
unsigned int    _min_spin_intrv = 1000000 / RTR_MAX_SPIN_SPEED_HZ;
unsigned int    _max_spin_intrv = 1000000 / RTR_MIN_SPIN_SPEED_HZ;

CALL int CNV rtr_set_min_max_spin_speed ( double min_speed , double max_speed)
{
	_min_spin_speed = min_speed;
	_max_spin_speed = max_speed;
	_min_spin_intrv = (unsigned int) floor ( ( 1000000 / max_speed ) + 0.5 );
	_max_spin_intrv = (unsigned int) floor ( ( 1000000 / min_speed ) + 0.5 );
	return RTR_OK;
}

// Up to 5 Seconds allowed for box to get ready at switch on.
int RTR_USB_FTDI_INIT_BUSY_TIMEOUT = 2000;

// Up to 10 seconds allowed for wandering wheels to come home.
int RTR_USB_FTDI_STEPPING_TIMEOUT = 200;


//int _FTDI_Handle_From_Rotor ( t_rtr_hnd handle , FT_HANDLE *ft_handle )
//{
//    VAR_ME
//    VAR_FT
//    CHECK_INIT
//    CHECK_ME
//    CHECK_FT
//    ft_handle* = ft_hnd;
//    return RTR_OK;
//}

int DO_ALWAYS_INLINE _rtr_mesg_flags( t_rtr_hnd handle , FT_HANDLE ft_hnd , unsigned short msg_id , unsigned char send ,
                                     unsigned char expect , unsigned int rply_wait_ms , int stepping_wait , int purge )
{
    int err;
    double start,now;
    char * oops;
    int done_wait_msg = 0;
    unsigned short *msg;
	unsigned char e_usb;
	unsigned char s_usb;

	if ( DUMMY_FT_HANDLE == ft_hnd ) return _dummy_mesg ( msg_id );
    start = time_stamp_ms( 0 ); // Used with rply_wait_ms AND some of the rotor error message loops
_try_again:
    msg = (unsigned short *)buf_header;
    *msg = msg_id;


    // Send the data and it's two unsigned char message id
	err = _FTDI_Tx ( handle , ft_hnd , 2+send , buf_header );
    if ( err != RTR_OK ) return err;
    // Read the response two unsigned char, error and size information
	if ( rply_wait_ms )
    {
        now = time_stamp_ms( 0 );
        while ( now - start < rply_wait_ms )
        {
            ms_wait  ( 10 );
            err = _FTDI_Rx( handle , ft_hnd , 2 , buf_header );
            if ( err == RTR_OK ) break;
            now = time_stamp_ms ( 0 );
        }
    }else{
        err = _FTDI_Rx( handle , ft_hnd , 2 , buf_header );
    }
    if ( err != RTR_OK ) return err;
	e_usb = buf_header[0];
	s_usb = buf_header[1];
    // Was the Optospin happy?
    if ( e_usb != RTR_USB_RTN_OK )
    {
       switch ( e_usb )
        {
            case RTR_USB_RTN_INIT_BUSY:
                now = time_stamp_ms ( 0 );
                if ( now - start > RTR_USB_FTDI_INIT_BUSY_TIMEOUT )
				{
					_internal_error( RTR_ERR_ROTOR , "Rotor init busy waiting timed out" , ERR_LVL_ABORT );
					return RTR_ERR_ROTOR;
				}
                //This should never actually occur in practice so we'll just keep trying.
                //It's not really an "error" condition it just means the box has only just
                //been switched on. May be more of an issue if Martin ever implements the
                //USB cpontrolled software mains power up, like in the optoscan, but I'd
                //guess the PIC will be much faster than us anyway. AAH
                if ( !done_wait_msg )
                {
                    _do_debug( handle , "Waiting as rotor still initialising......" );
                    done_wait_msg = 1;
                }
                sched_yield ( );
                ms_wait ( 1 );
                goto  _try_again;
            break;

            case RTR_USB_RTN_SPINNING:
                _internal_error( RTR_ERR_ROTOR , "Can't execute in spin mode" , ERR_LVL_ABORT );
                return RTR_ERR_ROTOR;
            break;

            case RTR_USB_RTN_ALREADY:
                _do_debug( handle , "Rotor says: Command was already executed." );
                //_internal_error( RTR_OK , "Command was already executed" , ERR_LVL_INFORM );
                return RTR_OK;
            break;

            case RTR_USB_RTN_TTL:
                _internal_error( RTR_ERR_ROTOR , "TTL/USB command conflict" , ERR_LVL_ABORT );
                return RTR_ERR_ROTOR;
            break;

            case RTR_USB_RTN_STEPPING:
                now = time_stamp_ms ( 0 );
                if ( now - start > RTR_USB_FTDI_STEPPING_TIMEOUT )
				{
					_internal_error( RTR_ERR_ROTOR , "Rotor stepping waiting timed out" , ERR_LVL_ABORT );
					return RTR_ERR_ROTOR;
				}
                if ( 0 != stepping_wait )
                {
                    _do_debug( handle , "Waiting as rotor replied it's still stepping." );
                    sched_yield ( );
                    ms_wait ( 1 );
                    goto  _try_again;
                }
                return RTR_ERR_ROTOR_STEPPING;
            break;

            case RTR_USB_RTN_NO_WHEELS:
                _internal_error( RTR_ERR_ROTOR , "Required wheels not available" , ERR_LVL_ABORT );
                return RTR_ERR_ROTOR;
            break;

            case RTR_USB_RTN_PROTECTED:
                _internal_error( RTR_ERR_ROTOR , "Attempted protected code write" , ERR_LVL_ABORT );
                return RTR_ERR_ROTOR;
            break;

			case RTR_USB_RTN_POST_STEP_REST:
                _do_debug( handle , "Rotor not yet static after stepping" );
                return RTR_ERR_ROTOR_WAITING;
			break;

			default:
                oops = (char *)malloc ( 1024 );
                oops[0] = 0;
                sprintf( oops , "Unknown rotor error report:%d" , e_usb );
                _internal_error ( RTR_ERR_ROTOR , oops , ERR_LVL_ABORT );
                free ( oops );
                return RTR_ERR_ROTOR;
        }
    }

    // Did we get the expected number of unsigned chars as a response
    if ( expect != s_usb )
    {
		_internal_error ( RTR_ERR_INCOMPLETE , "Rotor USB packet size error" , ERR_LVL_ABORT );
        FT_Purge( ft_hnd , FT_PURGE_RX | FT_PURGE_TX );
        return RTR_ERR_INCOMPLETE;
    }
    // All ok so read the data
    if ( expect )
    {
        err = _FTDI_Rx( handle , ft_hnd , expect , buf_data );
        if ( err != RTR_OK )
		{
			_internal_error ( RTR_ERR_INCOMPLETE , "Rotor USB data packet complete read error" , ERR_LVL_ABORT );
	        FT_Purge( ft_hnd , FT_PURGE_RX | FT_PURGE_TX );
			return err;
		}
    }

    if ( purge != 0 )
    {
        FT_Purge( ft_hnd , FT_PURGE_RX | FT_PURGE_TX );
    }
    return RTR_OK;
}

//Dafault flags settings rotor message
int DO_ALWAYS_INLINE _rtr_mesg( t_rtr_hnd handle , FT_HANDLE ft_hnd , unsigned short msg_id , unsigned char send , unsigned char expect )
{
    return _rtr_mesg_flags ( handle , ft_hnd , msg_id , send , expect , 0 , 0 , 0 );
}

CALL int CNV rtr_do_lib_init ( unsigned char *maj , unsigned char *min )
{
	platform_init ();
    _init_errors ( );
    *maj = LIBRARY_MAJ_VERSION;
    *min = LIBRARY_MIN_VERSION;
	rtr_set_min_max_spin_speed ( RTR_MIN_SPIN_SPEED_HZ , RTR_MAX_SPIN_SPEED_HZ );
	buf_size = DEFAULT_BUFFER_SIZE;
    buf_header = (unsigned char* )malloc ( buf_size + 2 );
    buf_data = buf_header;
    buf_data++;
    buf_data++;
    #ifdef LINUX
    FT_SetVIDPID( 0x156B,0x0003 );
    #endif
    _lib_inited = t_bool_true;
    _do_debug ( NULL , "Optospin library initiated.");
    return RTR_OK;
}

CALL int CNV rtr_do_lib_term ( void )
{
    CHECK_INIT
    _do_debug ( NULL , "Optospin library terminating.");
    rtr_do_clear_all_hooks();
    while ( _rtr_details_list )
    {
        rtr_do_device_close( _rtr_details_list );
    }
    if ( dummy_name ) free ( dummy_name );
    if ( buf_header )
    {
        buf_size = 0;
        free ( buf_header );
        buf_header = NULL;
        buf_data = NULL;
    }
    _FTDI_Lib_Term ( );
    _term_errors ( );
    return RTR_OK;
}

// Undocumented way of changing power limits;
CALL int CNV rtr_set_min_max_power ( t_pwr_lvl min , t_pwr_lvl max  )
{
    MIN_POWER = min;
    MAX_POWER = max;
    return RTR_OK;
}

CALL int CNV rtr_set_prog_hook ( t_prog_callback callback  )
{
    _progress = callback;
    if ( _progress ) _progress( NULL , 0 , 1 );
    _do_debug ( NULL , "Progress indicator hook installed." );
    return RTR_OK;
}


void _decode_wheel_pos ( protor_details me , unsigned char datum , int base , int mode )
{
	unsigned char combined;
	combined = ( ( 0x80 & datum ) == 0x80 );
	if ( combined )
	{
		me->mode = me->mode + mode;
		me->whl_pos[base] = ( datum & 0x0F );
		me->whl_pos[base+1] = 0;
		me->whl_max[base] = 10;
		me->whl_max[base+1] = 0;
	}else{
		me->whl_pos[base] = ( datum & 0x07 );
		datum = datum >> 4;
		me->whl_pos[base+1] = ( datum & 0x07 );
		me->whl_max[base] = 6;
		me->whl_max[base+1] = 6;
	}
	return;
}

int _load_wheel_posns ( protor_details me )
{
	int err;
	me->mode = RTR_MODE_NORMAL;
	err = _rtr_mesg ( me , me->ft_handle , FTDI_OSPN_GET_ROTOR_POSNS , 0 , 2 );
    if ( RTR_OK != err )
    {
        _internal_error ( err , "Can't get current wheel positions" , ERR_LVL_ABORT );
        return err;
	}
    _decode_wheel_pos( me , buf_data[0] , 0 , RTR_MODE_COMBINED_A );
    _decode_wheel_pos( me , buf_data[1] , 2 , RTR_MODE_COMBINED_B );
    if ( _debug_callback_installed ( ) )
    {
        char * str = ( char * ) malloc ( 1024 );
        str[0] = 0;
        sprintf( str , "Loaded current wheel positions: %d,%d,%d,%d" , me->whl_pos[0] , me->whl_pos[1] , me->whl_pos[2] , me->whl_pos[3] );
        _do_debug ( me , str );
        free ( str );
    }
    return RTR_OK;
}

/*  if _decode_wheel_pos() works then nuke this shit!!!
	datum = buf_data[0];
	combined = ( ( 0x80 & datum ) == 0x80 );
	if ( combined )
	{
		me->mode = me->mode + RTR_MODE_COMBINED_A;
		me->whl_pos[0] = ( datum & 0x0F );
		me->whl_pos[1] = 0;
		me->whl_max[0] = 10;
		me->whl_max[1] = 0;
	}else{
		me->whl_pos[0] = ( datum & 0x07 );
		datum = datum >> 4;
		me->whl_pos[1] = ( datum & 0x07 );
		me->whl_max[0] = 6;
		me->whl_max[1] = 6;
	}
	datum = buf_data[1];
	combined = ( ( 0x80 & datum ) == 0x80 );
	if ( combined )
	{
		me->mode = me->mode + RTR_MODE_COMBINED_B;
		me->whl_pos[2] = ( datum & 0x0F );
		me->whl_pos[3] = 0;
		me->whl_max[2] = 10;
		me->whl_max[3] = 0;
	}else{
		me->whl_pos[2] = ( datum & 0x07 );
		datum = datum >> 4;
		me->whl_pos[3] = ( datum & 0x07 );
		me->whl_max[2] = 6;
		me->whl_max[3] = 6;
	}
*/

int _load_wheel_spins ( protor_details me )
{
    int err = _rtr_mesg ( me , me->ft_handle , FTDI_OSPN_RD_SPIN_RTRS , 0 , 1 );
    if ( RTR_OK != err )
    {
        _internal_error ( err , "Unable to read spinning information from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    if ( ( RTR_WHEEL_1 & buf_data[0] ) == 0 ) me->wh_spins[0] = t_bool_false; else me->wh_spins[0] = t_bool_true;
    if ( ( RTR_WHEEL_2 & buf_data[0] ) == 0 ) me->wh_spins[1] = t_bool_false; else me->wh_spins[1] = t_bool_true;
    if ( ( RTR_WHEEL_3 & buf_data[0] ) == 0 ) me->wh_spins[2] = t_bool_false; else me->wh_spins[2] = t_bool_true;
    if ( ( RTR_WHEEL_4 & buf_data[0] ) == 0 ) me->wh_spins[3] = t_bool_false; else me->wh_spins[3] = t_bool_true;
    return RTR_OK;
}

int _fw_reset ( t_rtr_hnd handle , FT_HANDLE ft_hnd )
{
	int res;
	_do_debug ( NULL , "Doing Optospin firmware reset." );
    res = _FTDI_Issue_FW_Reset( handle , ft_hnd );
    if ( res != RTR_OK )
    {
        _internal_error ( res , "Unable to reset Optospin USB I/O" , ERR_LVL_ABORT );
        return res;
    }
    ms_wait ( 250 );
    FT_Purge( ft_hnd , FT_PURGE_RX | FT_PURGE_TX );
_wait_longer:
    _do_debug( NULL , "Waiting for wheels to be ready." );
    res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_GET_READY_STATUS , 0 , 1 );
    if ( res != RTR_OK )
    {
        _internal_error ( res , "Unable to init stop Optospin" , ERR_LVL_ABORT );
        return res;
    }
    if ( 0 == buf_data[0] ) goto _wait_longer;
    return RTR_OK;
}

int _ensure_wheels_stopped ( t_rtr_hnd me , FT_HANDLE ft_handle )
{
 	int res,timeout;
    _do_debug( NULL , "Checking for spinning rotors." );
    res = _rtr_mesg ( me , ft_handle , FTDI_OSPN_GET_ROTOR_STATUS , 0 , 1 );
    if ( res != RTR_OK )
    {
        _internal_error ( res , "Unable to read Optospin status" , ERR_LVL_ABORT );
        return res;
    }
    // The rotors are spinning! Whilst this is not really an error condition I've
    // chosen to stop the buggers so the wheels always start in a known state.
    if ( 0 == ( buf_data[0] & 0x80 ) )
    {
        _do_debug( NULL , "Spinning rotor found, stopping." );
        res = _rtr_mesg ( me , ft_handle , FTDI_OSPN_STOP_RTRS , 0 , 0 );
        if ( res != RTR_OK )
        {
            _internal_error ( res , "Unable to init stop Optospin" , ERR_LVL_ABORT );
            return res;
        }
        _do_debug( NULL , "500ms initial wheel stop wait." );
        ms_wait ( 500 );
        timeout = 0;
        while ( ( buf_data[0] & 0x80 ) == 0 )
        {
            res = _rtr_mesg ( me , ft_handle , FTDI_OSPN_GET_ROTOR_STATUS , 0 , 1 );
            if ( res != RTR_OK )
            {
                _internal_error ( res , "Unable to read Optospin status" , ERR_LVL_ABORT );
                return res;
            }
            _do_debug( NULL , "Extra 100ms wheel stop wait." );
            ms_wait( 100 );
            timeout++;
            if ( timeout > 50 )
            {
                res = RTR_ERR_TIMEOUT;
                _internal_error ( res , "Unable to stop the Optospin wheels" , ERR_LVL_ABORT );
                return res;
            }
        }
        _do_debug( me , "Nothing succeeds like a bird with no beak!!!" );
    }
    return RTR_OK;
}

CALL int CNV rtr_get_device_by_serial ( char *serial , t_rtr_hnd *handle , t_bool reset )
{
    int res;
    protor_details new_details;
    FT_HANDLE ft_handle = 0;
    CHECK_INIT
    if ( DUMMY )
    {
        res = strcmp( serial , DUMMY_NAME );
        if ( !res )
        {
            if ( NULL == dummy_name )
                dummy_name = (char *)malloc ( 512 );
            *handle = NULL;
            return RTR_OK;
        }
    }

    // Open a connection to the USB chip.
    res = _FTDI_Open_Channel ( serial , &ft_handle );
    if ( res != RTR_OK ) return res;


    _do_debug( NULL , "Checking power status of rotor system." );
    res = _rtr_mesg ( NULL , ft_handle , FTDI_OSPN_GET_POWER_STATUS , 0 , 1 );
    if ( res != RTR_OK )
    {
        _internal_error ( res , "Unable to read power status" , ERR_LVL_ABORT );
        goto _err_with_ftdi;
    }
    if ( 0 == buf_data[0] )
    {
        _internal_error ( RTR_ERR_NOT_INIT , "Rotor system not powered up" , ERR_LVL_ABORT );
        goto _err_with_ftdi;
    }


    // Firmware Reset Option...
    // Don't normally do this as it causes a sllooowww reset to wheel position
    // one on all wheels, which is probably undisirable as a feature anyway,
    // and takes fekin' aaaaaages. Left in for firmware upload debugging atm.
    if ( t_bool_true == reset )
    {
        res = _FTDI_Issue_FW_Reset ( NULL , ft_handle );
        if ( RTR_OK != res ) goto _err_with_ftdi;

    }else{
        _do_debug( NULL , "Initialising rotors USB I/O system." );
        res = _rtr_mesg ( NULL , ft_handle , FTDI_OSPN_INIT_USB , 0 , 0 );
        if ( res != RTR_OK )
        {
            _internal_error ( res , "Unable to initialise Optospin USB I/O" , ERR_LVL_ABORT );
            goto _err_with_ftdi;
        }
    }
    _do_debug ( NULL , "Checking Optospin status." );

    if ( _FTDI_Purge_IO ( ft_handle ) != RTR_OK ) goto _err_with_ftdi;

    _do_debug ( NULL , "Ensuring Optospin wheels are stopped." );
    res = _ensure_wheels_stopped( NULL , ft_handle );
    if ( RTR_OK != res ) goto _err_with_ftdi;


    _do_debug ( NULL , "Reading Optospin wheel present data." );
    res = _rtr_mesg ( NULL , ft_handle , FTDI_OSPN_RD_RTR_PRSNT , 0 , 1 );
    if ( res != RTR_OK )
    {
        _internal_error ( res , "Unable to read wheels data flags" , ERR_LVL_ABORT );
        goto _err_with_ftdi;
    }
    _do_debug ( NULL , "Opened rotor USB connection, creating data structure.");
	new_details = _create_new_rotor_details ( buf_data[0] , serial , ft_handle );
	res = _load_wheel_posns( new_details );
    if ( RTR_OK != res )  goto _err_with_details;
    res = _load_wheel_spins( new_details );
    if ( RTR_OK != res )  goto _err_with_details;
    res = _rtr_mesg ( new_details , ft_handle , FTDI_OSPN_VERSION , 0 , 2 );
    if ( RTR_OK != res )  goto _err_with_details;
    new_details->fw_ver_maj = buf_data[0];
    new_details->fw_ver_min = buf_data[1];
    *handle = (t_rtr_hnd)new_details;
    return RTR_OK;
_err_with_details:
    free ( new_details );
_err_with_ftdi:
    FT_Close( ft_handle );
    return res;
}

CALL int CNV rtr_get_wheel_count ( t_rtr_hnd handle , t_whl_idx *count )
{
	VAR_ME
	CHECK_INIT
    CHECK_ME
    *count = me->wh_count;
    return RTR_OK;
}

CALL int CNV rtr_get_mode ( t_rtr_hnd handle , t_rtr_mode *mode )
{
	VAR_ME
	VAR_FT
    int res;
    unsigned char R1,R2,R3,R4;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    // Start as 0 = all wheels seperate
    *mode = RTR_MODE_NORMAL;
    res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_GET_R1R2_MODE , 0 , 2 );
    if ( RTR_OK != res )  goto _err_reading;
    R1 = buf_data[0];
    R2 = buf_data[1];
    res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_GET_R3R4_MODE , 0 , 2 );
    if ( RTR_OK != res )  goto _err_reading;
    R3 = buf_data[0];
    R4 = buf_data[1];
    // See if someone has been swapping miss-configured wheels
    if ( R1 != R2 )
    {
        _internal_error ( RTR_ERR_NOT_INIT , "A wheels in incompatible modes, resetting to normal mode" , ERR_LVL_NOTE );
        buf_data[0] = 0;
        res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R1R2_MODE , 1 , 0 );
        if ( RTR_OK != res )  goto _err_reading;
        R1 = 0;
    }
    if ( R3 != R4 )
    {
        _internal_error ( RTR_ERR_NOT_INIT , "B wheels in incompatible modes, resetting to normal mode" , ERR_LVL_NOTE );
        buf_data[0] = 0;
        res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R3R4_MODE , 1 , 0 );
        if ( RTR_OK != res )  goto _err_reading;
        R1 = 0;
    }

    if ( me->whl_found[0] && me->whl_found[1] )
    {
        switch ( R1 )
        {
            case 1:
                *mode = *mode + RTR_MODE_COMBINED_A;
                break;
            case 2:
                *mode = *mode + RTR_MODE_INERTIAL_A;
                break;
        }
    }else{
        if ( R1 | R2 )  //There aren't 2 wheels here so put it in normal mode
        {
            _internal_error ( RTR_OK , "Only 1 A wheel found, resetting to normal mode" , ERR_LVL_NOTE );
            buf_data[0] = 0;
            res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R1R2_MODE  , 1 , 0 );
            if ( RTR_OK != res )  goto _err_writing;
        }
    }
    if ( me->whl_found[3] && me->whl_found[4] )
    {
        switch ( R3 )
        {
            case 1:
                *mode = *mode + RTR_MODE_COMBINED_B;
                break;
            case 2:
                *mode = *mode + RTR_MODE_INERTIAL_B;
                break;
        }
    }else{
        if ( R3 | R4 )  //There aren't 2 wheels here so put it in normal mode
        {
            _internal_error ( RTR_OK , "Single R3R4 wheel, resetting to normal mode" , ERR_LVL_NOTE );
            buf_data[0] = 0;
            res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R3R4_MODE  , 1 , 0 );
            if ( RTR_OK != res )  goto _err_writing;
        }
    }
    me->mode = *mode;
    return RTR_OK;
_err_reading:
    _internal_error ( res , "Unable to read rotor modes" , ERR_LVL_ABORT );
    return res;
_err_writing:
    _internal_error ( res , "Unable to write rotor modes" , ERR_LVL_ABORT );
    return res;
}



CALL int CNV rtr_set_mode ( t_rtr_hnd handle , t_rtr_mode mode )
{
	VAR_ME
	VAR_FT
    int res;
	unsigned char R1R2,R3R4;
    CHECK_INIT
    CHECK_ME
    CHECK_FT

    R1R2 = 0;
    R3R4 = 0;
    if ( mode )
    {
        if ( mode & RTR_MODE_COMBINED_A )
        {
            R1R2 = 1;
        }else{
            if ( mode & RTR_MODE_INERTIAL_A )
            {
                R1R2 = 2;
            }
        }
        if ( mode & RTR_MODE_COMBINED_B )
        {
            R3R4 = 1;
        }else{
            if ( mode & RTR_MODE_INERTIAL_B )
            {
                R3R4 = 2;
            }
        }
    }

    buf_data[0] = R1R2;
    res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R1R2_MODE  , 1 , 0 );
    if ( RTR_OK != res ) goto _err_writing;

    buf_data[0] = R3R4;
    res = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_R3R4_MODE  , 1 , 0 );
    if ( RTR_OK != res ) goto _err_writing;

	me->mode = mode;

    return RTR_OK;
_err_writing:
    _internal_error ( res , "Unable to store current mode setting in rotor wheels" , ERR_LVL_ABORT );
    return res;
}

CALL int CNV rtr_get_wheel_position_max ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos *filters )
{
	VAR_ME
	CHECK_ME
    CHECK_INIT
    CHECK_WHEEL
    if ( !filters )
    {
        _internal_error ( RTR_ERR_INVALID_PARAM , "Invalid rtr_get_wheel_data filter parameter, * int required" , ERR_LVL_ABORT );
        return RTR_ERR_INVALID_PARAM;
    }
    *filters = me->whl_max[wheel-1];
    return RTR_OK;
}

#define WHEEL_POS_INVALID(mode,whl,pos)  \
    sprintf(msg,"Invalid %s mode wheel(%d) position(%d) request",mode,whl,pos);  \
    _internal_error ( RTR_ERR_INVALID_PARAM , msg , ERR_LVL_ABORT ); \
	return RTR_ERR_INVALID_PARAM

CALL int CNV rtr_set_wheel_positions ( t_rtr_hnd handle , t_whl_pos *pos ) // 0 == stay where you are
{
	VAR_ME
	VAR_FT
    char msg[64];
	int err;
	unsigned char n0;
	unsigned char n1;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    // Do some error checking on the data
//    sprintf(msg,"Mode is 0x%x in  rtr_set_wheel_positions." ,me->mode);
//    _internal_error( RTR_ERR_MESSAGE , msg , ERR_LVL_NOTE );
//    sprintf(msg,"Positions %d:%d:%d:%d in rtr_set_wheel_positions." , pos[0],pos[1],pos[2],pos[3]);
//    _internal_error( RTR_ERR_MESSAGE , msg , ERR_LVL_NOTE );

	if ( me->mode & RTR_MODE_COMBINED_A )
	{
		if ( pos[0] > 10 ) { WHEEL_POS_INVALID("combined",1,pos[0]); }
		if ( pos[1] != 0 ) { WHEEL_POS_INVALID("combined",2,pos[1]); }
	}else{
	    if ( me->mode & RTR_MODE_INERTIAL_A )
        {
            if ( pos[0] > 6 ) { WHEEL_POS_INVALID("inertial",1,pos[0]); }
        }else{
            if ( pos[0] > 6 ) { WHEEL_POS_INVALID("normal",1,pos[0]); }
            if ( pos[1] > 6 ) { WHEEL_POS_INVALID("normal",1,pos[0]); }
        }
	}
	if ( me->mode & RTR_MODE_COMBINED_B )
	{
		if ( pos[2] > 10 ) { WHEEL_POS_INVALID("combined",3,pos[2]); }
		if ( pos[3] != 0 ) { WHEEL_POS_INVALID("combined",4,pos[3]); }
	}else{
	    if ( me->mode & RTR_MODE_INERTIAL_B )
        {
            if ( pos[2] > 6 ) { WHEEL_POS_INVALID("inertial",3,pos[2]); }
        }else{
            if ( pos[2] > 6 ) { WHEEL_POS_INVALID("normal",3,pos[2]); }
            if ( pos[3] > 6 ) { WHEEL_POS_INVALID("normal",4,pos[3]); }
        }
	}
    // Use 0 as a stay in the same place flag
	for ( err = 0 ; err < RTR_BOX_MAX_WHEEL_COUNT ; err++ );
	{
		if ( !pos[err] )
            pos[err] = me->whl_pos[err]+1;
	}
    // Clear the USB a data packet variables

//    sprintf(msg,"Positions %d:%d:%d:%d in rtr_set_wheel_positions." , pos[0],pos[1],pos[2],pos[3]);
//    _internal_error( RTR_ERR_MESSAGE , msg , ERR_LVL_NOTE );

	n0 = 0;
	n1 = 0;
    // Build a data packet of the correct format
    if ( me->mode & RTR_MODE_INERTIAL_A )
        pos[1] = pos[0];
    if ( me->mode & RTR_MODE_INERTIAL_B )
        pos[3] = pos[2];

	if ( me->mode & RTR_MODE_COMBINED_A )
	{
		n0 = 0x30 + pos[0];
	}else{
        n0 = pos[1];
        n0 = n0 << 4;
        n0 = n0 + ( pos[0] );
	};
	if ( me->mode & RTR_MODE_COMBINED_B )
	{
		n1 = 0x30 + pos[2];
	}else{
        n1 = pos[3];
		n1 = n1 << 4;
		n1 = n1 + ( pos[2]  );
	};
    // Put the data packet data into the USB buffer
	buf_data[0] = n0;
	buf_data[1] = n1;

//    sprintf(msg,"Sending 0x%x:0x%x over USB in set_wheel_positions." , n0 , n1 );
//    _internal_error( RTR_ERR_MESSAGE , msg , ERR_LVL_NOTE );

	err = _rtr_mesg ( me , ft_hnd , FTDI_OSPN_USB_GO , 2 , 0 );
	if ( err == RTR_ERR_ROTOR_WAITING )
	{
        _internal_error ( err , "Rotor now says not ready" , ERR_LVL_ABORT );
	}
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to step the Optospin wheels" , ERR_LVL_ABORT );
        return err;
    }
    _do_debug(handle,"Rotor move requested.");
    return RTR_OK;
}

/*dep
CALL int CNV rtr_set_wheel_position_indep ( t_rtr_hnd handle , t_whl_idx raw_wheel , t_whl_pos pos )
{
	VAR_ME
	VAR_FT
//	VAR_WHEEL
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_POS
	CHECK_RAW_WHEEL
    me->whl_pos[raw_wheel-1] = pos-1;
    buf_data[0] = me->whl_pos[0];
    buf_data[1] = me->whl_pos[1];
    buf_data[2] = me->whl_pos[2];
    buf_data[3] = me->whl_pos[3];
    _do_debug( handle , "FTDI_OSPN_USB_INDP_GO" );
	err = _rtr_mesg ( ft_hnd , FTDI_OSPN_USB_INDP_GO , 4 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to step the Optospin wheels" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}
*/


CALL int CNV rtr_get_wheel_position ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos *pos )
{
	VAR_ME
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_WHEEL
    err = _load_wheel_posns( me );
    if ( RTR_OK != err ) return err;
    *pos = me->whl_pos[wheel-1];
    return RTR_OK;
}

CALL int CNV rtr_get_wheel_positions ( t_rtr_hnd handle , t_whl_pos *pos )
{
	VAR_ME
	int err;
	CHECK_INIT
    CHECK_ME
    err = _load_wheel_posns( me );
    if ( RTR_OK != err ) return err;
    for ( err = 0 ; err < RTR_BOX_MAX_WHEEL_COUNT ; err++ )
	{
		pos[err] = me->whl_pos[err];
	}
	return RTR_OK;
}



/* dep
CALL int CNV rtr_get_wheel_position_indep ( t_rtr_hnd handle , t_whl_idx raw_wheel , t_whl_pos *pos )
{
	VAR_ME
//	VAR_WHEEL
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_RAW_WHEEL
    err = _load_wheel_posns( me );
    if ( RTR_OK != err ) return err;
    *pos = me->whl_pos[raw_wheel-1]+1;
    return RTR_OK;
}
*/

CALL int CNV rtr_set_usb_timeouts ( t_rtr_hnd handle , int read , int write  )
{
	VAR_ME
	VAR_FT
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    _read_timeout = read;
    _write_timeout = write;
    return _FTDI_Set_Timeouts ( handle , ft_hnd , read , write );
}

CALL int CNV rtr_do_usb_timeout_default ( t_rtr_hnd handle )
{
	VAR_ME
	VAR_FT
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    return _FTDI_Default_Timeouts( handle , ft_hnd );
}

CALL int CNV rtr_get_firmware ( t_rtr_hnd handle , unsigned char * buffer , int * buf_size )
{
	VAR_ME
	VAR_FT
    unsigned char * ptr;
    unsigned int i;
    int err,j;
    int size;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    size = 0;
    ptr = buffer;
    if ( _progress ) _progress ( handle , 0 , 959 );
    for ( i=0 ; i<960 ; i++ )
    {
        size = size + 64;
        if ( size > *buf_size )
        {
            _internal_error ( RTR_ERR_BUFFER_OVERFLOW , "Failed to rip firmware" , ERR_LVL_INFORM );
            return RTR_ERR_BUFFER_OVERFLOW;
        }
        buf_data[1] = i & 0x000000FF;
        buf_data[0] = ( i >> 8 ) & 0x000000FF;
        err = _rtr_mesg( handle , ft_hnd , FTDI_OSPN_READ_PROG , 2 , 64 );
        if ( RTR_OK != err )
        {
            _internal_error ( err , "Unable to read Optospin firmware update" , ERR_LVL_INFORM );
            return err;
        }
        for ( j=0 ; j<64 ; j++ )
        {
            *ptr = buf_data[j];
            ptr++;
        }
        if ( _progress ) _progress ( handle , i , 959 );
    }
    *buf_size = size;
    if ( _progress ) _progress ( handle , 0 , 959 );
    return RTR_OK;
}

CALL int CNV rtr_set_firmware ( t_rtr_hnd handle , unsigned char * buffer )
{
	VAR_ME
 	VAR_FT
    unsigned int i;
    int err,j;
    unsigned char * ptr;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    ptr = buffer;
    if ( _progress ) _progress ( handle , 0 , 958 );
    for ( i=0 ; i<959 ; i++ )
    {
        buf_data[1] = i & 0x000000FF;
        buf_data[0] = ( i >> 8 ) & 0x000000FF;
        for ( j=2 ; j<66 ; j++ )
        {
            buf_data[j] = *ptr;
            ptr++;
        }
        err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_WRITE_PROG , 66 , 0 );
        if ( RTR_OK != err )
        {
            _internal_error ( err , "Unable to transmit Optospin firmware update" , ERR_LVL_INFORM );
            return err;
        }
        if ( _progress ) _progress ( handle , i , 958 );
    }
    if ( _progress ) _progress ( handle , 0 , 958 );
    return RTR_OK;
}


CALL int CNV rtr_do_reinitialise ( t_rtr_hnd handle )
{
	VAR_ME
	VAR_FT
//    int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    FT_Purge( ft_hnd , FT_PURGE_RX | FT_PURGE_TX );
    _rtr_mesg( handle , ft_hnd , FTDI_OSPN_INIT_USB , 0 , 0 );
//    err = _rtr_mesg( ft_hnd , FTDI_OSPN_INIT_USB , 0 , 0 );
//    if ( RTR_OK != err )
//    {
//        _internal_error ( err , "Unable to read version information from the Optospin" , ERR_LVL_ABORT );
//        return err;
//    }

    return RTR_OK;
}

CALL int CNV rtr_get_fw_version ( t_rtr_hnd handle , unsigned char *maj , unsigned char *min )
{
	VAR_ME
	CHECK_INIT
    CHECK_ME
    *maj = me->fw_ver_maj;
    *min = me->fw_ver_min;
    return RTR_OK;
}

CALL int CNV rtr_do_fw_reset ( t_rtr_hnd handle )
{
    VAR_ME
    VAR_FT
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    if ( DUMMY_FT_HANDLE == ft_hnd ) return RTR_OK;
    return _FTDI_Issue_FW_Reset( handle , ft_hnd );
}

CALL int CNV rtr_get_temps ( t_rtr_hnd handle , t_whl_idx wheel , t_temp *temp )
{
	VAR_ME
    VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    *temp = 0;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_READ_TEMPERATURES , 0 , 4 );
    if ( RTR_OK != err )
    {
        _internal_error ( FT_OK , "Unable to read temperature information from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    *temp = buf_data[wheel-1];
    return RTR_OK;
}

CALL int CNV rtr_do_measure_temps ( t_rtr_hnd handle )
{
	VAR_ME
    VAR_FT
    int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_MEASURE_TEMPERATURES , 0 , 0 );
    if ( RTR_OK != err )
    {
        _internal_error ( err , "Unable to measure temperature information from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}

CALL int CNV rtr_get_step_power ( t_rtr_hnd handle , t_whl_idx wheel , t_pwr_lvl * power , t_decel_fact * decel_fact)
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    buf_data[0] = (wheel - 1) & 0x00FF;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_READ_STEP_POWER , 1 , 2 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read step power information from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    *power = buf_data[0];
    *decel_fact = buf_data[1];
    return RTR_OK;
}

CALL int CNV rtr_set_step_power ( t_rtr_hnd handle , t_whl_idx wheel , t_pwr_lvl power , t_decel_fact decel_fact )
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    CHECK_POWER
    buf_data[0] = (wheel - 1) & 0x00FF;
    buf_data[1] = power;
    buf_data[2] = decel_fact;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_STEP_POWER , 3 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set step power information from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}

CALL int CNV rtr_get_spin_wheel ( t_rtr_hnd handle , t_whl_idx wheel , t_bool *would_spin )
{
	VAR_ME
    CHECK_INIT
    CHECK_ME
    CHECK_WHEEL;
    if ( me->wh_spins[wheel - 1] )
        *would_spin = t_bool_true;
    else
        *would_spin = t_bool_false;
    return RTR_OK;
}

CALL int CNV rtr_set_spin_wheels ( t_rtr_hnd handle , t_wheels wheels )
{
	VAR_ME     // Set up for the "me" record of the rotor system
	VAR_FT
    int err;
	unsigned char spin;
	CHECK_INIT
    CHECK_ME    //Ensure the rotor handle is valid and get the correct "me" record
    CHECK_FT
    if ( 0 == ( wheels & RTR_WHEEL_1 ) ) me->wh_spins[0] = 0; else me->wh_spins[0] = 1;
    if ( 0 == ( wheels & RTR_WHEEL_2 ) ) me->wh_spins[1] = 0; else me->wh_spins[1] = 1;
    if ( 0 == ( wheels & RTR_WHEEL_3 ) ) me->wh_spins[2] = 0; else me->wh_spins[2] = 1;
    if ( 0 == ( wheels & RTR_WHEEL_4 ) ) me->wh_spins[3] = 0; else me->wh_spins[3] = 1;
    spin = 0;
    if ( me->wh_spins[0] ) spin = spin + 0x01;
    if ( me->wh_spins[1] ) spin = spin + 0x02;
    if ( me->wh_spins[2] ) spin = spin + 0x04;
    if ( me->wh_spins[3] ) spin = spin + 0x08;
    buf_data[0] = spin;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SEL_SPIN_RTRS , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set spinning wheels of the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}

CALL int CNV rtr_get_spin_wheels ( t_rtr_hnd handle , t_wheels * wheels )
{
	VAR_ME     // Set up for the "me" record of the rotor system
	CHECK_INIT
    CHECK_ME    //Ensure the rotor handle is valid and get the correct "me" record
    *wheels = 0;
    if ( me->wh_spins[0] ) *wheels = *wheels + RTR_WHEEL_1;
    if ( me->wh_spins[1] ) *wheels = *wheels + RTR_WHEEL_2;
    if ( me->wh_spins[2] ) *wheels = *wheels + RTR_WHEEL_3;
    if ( me->wh_spins[3] ) *wheels = *wheels + RTR_WHEEL_4;
    return RTR_OK;
}

CALL int CNV rtr_set_spin_wheel ( t_rtr_hnd handle , t_whl_idx wheel , t_bool would_spin )
{
	VAR_ME
	VAR_FT
    int err;
	unsigned char spin;
	unsigned char cur_set;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    me->wh_spins[wheel - 1] = would_spin;
    spin = 0;
    if ( me->wh_spins[0] ) spin = spin + 0x01;
    if ( me->wh_spins[1] ) spin = spin + 0x02;
    if ( me->wh_spins[2] ) spin = spin + 0x04;
    if ( me->wh_spins[3] ) spin = spin + 0x08;
	//cur_set = buf_data[0];
    buf_data[0] = spin;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SEL_SPIN_RTRS , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set spinning wheels of the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}

CALL int CNV rtr_get_ready_to_acqu ( t_rtr_hnd handle , t_bool* ready )
{
	VAR_ME
	VAR_FT
    int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_READY_STATUS , 0 , 1 , 0 , 1 , 0);
    if  ( err == RTR_ERR_ROTOR_STEPPING )
    {
        buf_data[0] = 0; // Flag this as a busy = !ready condition but not an error
		err = RTR_OK;
    }else{
	    if  ( err == RTR_ERR_ROTOR_WAITING )
		{
			buf_data[0] = 1; // Flag this as a ready now, don't ask me why, ask Martin
			err = RTR_OK;
		}else{
			if ( err != RTR_OK )
	        {
	            _internal_error ( err , "Unable to read ready information from the Optospin from acq" , ERR_LVL_ABORT );
	        }
		}
    }
    *ready = ( buf_data[0] == 1 ) ;
    if ( _debug_callback_installed ( ) )
    {
        if ( *ready )
        {
            _do_debug( NULL , "Ready to acquire status polled: ready." );
        }else{
            _do_debug( NULL , "Ready to acquire status polled: not ready." );
        }
    }
    return err;
}

CALL int CNV rtr_get_ready_to_move ( t_rtr_hnd handle , t_bool* ready )
{
	VAR_ME
	VAR_FT
    int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    *ready = t_bool_false;
	err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_READY_STATUS , 0 , 1 , 0 , 1 , 0);
	if ( err == RTR_ERR_ROTOR_WAITING ) return RTR_OK;
	if  ( err == RTR_ERR_ROTOR_STEPPING ) return RTR_OK;
    if ( err != RTR_OK )
	{
		_internal_error ( err , "Unable to read move ready information from the Optospin" , ERR_LVL_HALT );
	}
    if ( buf_data[0] == 1 ) *ready = t_bool_true;
	if ( _debug_callback_installed ( ) )
    {
        if ( *ready )
        {
            _do_debug( NULL , "Ready to move status polled: ready." );
        }else{
            _do_debug( NULL , "Ready to move status polled: not ready." );
        }
    }
    return err;
}

CALL int CNV rtr_do_wait_ready_to_move ( t_rtr_hnd handle , t_bool* ready )
{
	VAR_ME
	VAR_FT
    int err,time;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
	time = 0;
try_again:
	err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_READY_STATUS , 0 , 1 , 0 , 1 , 0);
	if ( err == RTR_ERR_ROTOR_WAITING )
	{
		if ( time > 250 )
		{
			_internal_error ( RTR_ERR_TIMEOUT , "Unable to move - still not even stopped" , ERR_LVL_HALT );
			buf_data[0] = 0;
			goto now_done;
		}
		time++;
		ms_wait ( 1 );
		goto try_again;
	}
	if  ( err == RTR_ERR_ROTOR_STEPPING )
    {
        _internal_error ( err , "Unable to move - not even stopped" , ERR_LVL_HALT );
		buf_data[0] = 0;
		goto now_done;
    }
    if ( err != RTR_OK )
	{
		_internal_error ( err , "Unable to read ready information from the Optospin from move" , ERR_LVL_HALT );
	}

now_done:
    (*ready) = ( buf_data[0] == 1 );
	if ( _debug_callback_installed ( ) )
    {
        if ( *ready )
        {
            _do_debug( NULL , "Ready to move wait over: ready." );
        }else{
            _do_debug( NULL , "Ready to move wait over: not ready." );
        }
    }
    return err;
}

CALL int CNV rtr_get_status ( t_rtr_hnd handle , t_rtr_status *status )
{
	VAR_ME
	VAR_FT
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_ROTOR_STATUS , 0 , 1 , 0 , 0 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read Optospin status for ready signals" , ERR_LVL_ABORT );
        return err;
    }
    *status = buf_data[0];
    return RTR_OK;
}

CALL int CNV rtr_get_spinning ( t_rtr_hnd handle , t_bool* spinning )
{
	VAR_ME
	CHECK_INIT
    CHECK_ME
	if ( me->wh_spinning )
		*spinning = t_bool_true;
	else
		*spinning = t_bool_false;
    return RTR_OK;
}

CALL int CNV rtr_do_wait_for_sync ( t_rtr_hnd handle , int timeout_ms )
{
	VAR_ME
	VAR_FT
    int err;
	double now;
	double start;
	double end;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    start = time_stamp_ms ( 0 );
    end = timeout_ms / 1000;
    _do_debug( NULL , "Ready waiting started..." );
    rtr_set_usb_timeouts( handle , 5000 , 5000 );
_wait_more:
    err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_READY_STATUS , 0 , 1 , 0 , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read ready signal from the Optospin do wait" , ERR_LVL_ABORT );
        goto _do_return;
    }
    if ( buf_data[0] == 0 ) goto _not_ready;
    err = _rtr_mesg_flags ( handle , ft_hnd , FTDI_OSPN_GET_ROTOR_STATUS , 0 , 1 , 0 , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read Optospin status for ready signals" , ERR_LVL_ABORT );
        goto _do_return;
    }
    if ( 0 != ( buf_data[0] & 0x01 ) )              // Rotors are spinning...
    {
        if ( 0 == ( buf_data[0] & 0x02 ) )          // ...but not in sync.
        {
            goto _not_ready;
        }
    }
    if ( 0 != ( buf_data[0] & 0x40 ) )              // Rotors are slowing down.
    {
        goto _not_ready;
    }
    _do_debug( NULL , "Ready sync wait over: ready." );
    err = RTR_OK;
    goto _do_return;
_not_ready:
    now = time_stamp_ms ( 0 );
    now = now - start;
    if ( now > end )
    {
        _internal_error ( RTR_ERR_TIMEOUT , "Timed out waiting for ready status from the Optospin" , ERR_LVL_ABORT );
        err = RTR_ERR_TIMEOUT;
        goto _do_return;
    }
    goto _wait_more;
_do_return:
    rtr_do_usb_timeout_default ( handle );
    return err;

}

CALL int CNV rtr_set_position_data ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos pos , char * data )
{
	VAR_ME
	VAR_FT
	int err;
    int i;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    CHECK_POS
    buf_data[0] = (wheel - 1) & 0x00FF;
    buf_data[1] = pos-1;
    for ( i=0 ; i<16 ; i++ )
    {
        buf_data[i+2] = data[i];
    }
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_WRITE_FILTER_EEPROM , 18 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to write wheel eeprom information to the Optospin" , ERR_LVL_ABORT );
    }
    return err;
}

#define     RTR_POSITION_DATA_BUFFER_SIZE    17
char		filt_dat[RTR_POSITION_DATA_BUFFER_SIZE];

CALL int CNV rtr_get_position_data ( t_rtr_hnd handle , t_whl_idx wheel , t_whl_pos pos , char ** data )
{
	VAR_ME
	VAR_FT
    int i;
    int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    CHECK_POS
    *data = NULL;
    buf_data[0] = (wheel - 1) & 0x00FF;
    buf_data[1] = pos-1;
    // Reads from Martin's 1-wire code are slow.
    // Give it a couple of seconds per block.
    err = _FTDI_Set_Timeouts ( handle , ft_hnd , 2000 , _write_timeout );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read eeprom data from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    err = _rtr_mesg ( handle ,ft_hnd , FTDI_OSPN_READ_FILTER_EEPROM , 2 , 16 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read eeprom data from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    err = _FTDI_Set_Timeouts ( handle , ft_hnd , _read_timeout , _write_timeout );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read eeprom data from the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    for ( i = 0 ; i<RTR_POSITION_DATA_BUFFER_SIZE-1 ; i++ )
    {
        filt_dat[i] = buf_data[i];
    }
    filt_dat[RTR_POSITION_DATA_BUFFER_SIZE-1] = 0;
    *data = &filt_dat[0];
    return RTR_OK;
}

CALL int CNV rtr_set_speed ( t_rtr_hnd handle , double speed )
{
	VAR_ME
	VAR_FT
	unsigned short spd;
	double spdx100;
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
	if  ( ( speed < _min_spin_speed ) || ( speed > _max_spin_speed ) )
	{
		return RTR_ERR_INVALID_PARAM;
	};
    spdx100 =  speed * 100 ;
	spdx100 = floor ( spdx100 + 0.5 );
	spd = (unsigned short) spdx100;
    buf_data[0] = spd & 0x00FF;
    spd = spd >> 8;
    buf_data[1] = spd & 0x00FF;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_SPD_X100 , 2 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set wheel speeds of the Optospin" , ERR_LVL_ABORT );
        return err;
    }
	return RTR_OK;
}

CALL int CNV rtr_set_interval ( t_rtr_hnd handle , unsigned int microsec )
{
	VAR_ME
	VAR_FT
	int err;
	unsigned int tmp;
    unsigned char low,mid,hgh;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    tmp = microsec;
    if ( tmp < _min_spin_intrv ) return RTR_ERR_INVALID_PARAM;
    if ( tmp > _max_spin_intrv ) return RTR_ERR_INVALID_PARAM;
    low = tmp & 0x000000FF;
    tmp = tmp >> 8;
    mid = tmp & 0x000000FF;
    tmp = tmp >> 8;
    hgh = tmp & 0x000000FF;
    tmp = tmp >> 8;
    buf_data[0] = low;
    buf_data[1] = mid;
    buf_data[2] = hgh;
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_REV_INTRV , 3 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set the Optospin revolution interval" , ERR_LVL_ABORT );
        return err;
    }
    return RTR_OK;
}

CALL int CNV rtr_get_spin_params ( t_rtr_hnd handle , unsigned int *microsec , t_bool *spinning , t_bool *syncing , t_bool *external )
{
	VAR_ME
	VAR_FT
	int err;
    unsigned int tmp;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    *microsec = 0;
    *spinning = t_bool_false;
    *syncing = t_bool_false;
    *external = t_bool_false;
    err = _rtr_mesg ( handle ,ft_hnd , FTDI_OSPN_RD_REV_INTRV , 0 , 6 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set the Optospin revolution interval" , ERR_LVL_ABORT );
        return err;
    }
    tmp = buf_data[5];
    tmp = tmp << 8;
    tmp = tmp + buf_data[4];
    tmp = tmp << 8;
    tmp = tmp + buf_data[3];
    *microsec = tmp;
    if ( 0 != buf_data[0] ) *spinning = t_bool_true;
    if ( 0 != buf_data[1] ) *syncing = t_bool_true;
    if ( 0 != buf_data[2] ) *external = t_bool_true;
    return RTR_OK;
}

// Generally base is ascii "1" or ascii "a" to give filter codes like "1153" or "aaec"
CALL int CNV rtr_set_ascii_wheel_positions ( t_rtr_hnd handle , char * data , int len , char base )
{
	VAR_ME
    int i;
	t_whl_pos posns[RTR_BOX_MAX_WHEEL_COUNT];
	t_whl_idx wheel;
    char* p_datum = data;
    char is;
    CHECK_INIT
    CHECK_ME
    wheel = 0;
    for ( i=0 ; i<RTR_BOX_MAX_WHEEL_COUNT ; i++)
		posns[i]=0;

    for ( i=0 ; i<len ; i++)
    {
        if ( i == RTR_BOX_MAX_WHEEL_COUNT )  break;
        is = *p_datum;
        p_datum++;
        wheel++;
        CHECK_WHEEL
        if ( is >= base )
            posns[wheel] = is - base;
    }
    return rtr_set_wheel_positions( handle , posns );
}

CALL int CNV rtr_set_deceleration_feedback_on(t_rtr_hnd handle , t_whl_idx wheel )
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    buf_data[0] = wheel - 1;
	err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_ENABLE_DECEL_FB , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Error enabling deceleration feedback" , ERR_LVL_ABORT );
        return err;
    }
    _do_debug(handle,"Now running deceleration closed loop.");
	return RTR_OK;
}

CALL int CNV rtr_set_deceleration_feedback_off(t_rtr_hnd handle , t_whl_idx wheel )
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    buf_data[0] = wheel - 1;
	err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_DISABLE_DECEL_FB , 1 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Error enabling deceleration feedback" , ERR_LVL_ABORT );
        return err;
    }
    _do_debug(handle,"Now running deceleration open loop.");
	return RTR_OK;
}


CALL int CNV rtr_set_brake_time(t_rtr_hnd handle , t_whl_idx wheel , unsigned char * centiseconds )
{
	VAR_ME
	VAR_FT
	int err;
	unsigned char time;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
    time = *centiseconds;
    if ( time > 100 )
    {
        *centiseconds = 100;
        time = 100;
    }
	buf_data[1] = time & 0x000000FF;
    buf_data[0] = wheel - 1;
	err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SET_BRAKE_TIME , 2 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to set the brake time" , ERR_LVL_ABORT );
        return err;
    }
	return RTR_OK;
}

CALL int CNV rtr_get_brake_time(t_rtr_hnd handle , t_whl_idx wheel , unsigned char * centiseconds )
{
	VAR_ME
	VAR_FT
	int err;
	CHECK_INIT
    CHECK_ME
    CHECK_FT
    CHECK_WHEEL
	buf_data[0] = wheel - 1;
    err = _rtr_mesg_flags( handle , ft_hnd , FTDI_OSPN_GET_BRAKE_TIME , 1 , 1 , 0 , 0 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to read Optospin break time" , ERR_LVL_ABORT );
        return err;
    }
    *centiseconds = buf_data[0];
    return RTR_OK;
}

CALL int CNV rtr_do_spin_wheels ( t_rtr_hnd handle  )
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_SPIN_RTRS , 0 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to spin the Optospin wheels" , ERR_LVL_ABORT );
        return err;
    }
	_do_debug ( handle , "Wheel spinning command executed." );
	me->wh_spinning = 1;
    return RTR_OK;
}

CALL int CNV rtr_do_stop_wheels ( t_rtr_hnd handle )
{
	VAR_ME
	VAR_FT
	int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_STOP_RTRS , 0 , 0 );
    if ( err != RTR_OK )
    {
        _internal_error ( err , "Unable to stop the Optospin wheels" , ERR_LVL_ABORT );
        return err;
    }
    me->wh_spinning = 0;
    return RTR_OK;
}

CALL int CNV rtr_do_ignore_ttl ( t_rtr_hnd handle , t_bool ignore )
{
	VAR_ME
	VAR_FT
    int err;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    if ( ignore == t_bool_true )
    {
        _do_debug( handle , "Ignoring TTL inputs from now on." );
        err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_IGN_TTL , 0 , 0 );
    }else{
        _do_debug( handle , "Using TTL inputs from now on." );
        err = _rtr_mesg ( handle , ft_hnd , FTDI_OSPN_USE_TTL , 0 , 0 );
    }
    if ( err != RTR_OK )
    {
        _internal_error( err , "Error changing ignore TTL state of the Optospin" , ERR_LVL_ABORT );
        return err;
    }
    return err;
}

int note_eigth = 70;
t_rtr_hnd han;
int DEFAULT_RISE = 10;
int err = RTR_OK;

void do_note ( double freq , int eighths , int rise )
{
	double per;
    per = freq / 100;
    per = per * 10;
    if ( rise )
    {
        err = rtr_set_speed( han , freq-per );
        if ( err != RTR_OK ) return;
        ms_wait(rise);
        err = rtr_set_speed( han , freq );
        if ( err != RTR_OK ) return;
        ms_wait( (note_eigth * eighths) - rise );
    }else{
        err = rtr_set_speed( han , freq );
        if ( err != RTR_OK ) return;
        ms_wait( (note_eigth * eighths) );
    }
    return;
}

CALL int CNV rtr_do_ode( t_rtr_hnd handle )
{
	VAR_ME
    int do_stop = 0;
    CHECK_INIT
    CHECK_ME;
    han = handle;
    err = RTR_OK;
    note_eigth = 110;
    DEFAULT_RISE = 20;
    #define E_CHK     if ( err != RTR_OK ) goto ode_done;
    // Set an initial "note"
    do_note( 3.496010 , 4 , 0 ); E_CHK // D
		if ( me->wh_spinning == 0 )
    {
        err = rtr_do_spin_wheels( handle ); E_CHK
        do_stop = 1;
    }
    err = rtr_do_wait_for_sync( handle , 5000 ); E_CHK
    ms_wait( 1000 ); // Breath in, and......
    do_note( 3.924133 , 4 , DEFAULT_RISE ); E_CHK // E
    do_note( 3.924133 , 4 , DEFAULT_RISE ); E_CHK // E
    do_note( 4.157476 , 4 , DEFAULT_RISE ); E_CHK // F
    do_note( 4.666610 , 4 , DEFAULT_RISE ); E_CHK // G
    do_note( 4.666610 , 4 , DEFAULT_RISE ); E_CHK // G
    do_note( 4.157476 , 4 , DEFAULT_RISE ); E_CHK // F
    do_note( 3.924133 , 4 , DEFAULT_RISE ); E_CHK // E
    do_note( 3.496010 , 4 , DEFAULT_RISE ); E_CHK // D
    do_note( 3.114590 , 4 , DEFAULT_RISE ); E_CHK // C
    do_note( 3.114590 , 4 , DEFAULT_RISE ); E_CHK // C
    do_note( 3.496010 , 4 , DEFAULT_RISE ); E_CHK // D
    do_note( 3.924133 , 4 , DEFAULT_RISE ); E_CHK // E
    do_note( 3.924133 , 6 , DEFAULT_RISE ); E_CHK // E
    do_note( 3.496010 , 2 , DEFAULT_RISE ); E_CHK // D
    do_note( 3.496010 , 12 , DEFAULT_RISE ); E_CHK // D
ode_done:
    if ( do_stop == 1 )
      err = rtr_do_stop_wheels ( handle );
    return err;
}

CALL int CNV rtr_do_device_close ( t_rtr_hnd handle )
{
	VAR_ME
	VAR_FT
    if ( NULL == handle ) return RTR_OK;
    CHECK_INIT
    CHECK_ME
    CHECK_FT
    if ( me->wh_spinning )
    {
        rtr_do_stop_wheels( handle );
    }
    FT_Close( ft_hnd );
    _destroy_rotor_details ( me );
    return RTR_OK;
}

CALL int CNV rtr_get_max_wheel_count ( t_rtr_hnd handle , t_whl_idx *max )
{
    *max = RTR_BOX_MAX_WHEEL_COUNT;
    return RTR_OK;
}

CALL int CNV rtr_get_device_count ( unsigned int * available )
{
    *available = 0;
    CHECK_INIT
    return _FTDI_Get_Device_Count( available );
}

CALL int CNV rtr_get_device_info ( unsigned int index , char * * serial , char * * descr )
{
    CHECK_INIT
    return _FTDI_Get_Device_Info( index , serial , descr );
}

//    char * str = (char *)malloc ( 1024 );
//    str[0] = 0;
//    sprintf ( str , "rtr_stop_wheels rx %d." , spd );
//    show_message ( str , "Rotor Status." );


//    str[0] = 0;
//    sprintf ( str , "rtr_stop_wheels buffer- %d,%d,%d,%d." , buffer[0] , buffer[1] , buffer[2] , buffer[3] );
//    show_message ( str , "Rotor Status." );
//    free ( str );
//    return RTR_OK;


CALL double CNV rtr_time_stamp ( void )
{
    return time_stamp_ms ( 0 );
}


#ifdef WINDOWS
#ifdef BUILD_DLL
/*
HINSTANCE Inst;
LPVOID Res;

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    Inst = hinstDLL;
    Res = lpvReserved;
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
//            initiate ( );
            break;

        case DLL_PROCESS_DETACH:
//            terminate ( );
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}
*/
#endif
#endif
