/*
 	Description:	Optospin library error code
 	Author: 		Andrew Hill <a.hill@cairn-research.co.uk>
 	Copyright:		2010 - 2011 Andrew Hill

    This file is part of optospin.dll and liboptospin.so

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "platform.h"
#include "optospin.h"
#include "errors.h"
#include <assert.h>

typedef struct _error_details error_details;
typedef error_details * perror_details;
struct _error_details
{
    t_err_lvl lvl;
    int user_code;
    char *error;
    perror_details next;
    perror_details prev;
};


perror_details e_top = NULL;
perror_details e_bot = NULL;
int e_cnt = 0;

void add_err_str ( const char * str , t_err_lvl lvl , int user )
{
    perror_details e_new;
	size_t len;
    if ( NULL == str ) return;
	len = strlen ( str );
    if ( !len ) return;
    e_new = (perror_details) malloc ( sizeof( error_details ) );
    e_new->error = (char *) malloc ( len+1 );
    e_new->error[0] = 0;
    e_new->lvl = lvl;
    e_new->user_code = user;
    e_new->next = NULL;
    e_new->prev = NULL;
	strcat ( e_new->error , str );
    if ( !e_cnt )
    {
        e_top = e_new;
        e_bot = e_new;
    } else {
        e_bot->next = e_new;
        e_new->prev = e_bot;
        e_bot = e_new;
    }
    e_cnt++;
    return;
}

void drop_error ( perror_details e_tmp )
{
    assert ( e_tmp );
    if ( e_tmp == e_top ) e_top = e_top->next;
    if ( e_tmp == e_bot ) e_bot = e_bot->prev;
    if ( e_tmp->prev ) e_tmp->prev = e_tmp->next;
    if ( e_tmp->next ) e_tmp->next = e_tmp->prev;
    if ( e_tmp->error ) free ( e_tmp->error );
    free ( e_tmp );
    e_cnt--;
    return;
}

void clr_err_strs ( t_err_lvl lvl )
{
    perror_details e_tmp;
    perror_details e_nxt;
    e_tmp = e_top;
    while ( e_tmp )
    {
        e_nxt = e_tmp->next;
        if ( ( e_tmp->lvl == lvl ) | ( ERR_LVL_ALL == lvl ) )
        {
            drop_error ( e_tmp );
        }
        e_tmp = e_nxt;
    }
    return;
}

char * rtn_str =  NULL;
t_err_lvl rtn_lvl = ERR_LVL_NONE;

CALL int CNV rtr_get_error_str ( char ** str , t_err_lvl *lvl )
{
    perror_details e_tmp;
    if ( rtn_str )
    {
        free ( rtn_str );
        rtn_str = NULL;
    }
    if ( e_cnt )
    {
        e_tmp = e_top;
        rtn_str = e_tmp->error;
        e_tmp->error = NULL;
        rtn_lvl = e_tmp->lvl;
        drop_error ( e_tmp );
    }else{
        rtn_str = (char *)malloc ( 11 );
        rtn_str[0] = 0;
		strcat( rtn_str , "No Errors." );
		rtn_lvl = ERR_LVL_NONE;
    }
    *str = rtn_str;
    *lvl = rtn_lvl;
    return RTR_OK;
}

CALL int CNV rtr_do_clr_err_strs ( t_err_lvl lvl )
{
    clr_err_strs ( lvl );
    return RTR_OK;
}

void _init_errors ( void )
{
    return;
}

void _term_errors ( void )
{
    clr_err_strs( ERR_LVL_ALL );
    if ( rtn_str )
		free ( rtn_str );
    return;
}

