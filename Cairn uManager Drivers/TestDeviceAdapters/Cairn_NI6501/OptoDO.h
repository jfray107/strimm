#ifndef _OPTOBYTE_H_
#define _OPTOBYTE_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
//#include "FTD2XX.h"
#include "NIDAQmx.h"
#include <string>
#include <map>

#define ERR_UNKNOWN_MODE         102
#define ERR_UNKNOWN_POSITION     103


#define DOUT_COUNT_MAX				8
#define DOUT_COUNT_USED				8
//Cairn Optoscan Device
class COptoDO : public CShutterBase<COptoDO>
{
public:
   COptoDO() : bits_(0),initialized_(false),shuttered_(true){
      EnableDelay(); // signals that the dealy setting will be used
   }
   ~COptoDO() {}
   int Initialize();
   int Shutdown();
   int init_optoDO ( void );
   void GetName (char* pszName) const;
   bool Busy();
   // Shutter API
   int SetOpen (bool open);
   int GetOpen(bool& open);
   int Fire(double /*deltaT*/) {return DEVICE_UNSUPPORTED_COMMAND;}

	int OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel);

	int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnValue(MM::PropertyBase* pProp, MM::ActionType eAct);

private:
	uInt8  data[DOUT_COUNT_MAX];
	unsigned char bits_;
	long binValue;
	//FT_HANDLE hnd_;
	bool initialized_;
	bool shuttered_;
	void SetBits( unsigned char bits );
	int ErrorHandler(int error);
	TaskHandle DOtaskHandle;
	//TaskHandle DOtaskHandle1;
	int DOSetData(uInt8 *myData);

};

#endif //_OPTOBYTE_H_
