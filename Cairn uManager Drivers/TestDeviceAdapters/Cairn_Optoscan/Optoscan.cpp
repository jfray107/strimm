
#include "Optoscan.h"
#include <string>
#include <math.h>
#include "ModuleInterface.h"
#include <sstream>
#include <iterator>
using namespace std;

const char* g_Optoscan = "Optoscan";

// TODO: linux entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE, DWORD  ul_reason_for_call, LPVOID )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
	break;
   case DLL_PROCESS_DETACH:
	   CloseOSLibrary( ); 
      break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
   //AddAvailableDeviceName(g_Optoscan, "Optoscan Monochromator");
   RegisterDevice(g_Optoscan, MM::ShutterDevice, "Optoscan Monochromator");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_Optoscan) == 0) return new COptoscan();
   // ...supplied name not recognized
   return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}

void COptoscan::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_Optoscan);
}

int COptoscan::Shutdown()
{
	if ( initialized_ )
	{
		os_Set_Handle(oshnd_);
		os_Close_IO();
		CloseOSLibrary();
		initialized_ = false; 
	}
	return DEVICE_OK;
}

bool COptoscan::init_optoscan ( const char * name )
{
//	char strExePath [MAX_PATH];
//	GetModuleFileName (NULL, strExePath, MAX_PATH);

	/*if ( !OpenOSLibrary( name ) )
	{
		MessageBox( 0 , L"Can't initialise Optoscan library." , L"Optoscan Error." , 0 );
		return false;
	}*/


	if ( oshnd_ != 0 )
	{
		er_ = os_Set_Handle ( oshnd_ );
		if ( er_ )
		{
			MessageBox(0,(LPCWSTR)os_Char_Error_String( er_ ),L"Optoscan Get Handle Fail",0);
			oshnd_ = 0;
			CloseOSLibrary();
			return false;
		}
		er_ = os_Close_IO ();
		if ( er_ )
		{
			MessageBox(0,(LPCWSTR)os_Char_Error_String( er_ ),L"Optoscan Get Handle Fail",0);
			oshnd_ = 0;
			CloseOSLibrary();
			return false;
		}
		oshnd_ = 0;
	}

	char * str = (char *)malloc ( 4096 );
	er_ = os_Query_IO( 0 , str , 4096 );
	free ( str );
	if ( er_ )
	{
		MessageBox(0,(LPCWSTR)os_Char_Error_String(er_),L"Optoscan Query Fail",0);
		oshnd_ = 0;
		CloseOSLibrary();
		return false;
	}
//	MessageBox( 0 , "Queried Optoscan library." , "Optoscan Error." , 0 );
//	MessageBox ( 0 , name , "Optoscan Open?." , 0 );
	er_ = os_Open_IO( 0 , (LPSTR)name );
	if ( er_ )
	{
		MessageBox(0,(LPCWSTR)os_Char_Error_String( er_ ),L"Optoscan Connection Fail",0);
		oshnd_ = 0;
		CloseOSLibrary();
		return false;
	}
//	MessageBox( 0 , "Opened Optoscan library." , "Optoscan Error." , 0 );
	er_ = os_Get_Handle ( &oshnd_ );
	if ( er_ )
	{
		MessageBox(0,(LPCWSTR)os_Char_Error_String( er_ ),L"Optoscan Get Handle Fail",0);
		oshnd_ = 0;
		CloseOSLibrary();
		return false;
	}
//	MessageBox( 0 , "Got Handle Optoscan library." , "Optoscan Error." , 0 );
	er_ = os_Shut_Both_Slits( true );
	if ( er_ )
	{
		MessageBox(0,(LPCWSTR)os_Char_Error_String( er_ ),L"Optoscan Shutter Slits Fail",0);
		oshnd_ = 0;
		CloseOSLibrary();
		return false;
	}
//	MessageBox( 0 , "Shut Slits Optoscan library." , "Optoscan Error." , 0 );
	if ( !UpdateMonochromatorDetails ( &Mono_ ) )
	{
		MessageBox( 0 , L"Can't get Optoscan details." , L"Optoscan Error." , 0 );
		oshnd_ = 0;
		CloseOSLibrary();
		return false;
	}
//	MessageBox( 0 , "Got Details Optoscan library." , "Optoscan Error." , 0 );
	return true;
}

int COptoscan::Initialize()
{
	if (initialized_) return DEVICE_OK;
	
	if ( !init_optoscan ( m_monoDevice.c_str() ) ) return DEVICE_ERR;	
	if ( !oshnd_ ) return DEVICE_ERR;
	er_ = os_Set_Handle ( oshnd_ );
	if ( er_ != os_OK ) return DEVICE_ERR;
	er_  = os_Shut_Both_Slits( true );
	if ( er_ != os_OK ) return DEVICE_ERR;
//	er_  = os_Shut_Grating( true );
//	if ( er_ ) return DEVICE_ERR;
	er_ = os_Set_Params( wave_ , in_s_ , ou_s_ );
	if ( er_ != os_OK ) return  DEVICE_ERR;

	int ret = CreateProperty(MM::g_Keyword_Name, g_Optoscan, MM::String, true);
	if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description, "Optoscan monochromator driver", MM::String, true);
	if (DEVICE_OK != ret) return ret;

	CPropertyAction* pAct = new CPropertyAction (this, &COptoscan::OnShutter);
	ret = CreateProperty( "Shutter" , "1", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter" , "0" ); // Closed
	AddAllowedValue( "Shutter" , "1" ); // Open

	pAct = new CPropertyAction ( this , &COptoscan::OnWavelength);
	ret = CreateProperty("Wavelength" , "300" , MM::Float , false , pAct );
	if (ret != DEVICE_OK) return ret; 
	SetPropertyLimits("Wavelength", Mono_.dOSMinWave , Mono_.dOSMaxWave );

	pAct = new CPropertyAction ( this , &COptoscan::OnInBandwidth);
	ret = CreateProperty("Input Bandwidth" , "20" , MM::Float , false , pAct );
	if (ret != DEVICE_OK) return ret; 
	SetPropertyLimits("Input Bandwidth", Mono_.dOSMinInSlit, Mono_.dOSMaxInSlit );

	pAct = new CPropertyAction ( this , &COptoscan::OnOutBandwidth);
	ret = CreateProperty("Output Bandwidth" , "20" , MM::Float , false , pAct );
	if (ret != DEVICE_OK) return ret; 
	SetPropertyLimits("Output Bandwidth", Mono_.dOSMinOutSlit, Mono_.dOSMaxOutSlit );
	

	shutter_ = true;
	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	initialized_ = true;
	return DEVICE_OK;
}

bool COptoscan::Busy()
{
   if ( !oshnd_ ) return DEVICE_ERR;

	return false;
   BOOL in,gr,ou;	
   Sleep(0);
   os_Set_Handle( oshnd_ );
   os_Get_Ready ( &gr , &in , &ou );
//   int ready = 1;
//   if ( in == 0 ) ready = 0;
//   if ( gr == 0 ) ready = 0;
//   if ( ou == 0 ) ready = 0;
//   if ( ready == 1 )
//      return false;
//   else
//      return true;
   
   // return false;
   if ( gr && in && ou )
      return false;
   else
      return true;
}

int inline _block_wait_for_ready ( DWORD hndl )
{
	int in,out,grt,res,er;
    if ( !hndl ) return DEVICE_ERR;
__wait:
	er = os_Set_Handle ( hndl );
	if ( er != os_OK )
	{
		return DEVICE_ERR;
	}
	er = os_Get_Ready ( &grt, &in , & out );
	if ( er != os_OK ) 
		return er;
	res = grt + in + out;
	if ( res == 3 ) 
		return er;
	goto __wait;
}

int COptoscan::SetOpen ( bool open ) 
{	
	int er;
	if ( !oshnd_ )return DEVICE_ERR;
	er = os_Set_Handle ( oshnd_ );
	if ( er != os_OK )
	{
		return DEVICE_ERR;
	}
	if ( open )
	{
		er  = os_Shut_Both_Slits( false );
		if ( er != os_OK ) return DEVICE_ERR;
		er = _block_wait_for_ready(oshnd_);
		if ( er != os_OK) return DEVICE_ERR;
//		er  = os_Shut_Grating( false );
//		if ( er != 0 ) return DEVICE_ERR;
	}
	else
	{
		er  = os_Shut_Both_Slits( true );
		if ( er != 0 ) return DEVICE_ERR;
		er = _block_wait_for_ready(oshnd_);
		if ( er != os_OK) return DEVICE_ERR;
//		er  = os_Shut_Grating ( true );
//		if ( er != 0 ) return DEVICE_ERR;
	}	
	if ( open ) shutter_ = false; else shutter_ = true; 
	return DEVICE_OK;
}

int COptoscan::GetOpen ( bool& open) 
{
	if ( shutter_ ) open = false; else open = true; 
	return DEVICE_OK;
}


int COptoscan::OnWavelength(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set( wave_ );
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get( wave_ );
        if ( !oshnd_ ) return DEVICE_ERR;
		int er = os_Set_Handle ( oshnd_ );
		if ( er != os_OK )
		{
			return DEVICE_ERR;
		}
		er = os_Set_Params( wave_ , in_s_ , ou_s_ );
		if ( er != os_OK ) 
		{
			MessageBox ( 0 , (LPCWSTR)os_Char_Error_String ( er ) , L"Optoscan Set Wavelength Error." , 0 );
			return DEVICE_CAN_NOT_SET_PROPERTY;
		}
		er = _block_wait_for_ready(oshnd_);
		if ( er != os_OK) return DEVICE_ERR;
//		Sleep(1);
	};
	return DEVICE_OK;
}

int COptoscan::OnInBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set( in_s_ );
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get( in_s_ );
        if ( !oshnd_ ) return DEVICE_ERR;
		int er = os_Set_Handle ( oshnd_ );
		if ( er != os_OK )
		{
			return DEVICE_ERR;
		}

		er = os_Set_Params( wave_ , in_s_ , ou_s_ );
		if ( er != os_OK ) 
		{
			MessageBox ( 0 , (LPCWSTR)os_Char_Error_String ( er ) , L"Optoscan Set Bandwidth Error." , 0 );
			return DEVICE_CAN_NOT_SET_PROPERTY;
		}
		er = _block_wait_for_ready(oshnd_);
		if ( er != os_OK) return DEVICE_ERR;
	};
	return DEVICE_OK;
}

int COptoscan::OnOutBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set( ou_s_ );
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get( ou_s_ );
        if ( !oshnd_ ) return DEVICE_ERR;
		int er = os_Set_Handle ( oshnd_ );
		if ( er != os_OK )
		{
			return DEVICE_ERR;
		}
		er = os_Set_Params( wave_ , in_s_ , ou_s_ );
		if ( er != os_OK ) 
		{
			MessageBox ( 0 , (LPCWSTR)os_Char_Error_String ( er ) , L"Optoscan Set Bandwidth Error." , 0 );
			return DEVICE_CAN_NOT_SET_PROPERTY;
		}
		er = _block_wait_for_ready(oshnd_);
		if ( er != os_OK) return DEVICE_ERR;
	};
	return DEVICE_OK;
}

int COptoscan::OnMonoDevice(MM::PropertyBase * pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		pProp->Set(m_monoDevice.c_str());
	}
	else if (eAct == MM::AfterSet)
	{
		if (initialized_)
		{
			// revert
			pProp->Set(m_monoDevice.c_str());
			return 1;// ERR_PORT_CHANGE_FORBIDDEN;
		}
		pProp->Get(m_monoDevice);
	}


	return 0;
}

int COptoscan::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shutter_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}
