#ifndef _CAIRN_H_
#define _CAIRN_H_

#include "DeviceBase.h"
#include "ImgBuffer.h"
#include "DeviceThreads.h"
#include "osLibrary.h"
#include <string>
#include <map>
#include <vector>

#define ERR_UNKNOWN_MODE         102
#define ERR_UNKNOWN_POSITION     103

const char* g_MonoDevicePropertyName = "Optoscan Name";

//Cairn Optoscan Device
class COptoscan : public CShutterBase<COptoscan>
{
public:
	COptoscan() : shutter_(true), oshnd_(0), initialized_(false), wave_(300), in_s_(20.0), ou_s_(20.0) {
		EnableDelay(); // signals that the dealy setting will be used

		if (!OpenOSLibrary(NULL))
		{
			MessageBox(0, L"Can't initialise Optoscan library.", L"Optoscan Error.", 0);
			return;
		}

		CPropertyAction *pAct = new CPropertyAction(this, &COptoscan::OnMonoDevice);
		CreateProperty(g_MonoDevicePropertyName, "", MM::String, false, pAct, true);


		DWORD nDevices = os_Count_IO();

		std::vector<std::string> devices = std::vector<std::string>();
		for (int i = 1; i <= nDevices; i++)
		{
			LPSTR fromOSLib = os_Name_IO((DWORD)i);
			if (!fromOSLib)
			{
				MessageBox(NULL, L"os_Name_IO returned null!", L"Warning!", MB_ICONWARNING);
				continue;
			}
			std::string devName = std::string((char*)fromOSLib);
			devices.push_back(devName);
		}

		if (devices.size() == 0)
		{
			AddAllowedValue(g_MonoDevicePropertyName, "No Devices Detected!");
			SetProperty(g_MonoDevicePropertyName, "No Devices Detected!");
			Shutdown();
		}
		else for (int i = 0; i < devices.size(); i++)
			AddAllowedValue(g_MonoDevicePropertyName, devices[i].c_str());


	}
	~COptoscan() {}
	int Initialize();
	int Shutdown();
	bool init_optoscan(const char * name);
	void GetName(char* pszName) const;
	bool Busy();
	// Shutter API
	int SetOpen(bool open);
	int GetOpen(bool& open);
	int Fire(double /*deltaT*/) { return DEVICE_UNSUPPORTED_COMMAND; }
	// action interface
	int OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnWavelength(MM::PropertyBase* pProp, MM::ActionType eAct);

	int OnInBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct);
	int OnOutBandwidth(MM::PropertyBase* pProp, MM::ActionType eAct);

	int OnMonoDevice(MM::PropertyBase* pProp, MM::ActionType eAct);
private:
	sMonochromator Mono_;
	DWORD oshnd_;
	DWORD er_;
	double wave_;
	double in_s_;
	double ou_s_;
	bool shutter_;
	bool initialized_;

	std::string m_monoDevice;
	//   MM::MMTime changedTime_;
};

#endif //_DEMOCAMERA_H_
