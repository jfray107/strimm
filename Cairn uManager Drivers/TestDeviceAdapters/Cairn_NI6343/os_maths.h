

int os_Wavelength_To_Voltage (double mech_offset, double lines_mm , double * value );
int os_In_Slit_Bandwidth_To_Width ( double lines_mm , double wavelength , double * value );
int os_In_Slit_Width_To_Voltage ( double * value );
int os_Out_Slit_Bandwidth_To_Width ( double lines_mm , double wavelength , double * value );
int os_Out_Slit_Width_To_Voltage ( double * value );


