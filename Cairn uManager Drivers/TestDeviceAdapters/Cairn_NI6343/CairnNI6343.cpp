
#include "CairnNI6343.h"
//#include "stdfx.h"
#include <string>
#include <math.h>
#include "ModuleInterface.h"
#include <sstream>
//#include <boost/lexical_cast.hpp>
//#include <boost/algorithm/string/replace.hpp>

#ifdef WIN32
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>
   #define snprintf _snprintf 
#endif
int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);


#define SHUT_MODE_LABEL			"Shutter Digital Out Bit"
#define SHUT_MODE_UNUSED		"Unused"
#define SHUT_MODE_INVERT		"TTL Invert"
#define SHUT_MODE_LOW			"TTL Low"
#define SHUT_MODE_HIGH			"TTL High"
#define DOSM_UNUSED				0
#define DOSM_INVERT				1
#define DOSM_LOW				2
#define DOSM_HIGH				3
#define DOUT_LABEL				"Direct Digital Out"
#define AOUT_LABEL              "Direct Analog Out"
#define NUM_SHUTTER_CHANNELS	4
using namespace std;
const char* g_OptoByte = "CairnNI6343";
const char* digitalPort = "Dev1/port0/line0:7";
const char* g_PropertyChannel = "IOChannel";
const char* g_PropertyPort = "AutoDetectedOutputPort";
const char* g_UseCustom = "Use Custom";
// TODO: linux entry code

// windows DLL entry code
#ifdef WIN32
BOOL APIENTRY DllMain( HANDLE /*hModule*/, 
                      DWORD  ul_reason_for_call, 
                      LPVOID /*lpReserved*/
                      )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
   case DLL_THREAD_DETACH:
		break;
   case DLL_PROCESS_DETACH:
		break;
   }
   return TRUE;
}
#endif

MODULE_API void InitializeModuleData()
{
	RegisterDevice(g_OptoByte, MM::ShutterDevice, "Cairn NI output port");
  // AddAvailableDeviceName(g_OptoByte, "Cairn NI output port");
}

MODULE_API MM::Device* CreateDevice(const char* deviceName)
{
	if (deviceName == 0) return 0;
	if (strcmp(deviceName, g_OptoByte) == 0) return new CCairnNI6343();
	return 0;
}

MODULE_API void DeleteDevice(MM::Device* pDevice)
{
   delete pDevice;
}



std::string GetNextEntry(std::string line, size_t& startIndex)
{
   size_t nextIndex = line.find(", ", startIndex + 1);
   std::string result;
   if (startIndex == std::string::npos &&
      nextIndex == std::string::npos)
   {
      // No commas found in the entire string.
      result = line;
   }
   else
   {
      size_t tmp = startIndex;
      if (startIndex == std::string::npos)
      {
         // Start from the beginning instead.
         tmp = 0;
      }
      else if (startIndex > 0)
      {
         // In the middle of the string, so add 2 to skip the
         // ", " part we expect to have here.
         tmp += 2;
      }
      result = line.substr(tmp, nextIndex - tmp);
      startIndex = nextIndex;
   }
   return result;
}
std::vector<std::string> GetDevices()
{
   std::vector<std::string> result;
   char devNames[2048];
   DAQmxGetSysDevNames(devNames, 2048);
   // Names are comma-separated.
   std::string allNames = devNames;
   size_t index = allNames.find(", ");
   if (index == std::string::npos)
   {
       // Zero or one devices.
       if (allNames.size() != 0)
       {
         result.push_back(allNames);
      }
   }
   else
   {
      result.push_back(allNames.substr(0, index));
      do {
         result.push_back(GetNextEntry(allNames, index));
      } while (index != std::string::npos);
   }
   return result;
}

CCairnNI6343::CCairnNI6343() : channel_("undef"),bits_(0),initialized_(false),shuttered_(true) {



//	CPropertyAction* pAct = new CPropertyAction (this, &CCairnNI6343::OnChannel);
//int   nRet = CreateProperty(g_PropertyChannel, "devname", MM::String, false, pAct, true);
//    assert(nRet == DEVICE_OK);

	CPropertyAction* pAct = new CPropertyAction(this, &CCairnNI6343::OnPort);
	int nRet = CreateStringProperty(g_PropertyPort, "devname", false, pAct, true);
	std::vector<std::string> devices = GetDevices();
	if (devices.size() == 0)
   {
      AddAllowedValue(g_PropertyPort, "No valid devices found");
   }
   else
   {
      AddAllowedValue(g_PropertyPort, g_UseCustom);
      SetProperty(g_PropertyPort, g_UseCustom);
   }
   for (int i = 0; i< devices.size();  ++i) {
      AddAllowedValue(g_PropertyPort,  devices.at(i).c_str());
      
   }
}
///////////////////////////////////////////////////////////////////////////////
// osShutter implementation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CCairnNI6343::GetName(char* name) const
{
   CDeviceUtils::CopyLimitedString(name, g_OptoByte);
}

int CCairnNI6343::ErrorHandler(int error)
{
	char    errBuff[2048]={'\0'};
	if( DAQmxFailed(error) )
			DAQmxGetExtendedErrorInfo(errBuff,2048);
		if( DOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(DOtaskHandle);
			DAQmxClearTask(DOtaskHandle);
			DOtaskHandle = 0;
		}
		if( AOtaskHandle ) {
			/*********************************************/
			// DAQmx Stop Code
			/*********************************************/
			DAQmxStopTask(AOtaskHandle);
			DAQmxClearTask(AOtaskHandle);
			AOtaskHandle = 0;
		}
		if( DAQmxFailed(error) )
			printf("DAQmx Error: %s\n",errBuff);
	return error;
}

int CCairnNI6343::init_optoDO (  )
{

	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	data[3] = 0;
	data[4] = 0;
	data[5] = 0;
	data[6] = 0;
	data[7] = 0;

	AOData[0] = 0.0;
	AOData[1] = 0.0;
	AOData[2] = 0.0;
	AOData[3] = 0.0;

	DOtaskHandle = 0;
	AOtaskHandle = 0;

	int32 ret = DAQmxCreateTask("",&DOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);

	ret = DAQmxCreateTask("",&AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	/*********************************************/
	// DAQmx Create Digital output Channels
	/*********************************************/
	ostringstream os;
	os << port_ << "/port0/line0:7";
	ret = DAQmxCreateDOChan(DOtaskHandle,os.str().c_str(),"",DAQmx_Val_ChanForAllLines);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	os.str("");

	os << port_ << "/ao0";

	ret = DAQmxCreateAOVoltageChan(AOtaskHandle, os.str().c_str(),"",0.0,10.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	os.str("");
	os << port_ << "/ao1";
	ret = DAQmxCreateAOVoltageChan(AOtaskHandle,os.str().c_str(),"",0.0,10.0,DAQmx_Val_Volts,"");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	os.str("");
	os << port_ << "/ao2";
	ret = DAQmxCreateAOVoltageChan(AOtaskHandle, os.str().c_str(), "", 0.0, 10.0, DAQmx_Val_Volts, "");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	os.str("");
	os << port_ << "/ao3";
	ret = DAQmxCreateAOVoltageChan(AOtaskHandle, os.str().c_str(), "", 0.0, 10.0, DAQmx_Val_Volts, "");
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


	ret = DAQmxStartTask(DOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	ret = DAQmxStartTask(AOtaskHandle);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);


		binValue = 0;
	return 0;
}

int CCairnNI6343::Shutdown()
{
	SetOpen(false);
	DAQmxStopTask(DOtaskHandle);
	DAQmxClearTask(DOtaskHandle);
	DOtaskHandle = 0;
	DAQmxStopTask(AOtaskHandle);
	DAQmxClearTask(AOtaskHandle);
	AOtaskHandle = 0;
	
	initialized_ = false; 

	return DEVICE_OK;
}

int CCairnNI6343::Initialize()
{
	if (initialized_) return DEVICE_OK;
	if ( init_optoDO() ) return DEVICE_ERR;

	int ret = CreateProperty(MM::g_Keyword_Name, g_OptoByte, MM::String, true);
	if (DEVICE_OK != ret) return ret;

	ret = CreateProperty(MM::g_Keyword_Description, "Cairn NI port driver", MM::String, true);
	if (DEVICE_OK != ret) return ret;

	CPropertyAction* pAct = new CPropertyAction (this, &CCairnNI6343::OnShutter);
	ret = CreateProperty( "Shutter" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 
	AddAllowedValue( "Shutter" , "0" ); // Closed
	AddAllowedValue( "Shutter" , "1" ); // Open


		CPropertyActionEx *pExAct;

	int i;


	for ( i=0; i < DOUT_COUNT_USED; i++) 
	{
		pExAct = new CPropertyActionEx (this, &CCairnNI6343::OnBit , i );
		ostringstream os;
		os << DOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Integer, false, pExAct); 
		if (ret != DEVICE_OK) return ret; 
		AddAllowedValue( os.str().c_str() , "0" ); 
		AddAllowedValue( os.str().c_str() , "1" ); 
	}

	for ( i=0 ; i < DAC_COUNT_USED; i++)
	{
		pExAct = new CPropertyActionEx (this, &CCairnNI6343::OnAnaOut , i );
		ostringstream os;
		os << AOUT_LABEL << " " << i;
		ret = CreateProperty( os.str().c_str() , "0", MM::Float, false, pExAct);
		if (ret != DEVICE_OK) return ret;
		SetPropertyLimits( os.str().c_str() , 0.0 , 10.0);
	}

	pAct = new CPropertyAction (this, &CCairnNI6343::OnValue);
	ret = CreateProperty( "Value" , "0", MM::Integer, false, pAct); 
	if (ret != DEVICE_OK) return ret; 

	ret = UpdateStatus();
	if (ret != DEVICE_OK) return ret;
	initialized_ = true;
	return DEVICE_OK;
}


bool CCairnNI6343::Busy()
{
	return false;
}


////void CCairnNI6343::SetBits( unsigned char bits )
////{
////	if ( shuttered_ ) return;
////	DWORD wrtn;
////	FT_Write( hnd_ , &bits , 1 , &wrtn );
////	return;
////}

//int CCairnNI6343::SetOpen (bool open)
//{
//	if ( open )
//	{
//		shuttered_ = false;
//		SetBits ( bits_ );
//	}else{
//		SetBits ( 0 );
//		shuttered_ = true;
//	}
//	return DEVICE_OK;
//}
int CCairnNI6343::SetOpen (bool open)
{
	if ( open )
	{
		shuttered_ = false;
		DOSetData(data);
	}else{
		uInt8 DigitalShutter[DOUT_COUNT_MAX];
		int i;
		for ( i = 0 ; i < DOUT_COUNT_MAX ; i++ )
		{
			//switch ( DOShutMode[i] )
			//{
			//	case DOSM_INVERT:
			//			if ( data[i] )
			//				DigitalShutter[i] = 0;
			//			else
			//				DigitalShutter[i] = 1;
			//		break;
			//	case DOSM_HIGH:
			//			DigitalShutter[i] = 1;
			//		break;
			//	case DOSM_LOW:
						//////////////////////DigitalShutter[i] = 0;
			//		break;
			//	default:	
			//		DigitalShutter[i] = data[i];
			//}

				if ( i < NUM_SHUTTER_CHANNELS)
				{
					DigitalShutter[i] = 0;
				} else {
					DigitalShutter[i] = data[i];
				}
		}
		DOSetData( DigitalShutter);
		shuttered_ = true;
	}
	return DEVICE_OK;
}

int CCairnNI6343::GetOpen(bool& open)
{
	if ( shuttered_ )
		open = false;
	else
		open = true;
	return DEVICE_OK;
}

int CCairnNI6343::DOSetData(uInt8 *myData)
{

	int32 ret =DAQmxWriteDigitalLines(DOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myData,NULL,NULL);
	
	//////std::string aString;
	//////int anint;
	//////for (int i = 0 ; i <8 ; i++)
	//////{
	//////	anint = (int)myData[i];

	//////			  char *myBuff;
	//////			  //string strRetVal;

	//////			  // Create a new char array
	//////			  myBuff = new char[100];

	//////			  // Set it to empty
	//////			  memset(myBuff,'\0',100);

	//////			  // Convert to string
	//////			  itoa(anint,myBuff,10);

	//////			  // Copy the buffer into the string object
	//////			  aString = myBuff;
 ////// 
	//////			  // Delete the buffer
	//////			  delete[] myBuff;

	//////			  MessageBoxA(0,aString.c_str(),"test1",0);
	//////}
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}

//int CCairnNI6343::OnChannel(MM::PropertyBase* pProp, MM::ActionType eAct)
//{
//   if (eAct == MM::BeforeGet)
//   {
//	  
//      pProp->Set(channel_.c_str());
//   }
//   else if (eAct == MM::AfterSet)
//   {
//      pProp->Get(channel_);
//   }
//
//   return DEVICE_OK;
//}

int CCairnNI6343::OnPort(MM::PropertyBase* pProp, MM::ActionType eAct)
{
   if (eAct == MM::BeforeGet)
   {
      pProp->Set(port_.c_str());
   }
   else if (eAct == MM::AfterSet)
   {
      pProp->Get(port_);
      //if (strcmp(port_.c_str(), g_UseCustom) != 0)
      //{
      //   // User wants to use one of our auto-detected ports.
      //   channel_ = port_;
      //}
   }

   return DEVICE_OK;
}

int CCairnNI6343::OnValue(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	long pos;
	if (eAct == MM::BeforeGet)
	{
		pos = binValue;
		pProp->Set(pos);
	}
	else if (eAct == MM::AfterSet)
	{
		pProp->Get(pos);
		binValue = pos;
		int i;
		for ( i=0 ; i<8 ; i++ )
		{
			data[i] = pos & 1;
			pos = pos >> 1;
		}
		if (!shuttered_)
			DOSetData( data);
   }
   return DEVICE_OK;
}
// copy "$(OutDir)mmgr_dal_CairnNI6501.dll" "D:\Program Files\Micro-Manager-1.4\mmgr_dal_CairnNI6501.dll"
int CCairnNI6343::OnShutter(MM::PropertyBase* pProp, MM::ActionType eAct)
{
	if (eAct == MM::BeforeGet)
	{
		if ( shuttered_ )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			return SetOpen( false );
		else
			return SetOpen( true );
   }
   return DEVICE_OK;
}

int CCairnNI6343::OnBit(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		if ( data[channel]  )
			pProp->Set(1L);
		else
			pProp->Set(0L);
	}
	else if (eAct == MM::AfterSet)
	{
		long pos;
		pProp->Get(pos);
		if ( pos )
			data[channel] = 1;
		else
			data[channel] = 0;
		if (!shuttered_)
		{
		  DOSetData( data);
		} else {
			uInt8  tempData[DOUT_COUNT_MAX];
			for (int i = 0 ; i < DOUT_COUNT_MAX ; i++)
			{
				if ( i < NUM_SHUTTER_CHANNELS)
				{
					tempData[i] = 0;
				} else {
					tempData[i] = data[i];
				}
			}
			DOSetData( tempData);
		}


   }
   return DEVICE_OK;
}

int CCairnNI6343::OnAnaOut(MM::PropertyBase* pProp, MM::ActionType eAct, long channel)
{
	if (eAct == MM::BeforeGet)
	{
		float pVal;
		pVal = (float)AOData[channel];// * percentMult;
		pProp->Set(pVal);
	}
	else if (eAct == MM::AfterSet)
	{
		float64 val;
		pProp->Get(val);
		val = val;// / percentMult;
		AOData[channel] = val;
		AOSetData(AOData);
	}
	return DEVICE_OK;
}

int CCairnNI6343::AOSetData(float64 *myAOData)
{
	char err[2048];
	char * err_str;
	int32 ret = DAQmxWriteAnalogF64(AOtaskHandle,1,1,10.0,DAQmx_Val_GroupByChannel,myAOData,NULL,NULL);
	DAQmxGetErrorString (ret, err, 2048);
	if ((int)ret != DEVICE_OK) return (int)ErrorHandler(ret);
	return DEVICE_OK;
}