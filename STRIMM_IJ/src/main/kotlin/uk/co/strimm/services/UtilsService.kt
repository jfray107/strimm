package uk.co.strimm.services

import net.imagej.ImageJService
import org.scijava.plugin.Plugin
import org.scijava.service.AbstractService
import org.scijava.service.Service

@Plugin(type = Service::class)
class UtilsService : AbstractService(), ImageJService {
    fun sanitiseNameForPlugin(nameToSanitise: String) : String{
        return nameToSanitise.replace(")","")
                             .replace("(","")
                             .replace("-", "")
                             .replace(" ", "")
    }
}