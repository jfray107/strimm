package uk.co.strimm.services

import net.imagej.ImageJService
import org.scijava.plugin.Plugin
import org.scijava.service.AbstractService
import org.scijava.service.Service

@Plugin(type = Service::class)
class SoftwareTimerService : AbstractService(), ImageJService {
    fun currentTimeNano() = System.nanoTime()
}