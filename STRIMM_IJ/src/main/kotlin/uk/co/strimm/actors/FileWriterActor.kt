package uk.co.strimm.actors

import akka.actor.AbstractActor
import akka.actor.Props
import uk.ac.shef.*
import uk.ac.shef.planar.alias.PlanarCoordinate
import uk.ac.shef.planar.alias.PlanarDataset
import uk.ac.shef.planar.dsl.planar
import uk.co.strimm.Acknowledgement
import uk.co.strimm.Paths.Companion.EXPERIMENT_OUTPUT_FOLDER
import uk.co.strimm.TraceDataStore
import uk.co.strimm.actors.messages.Message
import uk.co.strimm.actors.messages.tell.*
import uk.co.strimm.gui.GUIMain
import uk.co.strimm.services.UIstate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.logging.Level
import kotlin.system.measureTimeMillis

class FileWriterActor : AbstractActor() {
    companion object {
        fun props(): Props {
            return Props.create<FileWriterActor>(FileWriterActor::class.java) { FileWriterActor() }
        }
    }

    lateinit var hdf_file : File
    var cameraData = hashMapOf<String, ArrayImgStore>()
    var traceData = hashMapOf<String, ArrayList<TraceDataStore>>()
    var traceFromROIData = hashMapOf<String, ArrayList<TraceDataStore>>()
    var numCamDatasetsReceived = 0
    var numTraceDatasetsReceived = 0
    var numTraceROIDatasetsReceived = 0
    var expectedNumCamDatasets = 0
    var expectedNumTraceDatasets = 0
    var expectedNumTraceROIDatasets = 0

    //These are lists of the dataset objects created for the HDF5 file and NOT ImageJ or otherwise datasets
    var cameraDatasets = arrayListOf<PlanarDataset>()
    var traceFromROIDatasets = arrayListOf<PlanarDataset>()
    var traceDatasets = arrayListOf<PlanarDataset>()

    override fun createReceive(): Receive {
        return receiveBuilder()
            .match<Message>(Message::class.java) { message ->
                GUIMain.loggerService.log(Level.INFO, "FileWriter actor received message: $message")
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<TellCreateFile>(TellCreateFile::class.java) { createFileMessage ->
                GUIMain.loggerService.log(Level.INFO, "FileWriter actor received message to create file")
                sender().tell(Acknowledgement.INSTANCE, self())
                //TODO move the creation of the file to here (requires getting metadata earlier)
            }
            .match<TellNewCameraDataStore>(TellNewCameraDataStore::class.java){ message ->
                expectedNumCamDatasets++
                println("Expected number of camera datasets is now: $expectedNumCamDatasets")
            }
            .match<TellNewTraceDataStore>(TellNewTraceDataStore::class.java){ message ->
                expectedNumTraceDatasets++
                println("Expected number of trace datasets is now: $expectedNumTraceDatasets")
            }
            .match<TellNewTraceROIDataStore>(TellNewTraceROIDataStore::class.java){ message ->
                expectedNumTraceROIDatasets++
                println("Expected number of trace ROI datasets is now: $expectedNumTraceROIDatasets")
            }
            .match<TellCameraData>(TellCameraData::class.java) { images ->
                var sourceCamera = ""
                when {
                    images.imageStore.byteStack.size > 0 -> sourceCamera = images.imageStore.byteStack.first().sourceCamera
                    images.imageStore.shortStack.size > 0 -> sourceCamera = images.imageStore.shortStack.first().sourceCamera
                    images.imageStore.floatStack.size > 0 -> sourceCamera = images.imageStore.floatStack.first().sourceCamera
                }

                cameraData[sourceCamera] = images.imageStore
                numCamDatasetsReceived++

                if(allDataReceived()){
                    GUIMain.loggerService.log(Level.INFO, "Received all expected datasets")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected camera datasets: $expectedNumCamDatasets, number of received camera datasets: $numCamDatasetsReceived")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected trace ROI datasets: $expectedNumTraceROIDatasets, number of received trace ROI datasets: $numTraceROIDatasetsReceived")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected trace datasets: $expectedNumTraceDatasets, number of received trace datasets: $numTraceDatasetsReceived")

                    resetStream()

                    createBasicFile()
                    writeFile()
                }
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .match<TellTraceData>(TellTraceData::class.java){ tracePoints ->
                if(tracePoints.isTraceFromROI){
                    numTraceROIDatasetsReceived++
                    traceFromROIData[tracePoints.traceData[0].roi!!.name] = tracePoints.traceData
                }
                else{
                    numTraceDatasetsReceived++
                    traceData[tracePoints.traceData[0].roi!!.name] = tracePoints.traceData
                }

                if(allDataReceived()){
                    GUIMain.loggerService.log(Level.INFO, "Received all expected datasets")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected camera datasets: $expectedNumCamDatasets, number of received camera datasets: $numCamDatasetsReceived")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected trace ROI datasets: $expectedNumTraceROIDatasets, number of received trace ROI datasets: $numTraceROIDatasetsReceived")
                    GUIMain.loggerService.log(Level.INFO, "Number of expected trace datasets: $expectedNumTraceDatasets, number of received trace datasets: $numTraceDatasetsReceived")

                    resetStream()

                    createBasicFile()
                    writeFile()
                }
                sender().tell(Acknowledgement.INSTANCE, self())
            }
            .build()
    }

    /**
     * Method to check that the file writer actor has received all the data it was expecting.
     * @return True if the number of datasets received is equal to the expected number of datasets for each dataset type:
     * camera, trace from ROI, and trace.
     */
    private fun allDataReceived() : Boolean {
        return (numCamDatasetsReceived == expectedNumCamDatasets) &&
                (numTraceDatasetsReceived == expectedNumTraceDatasets) &&
                (numTraceROIDatasetsReceived == expectedNumTraceROIDatasets)
    }

    /**
     * Reset analogue data stream classes so preview mode can be resumed after an experiment has finished.
     */
    private fun resetStream(){
        //When you run an experiment, it is assumed you will always want to save to file (otherwise you'd use preview mode)
        //Therefore we can safely assume that this file writer actor will be created and therefore the call to
        //resetAnalogueDataStreams() will be made
        GUIMain.timerService.resetAnalogueDataStreams()
        GUIMain.strimmUIService.state = UIstate.PREVIEW //TODO temporarily here until STRIMM actor takes control of states
    }

    /**
     * This creates a structured H5 file based on the experiment but will not populate the data yet. Metadata will be
     * populated here however.
     */
    private fun createBasicFile(){
        GUIMain.loggerService.log(Level.INFO, "Creating empty h5 file...")
        initialiseLibrary(relativeLibraryPath("UHDF5"))
        val timeStampFull = ZonedDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replace(":", "-")
        val timeStamp = timeStampFull.substring(0, timeStampFull.length-4) //Trim the end to not include milliseconds
        val softwareVersion = "STRÏMM v0.0.0" //TODO get software version from pom.xml

        hdf_file = buildFileExperimentalContracts("$EXPERIMENT_OUTPUT_FOLDER/strimm_exp$timeStamp.h5") {
            metadata {
                author = GUIMain.experimentService.expConfig.author
                affiliation = GUIMain.experimentService.expConfig.affiliation
                description = GUIMain.experimentService.expConfig.description
                software = softwareVersion
            }

            cameraData.forEach{ datasetName, data ->
                val newDataset = planar(datasetName){
                    val sourceConfig = GUIMain.experimentService.expConfig.sourceConfig.sources.first { x -> x.deviceLabel == datasetName }
                    description = sourceConfig.description

                    axes(1, "Time"){
                        descriptions = arrayOf("Time")
                        units = arrayOf("px")

                        //The literal units of each dimension. So if the unit was a time based unit e.g. milliseconds
                        //then a value of 100.0 would mean 100ms
                        unitsPerSample = doubleArrayOf(1.0)

                        namedAxisValues {
                            axis("Frame")
                        }
                    }
                    plane(2, "x", "y")
                }
                cameraDatasets.add(newDataset)
            }

            traceFromROIData.forEach{ datasetName, data ->
                val newDataset = planar(datasetName){
                    description = "" //TODO pull in description from relevant flow
                    axes(1, "Time")
                    plane(1, "Time")
                }
                traceFromROIDatasets.add(newDataset)
            }

            traceData.forEach{ datasetName, data ->
                val newDataset = planar(datasetName){
                    val sourceConfig = GUIMain.experimentService.expConfig.sourceConfig.sources.first { x -> x.sourceName in datasetName }
                    description = sourceConfig.description
                    axes(1, "Time")
                    plane(1, "Time")
                }
                traceDatasets.add(newDataset)
            }
        }
    }

    /**
     * Top level method to write the HDF5 file
     */
    private fun writeFile(){
        /**
         * ONE DATASET PER DATASOURCE. However, traces from ROIs are exceptions and will be treated as a datasource.
         */

        val time = measureTimeMillis {
            hdf_file.use {
                for(cameraDatasetKey in cameraData.keys){
                    writeCameraDataset(cameraDatasetKey)
                }

                for(traceFromROIDatasetKey in traceFromROIData.keys){
                    writeTraceFromROIDataset(traceFromROIDatasetKey)
                }

                for(traceDatasetKey in traceData.keys){
                    writeTraceDataset(traceDatasetKey)
                }
            }
        }

        GUIMain.loggerService.log(Level.INFO, "Time taken to write data to file: ${time}ms")
    }

    /**
     * Write a camera (image) dataset.
     * @param datasetKey The name of the dataset
     */
    private fun writeCameraDataset(datasetKey : String){
        GUIMain.loggerService.log(Level.INFO, "Writing camera data to h5 file")
        val cameraDatasetToUse = cameraDatasets.filter { x -> x.description.name == datasetKey }.first()
        var byteData: ByteArray
        var shortData : ShortArray
        var floatData : FloatArray
        var size : LongArray
        var xDim : Long
        var yDim : Long
        val dataToWrite = cameraData[datasetKey]
        var count = 0

        cameraDatasetToUse.use { ds ->
            when {
                dataToWrite!!.byteStack.size > 0 ->{
                    for (t in 0L until dataToWrite.byteStack.size){
                        xDim = dataToWrite.byteStack[t.toInt()].xDim
                        yDim = dataToWrite.byteStack[t.toInt()].yDim
                        byteData = dataToWrite.byteStack[t.toInt()].stack

                        //The dimensions are the other way around here. Likely because of the way
                        //images are read in HDF5 i.e. column then row or row then column
                        size = longArrayOf(yDim, xDim)
                        ds.write(PlanarCoordinate(t), DataType.from(byteData, size))
                        count++
                    }
                }
                dataToWrite.shortStack.size > 0 -> {
                    for (i in 0L until dataToWrite.shortStack.size) {
                        xDim = dataToWrite.shortStack[i.toInt()].xDim
                        yDim = dataToWrite.shortStack[i.toInt()].yDim
                        shortData = dataToWrite.shortStack[i.toInt()].stack

                        //The dimensions are the other way around here. Likely because of the way
                        //images are read in HDF5 i.e. column then row or row then column
                        size = longArrayOf(yDim, xDim)
                        ds.write(PlanarCoordinate(i), DataType.from(shortData, size))
                        count++
                    }
                }
                dataToWrite.floatStack.size > 0 -> {
                    for (i in 0L until dataToWrite.floatStack.size) {
                        xDim = dataToWrite.floatStack[i.toInt()].xDim
                        yDim = dataToWrite.floatStack[i.toInt()].yDim
                        floatData = dataToWrite.floatStack[i.toInt()].stack

                        //The dimensions are the other way around here. Likely because of the way
                        //images are read in HDF5 i.e. column then row or row then column
                        size = longArrayOf(yDim, xDim)
                        ds.write(PlanarCoordinate(i), DataType.from(floatData, size))
                        count++
                    }
                }
            }
        }
    }

    /**
     * Write a trace from ROI dataset
     * @param traceFromROIDatasetKey The name of the dataset
     */
    private fun writeTraceFromROIDataset(traceFromROIDatasetKey : String){
        GUIMain.loggerService.log(Level.INFO, "Writing trace from ROI data to h5 file")
        val traceFromROIDatasetToUse = traceFromROIDatasets.first { x -> x.description.name == traceFromROIDatasetKey }
        val traceFromROIDataToWrite = traceFromROIData[traceFromROIDatasetKey]!!.map{ x -> x.roiVal.toDouble()}.toDoubleArray()

        traceFromROIDatasetToUse.use { ds ->
            ds.write(PlanarCoordinate(0), DataType.from(traceFromROIDataToWrite, longArrayOf(traceFromROIDataToWrite.size.toLong())))
        }
    }

    /**
     * Write a trace dataset
     * @param traceDatasetKey The name of the dataset
     */
    private fun writeTraceDataset(traceDatasetKey : String){
        GUIMain.loggerService.log(Level.INFO, "Writing trace data to h5 file")
        val traceDatasetToUse = traceDatasets.first { x -> x.description.name == traceDatasetKey }
        val traceDataToWrite = traceData[traceDatasetKey]!!.map{x -> x.roiVal.toDouble()}.toDoubleArray()

        traceDatasetToUse.use{ ds ->
            ds.write(PlanarCoordinate(0), DataType.from(traceDataToWrite, longArrayOf(traceDataToWrite.size.toLong())))
        }
    }
}