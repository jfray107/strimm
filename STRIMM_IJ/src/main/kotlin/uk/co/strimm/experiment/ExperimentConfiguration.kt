package uk.co.strimm.experiment

class ExperimentConfiguration{
    var author = ""
    var affiliation = ""
    var description = ""
    var experimentConfigurationName = ""
    var experimentDurationMs = 0
    var MMDeviceConfigFile = ""
    var ROIAdHoc = ""
    var hardwareDevices = HardwareDevices()
    var sourceConfig = Sources()
    var flowConfig = Flows()
    var sinkConfig = Sinks()
    var roiConfig = ROIs()
}