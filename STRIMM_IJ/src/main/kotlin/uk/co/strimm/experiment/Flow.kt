package uk.co.strimm.experiment

class Flow{
    var flowName = ""
    var roiNumber = 0
    var description = ""
    var inputNames = arrayListOf<String>()
    var inputType = ""
    var outputType = ""
}