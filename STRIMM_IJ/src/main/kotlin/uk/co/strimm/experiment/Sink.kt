package uk.co.strimm.experiment

class Sink{
    var sinkName = ""
    var inputNames = arrayListOf<String>()
    var primaryDevice = ""
    var primaryDeviceChannel = ""
    var outputType = ""
    var displayOrStore = ""
    var actorPrettyName = ""
}